parser grammar BatteryStatsParser;

options { tokenVocab=BatteryStatsLexer; }

//file            :   battery_history rest;
file            :   battery_history GARBAGE EOF
                |   END GARBAGE EOF;

//battery_history :   bhTitle resetTime status init* (wake_history|noise)* END;
battery_history :   bhTitle resetTime status+ (wake_history|noise)* END;

bhTitle         :   NAME NAME OPEN (NAME|DIGITS)+ CLOSE COLON NEWLINE;

resetTime       :   DIGITS OPEN DIGITS CLOSE RESET_TIME reset_ts NEWLINE;
reset_ts        :   DIGITS MINUS DIGITS MINUS DIGITS MINUS DIGITS MINUS DIGITS MINUS DIGITS;

head            :   OPEN DIGITS CLOSE DIGITS;
status          :   ts head wake_entry+ NEWLINE;

//init            :   DIGITS head notanewline* NEWLINE;

wake_history    :   PLUS ts head wake_entry* NEWLINE;
wake_entry      :   (PLUS|MINUS)? NAME EQUALS entry
                |   (PLUS|MINUS)? NAME EQUALS NAME
                |   (PLUS|MINUS)? NAME EQUALS DIGITS    // volt=8353
                |   (PLUS|MINUS)? NAME;                 // -running

// 1d10h00m01s020ms
ts              :   (DIGITS|NAME)+;

// wake_lock=u0a14:"*walarm*:com.google.android.gms.gcm.HEARTBEAT_ALARM"
entry           :   MINUS? uid COLON QUOTE entityname QUOTE;
uid             :   (NAME|DIGITS)+;
entityname      :   (NAME|PLUS|DIGITS|MINUS)+;  // com.android.contacts/com.viber.voip/+17654918467:android

/*
    Sometimes there is "noise" elements like
    +1d10h39m59s983ms (2) 081 volt=8353
         Details: cpu=28800u+44140s
        /proc/stat=33010 usr, 39420 sys, 3230 io, 50 irq, 1060 sirq, 61670 idle (55.5% of 23m 4s 400ms)
*/
noise           :   notaplus notanewline* NEWLINE;
notaplus        :   (DIGITS|MINUS|EQUALS|OPEN|CLOSE|COLON|QUOTE|NAME);
notanewline     :   (DIGITS|PLUS|MINUS|EQUALS|OPEN|CLOSE|COLON|QUOTE|NAME);
