lexer grammar BatteryStatsLexer;

WS              :   [ \t]+ -> skip;
NEWLINE         :   [\r\n]+;

RESET_TIME      :   'RESET:TIME:';

/* END token. No need to parse after this */
END             :   'Per-PID Stats:' -> mode(GarbageMode);

DIGITS          :   [0-9]+;
PLUS            :   '+';
MINUS           :   '-';
EQUALS          :   '=';
OPEN            :   '(';
CLOSE           :   ')';
COLON           :   ':';
QUOTE           :   '"';

/* Name contains letters, :, * */
NAME            :   ~[-()+\r\n="0-9 ]+; //[a-zA-Z0-9.:%,]+;

mode GarbageMode;
GARBAGE : .*;
