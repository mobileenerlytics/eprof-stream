package com.mobileenerlytics.eprof.stream;

import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.readers.PropsReader;
import com.mobileenerlytics.symbolic.expression.SymbolicBridge;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Map;

@Component
public class Globals {
    @Autowired
    SymbolicBridge bridge;

    @Value("${parent.dir:/tmp/symbolic/}")
    public String parentPath;

    private Pair<Long, Long> globalTimeUs;
    private Map<Integer, TaskData> taskDataMap;
    private Path traceDir;
    private ZoneId zoneId;
    private ApplicationContext context;
    private final static boolean debug = false;
    private LocalDateTime ldt;

    @Autowired
    private PropsReader propsReader;

    @NonNull
    public Pair<Long, Long> getGlobalTimeUs() {
        return globalTimeUs;
    }

    public void setProps() {
        long startTimeUs = 1000L * Long.parseLong(propsReader.getProperty("startTime"));
        long stopTimeUs = 1000L * Long.parseLong(propsReader.getProperty("endTime"));
        this.globalTimeUs = Pair.of(startTimeUs, stopTimeUs);
        Instant startTimeInstant = Instant.ofEpochMilli(startTimeUs / 1000L);
        ldt = LocalDateTime.ofInstant(startTimeInstant, ZoneId.of("UTC"));

        Verify.verify(this.zoneId == null);
        this.zoneId = ZoneId.of(propsReader.getProperty("timezone"));
    }

    @NonNull
    public Map<Integer, TaskData> getTaskDataMap() {
        return taskDataMap;
    }

    public void setTaskDataMap(@NonNull Map<Integer, TaskData> taskDataMap) {
//        Verify.verify(this.taskDataMap == null);

        // Tasks that are not processes themselves must have a parent task and their parents must be processes
//        taskDataMap.values().parallelStream()
//                .filter(t -> !t.isProc())
//                .forEach(t -> {
//                    Verify.verify(taskDataMap.containsKey(t.parentTaskId));
//                    TaskData parent = taskDataMap.get(t.parentTaskId);
//                    Verify.verify(parent.isProc());
//                });

        this.taskDataMap = taskDataMap;
    }

    @NonNull
    public Path getTraceDir() {
        return traceDir;
    }

    @PostConstruct
    public void setTraceDir() {
        Verify.verify(this.traceDir == null);
        this.traceDir = FileSystems.getDefault().getPath(parentPath);
    }

    @NonNull
    public ZoneId getZoneId() {
        return zoneId;
    }

    @NonNull
    public ApplicationContext getContext() {
        return context;
    }

    public void setContext(@NonNull ApplicationContext context) {
        Verify.verify(this.context == null);
        this.context = context;
    }

    public boolean isDebug() {
        return debug;
    }

    public LocalDateTime getLocalDateTime() {
        return ldt;
    }

    public Path getConcreteOutDir() {
        return bridge.getConcreteOutDir();
    }

    public Path getOutDir() {
        return bridge.getOutDir();
    }
}
