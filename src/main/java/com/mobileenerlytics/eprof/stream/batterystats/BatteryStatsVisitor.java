package com.mobileenerlytics.eprof.stream.batterystats;

import com.mobileenerlytics.antlr.BatteryStatsParser;
import com.mobileenerlytics.antlr.BatteryStatsParserBaseVisitor;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.util.TimeHelper;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

@Component
public class BatteryStatsVisitor extends BatteryStatsParserBaseVisitor<String> {
    @Autowired
    Globals globals;

    @Autowired
    BatteryStatsDispatcher dispatcher;

    @Autowired
    TimeHelper timeHelper;

    final Logger logger = LoggerFactory.getLogger(BatteryStatsVisitor.class.getSimpleName());
    private ZoneOffset zoneOffset;

    @Override
    public String visitFile(BatteryStatsParser.FileContext context) {
        Instant instant = Instant.now();
        this.zoneOffset = globals.getZoneId().getRules().getOffset(instant);
        visitChildren(context);
        dispatcher.finish(timestampMs);
        return null;
    }

    private long resetTimeStampMs;
    private long timestampMs;
    private String uid;
    private String value;

    public String visitReset_ts(BatteryStatsParser.Reset_tsContext ctx) {
        int year = Integer.parseInt(ctx.DIGITS(0).getText());
        int month = Integer.parseInt(ctx.DIGITS(1).getText());
        int day = Integer.parseInt(ctx.DIGITS(2).getText());
        int hour = Integer.parseInt(ctx.DIGITS(3).getText());
        int minute = Integer.parseInt(ctx.DIGITS(4).getText());
        int second = Integer.parseInt(ctx.DIGITS(5).getText());
        resetTimeStampMs = TimeUnit.MILLISECONDS.convert(
                LocalDateTime.of(year, month, day, hour, minute, second).toEpochSecond(zoneOffset), TimeUnit.SECONDS);
        logger.debug("reset_ts: {} {}", ctx.getText(), resetTimeStampMs);
        return null;
    }

    @Override
    public String visitWake_history(BatteryStatsParser.Wake_historyContext ctx) {
        try {
            return visitChildren(ctx);
        } catch(Exception e) {
            logger.warn("Ignore. Something went wrong in parsing " + ctx.getText());
        }
        return null;
    }

    @Override
    public String visitTs(BatteryStatsParser.TsContext ctx) {
        Stack<Integer> stack = new Stack<>();
        for(TerminalNode digit : ctx.DIGITS()) {
            stack.push(Integer.parseInt(digit.getText()));
        }
        timestampMs = resetTimeStampMs;

        // milliseconds
        if(!stack.empty())
            timestampMs += stack.pop();

        // seconds
        if(!stack.empty())
            timestampMs += TimeUnit.MILLISECONDS.convert(stack.pop(), TimeUnit.SECONDS);

        // minutes
        if(!stack.empty())
            timestampMs += TimeUnit.MILLISECONDS.convert(stack.pop(), TimeUnit.MINUTES);

        // hours
        if(!stack.empty())
            timestampMs += TimeUnit.MILLISECONDS.convert(stack.pop(), TimeUnit.HOURS);
        logger.debug("ts {} {}", ctx.getText(), timestampMs);

        return null;
    }

    @Override
    public String visitWake_entry (BatteryStatsParser.Wake_entryContext ctx) {
        visitChildren(ctx);
        BatteryStatsDispatcher.STATE state = BatteryStatsDispatcher.STATE.UNKNOWN;
        if(ctx.PLUS() != null || timestampMs == resetTimeStampMs)
            state = BatteryStatsDispatcher.STATE.START;
        else if(ctx.MINUS() != null)
            state = BatteryStatsDispatcher.STATE.END;

        String event = ctx.NAME(0).getText();
        if(value == null && ctx.NAME().size() > 1) {
            // (PLUS|MINUS)? NAME EQUALS NAME
            value = ctx.NAME(1).getText();
        }
        if(value == null && ctx.DIGITS() != null) {
            // (PLUS|MINUS)? NAME EQUALS DIGIT
            value = ctx.DIGITS().getText();
        }

        Pair<Long, Long> globalTimeUs = globals.getGlobalTimeUs();
        long startTimeMs = globalTimeUs.getLeft()/1000;
        if(timestampMs < startTimeMs)
            timestampMs = startTimeMs;
        if(timeHelper.within(timestampMs*1000, globalTimeUs)) {
            dispatcher.dispatch(timestampMs, state, event, uid, value);
            logger.debug("In time range-");
        } else
            logger.debug("Not in time range-");
        logger.debug("{} -- {} {} {} {} {}", ctx.getText(), timestampMs, state, event, uid, value);

        uid = null;
        value = null;

        return null;
    }

    @Override
    public String visitUid(BatteryStatsParser.UidContext ctx) {
        logger.debug("uid {}",ctx.getText());
        if(ctx.DIGITS().size() == 2) { // u0a120
            uid = "app_" + ctx.DIGITS(1).getText(); // app_120
        } else
            uid = ctx.getText();
        return uid;
    }

    @Override
    public String visitEntityname(BatteryStatsParser.EntitynameContext ctx) {
        value = ctx.getText();
        logger.debug("entityname {}",value);
        return value;
    }
}
