package com.mobileenerlytics.eprof.stream.batterystats;

import com.mobileenerlytics.antlr.BatteryStatsLexer;
import com.mobileenerlytics.antlr.BatteryStatsParser;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.util.GsonHelper;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.UnbufferedTokenStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static java.util.Comparator.comparingLong;
import static java.util.stream.Collectors.*;

@Component
public class BatteryStatsReader {
    @Autowired
    Globals globals;

    @Autowired
    BatteryStatsVisitor visitor;

    @Autowired
    private GsonHelper gsonHelper;

    public void read() {
        Path traceDir = globals.getTraceDir();
        Path outDir = globals.getOutDir();
        Map<Integer, TaskData> taskDataMap = globals.getTaskDataMap();

        Path bsPath = traceDir.resolve("batterystats");

        try {
        /* Parse files */
            CharStream inputStream = CharStreams.fromPath(bsPath);
            BatteryStatsLexer lexer = new BatteryStatsLexer(inputStream);
            UnbufferedTokenStream tokenStream = new UnbufferedTokenStream(lexer);
            BatteryStatsParser parser = new BatteryStatsParser(tokenStream);

            BatteryStatsParser.FileContext fileContext = parser.file();
            visitor.visitFile(fileContext);

            Map<TaskData, Collection<EventData>> events = new HashMap<>();
            for (TaskData td : taskDataMap.values()) {
                Collection<EventData> eventCollection = td.getEvents()
                        .filter(e -> e.getDurationMs() > 0)
                        .collect(toList());
                if (!eventCollection.isEmpty())
                    events.put(td, eventCollection);
            }

            Map<Integer, Map<String, List<EventData>>> eventsByTaskId = events.keySet().stream()
                    .collect(toMap(td -> td.taskId,
                            td -> events.get(td).stream()
                                    .sorted(comparingLong(EventData::getStartTimeMs))
                                    .collect(groupingBy(e -> e.eventName))));

            Path eventsJsonPath = outDir.resolve("events.json");
            Files.deleteIfExists(eventsJsonPath);
            Files.createFile(eventsJsonPath);
            Files.write(eventsJsonPath, gsonHelper.toJsonBytes(eventsByTaskId));
        } catch (IOException e) {
//            e.printStackTrace();
        }
    }

    public void dumpBugreport() throws IOException {
        Path traceDir = globals.getTraceDir();
        File traceDirFile = traceDir.toFile();
        Path outDir = globals.getOutDir();
        File bugreportTxtFile = outDir.resolve("bugreport.txt").toFile();
        for (File f : traceDirFile.listFiles(
                (dir, name) -> name.startsWith("bugreport") && name.endsWith(".zip"))) {
            try (ZipInputStream zis = new ZipInputStream(new FileInputStream(f));
                FileOutputStream fos = new FileOutputStream(bugreportTxtFile)) {
                ZipEntry entry;
                while ((entry = zis.getNextEntry()) != null) {
                    String name = entry.getName();
                    if (name.startsWith("bugreport") && name.endsWith("txt")) {
                        int len = 0;
                        byte[] buffer = new byte[1024];
                        while ((len = zis.read(buffer)) > 0)
                            fos.write(buffer, 0, len);
                    }
                }
            }
        }

        // Older android phones produce bugreport-***.txt files instead of zip files
        for (File f : traceDirFile.listFiles(
                (dir, name) -> name.startsWith("bugreport") && name.endsWith(".txt")))
            f.renameTo(bugreportTxtFile);
    }
}
