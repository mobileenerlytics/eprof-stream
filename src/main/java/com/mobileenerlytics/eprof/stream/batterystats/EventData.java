package com.mobileenerlytics.eprof.stream.batterystats;

import com.google.common.base.Verify;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializer;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class EventData {
    public String eventName;
    public String name;
    private long startTimeMs;
    private long endTimeMs;
    protected long durationMs;
    int mPid;
    String mThreadName;

    public EventData(TaskData td, String eventName, String name, long startTimeMs) {
        this.eventName = eventName;
        this.name = name;
        this.startTimeMs = startTimeMs;
        mPid = td.taskId;
        mThreadName = td.taskName;
    }

//    public EventData(EventData e) {
//        this.eventName = e.eventName;
//        this.name = e.name;
//        this.startTimeMs = 0;
//        mPid = e.mPid;
//        mThreadName = e.mThreadName;
//    }

    public static JsonSerializer<EventData> getJsonSerializer(Globals globals) {
        return (src, typeOfSrc, context) -> {
            final long globalStartTimeMs = globals.getGlobalTimeUs().getLeft()/1000L;
            final JsonObject jsonObject = new JsonObject();
            // Event name and pid are already part of the map holding EventData object
            // while dumping the json
            String name = src.name;
            if(name != null) {
                name = name.replaceAll(src.mThreadName, "");
                name = name.replaceAll("\\.", "_");
                name = name.replaceAll("\\/", "_");
            }
            if (name == null || name.isEmpty())
                name = src.eventName;
            jsonObject.addProperty("name", name);
            jsonObject.addProperty("startTimeMs", src.startTimeMs - globalStartTimeMs);
            jsonObject.addProperty("endTimeMs", src.endTimeMs - globalStartTimeMs);
            return jsonObject;
        };
    }

    public void setEndTime(long timeMs) {
        this.endTimeMs = timeMs;
        durationMs = endTimeMs - startTimeMs;
        Verify.verify(durationMs >= 0);
    }

    public long getDurationMs() {
        return durationMs;
    }

    public long getStartTimeMs() {
        return startTimeMs;
    }

    public long getEndTimeMs() {
        return endTimeMs;
    }

    public int getPid() {
        return mPid;
    }
}
