package com.mobileenerlytics.eprof.stream.batterystats;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static java.util.Comparator.comparing;

@Component
public class BatteryStatsDispatcher {
    @Autowired
    Globals globals;

    private final Map<TaskData, Map<String, EventData>> lastEventMap = new HashMap<>();

    private final Logger logger = LoggerFactory.getLogger(BatteryStatsDispatcher.class.getSimpleName());

    void finish(long timeMs) {
        long endTimeMs = (long) Math.min(timeMs, globals.getGlobalTimeUs().getRight()/1e3);
        lastEventMap.values().stream()
                .flatMap(m -> m.values().stream())
                .forEach(e -> e.setEndTime(endTimeMs));
    }

    enum STATE {
        START, END, UNKNOWN;
    }

    void dispatch(long timeMs, STATE state, String eventName, String uid, String name) {
        if(eventName.equals("proc"))
            return;
        logger.debug("dispatch: {} {} {} {} {}", timeMs, state, eventName, uid, name);
        Optional<TaskData> userProc;
        if (uid == null) {
            userProc = Optional.of(globals.getTaskDataMap().get(PHONE_PID));
        } else {
            userProc = globals.getTaskDataMap().values().stream()
                    .filter(TaskData::isProc)
                    .filter(td -> td.userName.equals(uid))
                    // HACK: Out of all the processes, pick the process with shortest name
                    // Out of com.app and com.app:service, com.app will be picked
                    .sorted(comparing(td -> td.taskName))
                    .findFirst();
        }
        if (!userProc.isPresent()) {
            logger.warn("Process not found for user {}", uid);
            return;
        }
        logger.debug("Picked process {} for user {}", userProc, uid);
        TaskData td = userProc.get();
        if(!lastEventMap.containsKey(td))
            lastEventMap.put(td, new HashMap<>());
        Map<String, EventData> map = lastEventMap.get(td);
        String key = eventName + name;

        if (STATE.START == state) {
            if(!map.containsKey(key)) {
                EventData eventData = td.addEvent(timeMs, eventName, name);
                map.put(key, eventData);
            }
        } else if (STATE.END == state) {
            if (!map.containsKey(key)) {
                long startTimeMs = globals.getGlobalTimeUs().getLeft()/1000;
                EventData eventData = td.addEvent(startTimeMs, eventName, name);
                map.put(key, eventData);
            }
            map.get(key).setEndTime(timeMs);
            map.remove(key);
        }
    }
}
