package com.mobileenerlytics.eprof.stream.hardware.network;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.util.NetworkUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static com.mobileenerlytics.eprof.stream.ps.TaskData.UNKNOWN_PID;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.*;

@Component
public class NetworkTablesParser {

    private static final String[] TABLE_NAMES = new String[]{"tcp", "tcp6", "udp", "udp6"};
    private static Logger logger = LoggerFactory.getLogger(NetworkTablesParser.class.getSimpleName());
    private static final int UID_INDEX = 8; // TODO: set dynamically (maybe not necessary)

    @Autowired
    Globals globals;

    /**
     * A table with row as local (= localIP:localPort) and columns as remote (= remoteIP:remotePort) and each cell
     * contains list of conversations with same local and remote. The table cell needs to have a list since multiple
     * network connections can potentially be closed and opened to the same remote program.
     */
    Table<String, String, List<Conversation>> conversationTable = HashBasedTable.create();

    public Map<Integer, List<Conversation>> matchProcesses(List<Conversation> conversations) {
        conversations.stream()
            .forEach(c -> {
                String local = c.localIP + ":" + c.localPort;
                String remote = c.remoteIP + ":" + c.remotePort;
                if(!conversationTable.contains(local, remote))
                    conversationTable.put(local, remote, new LinkedList<>());
                conversationTable.get(local, remote).add(c);
            });

        Map<Integer, List<Conversation>> pidConversationList = new HashMap<>();
        for (String tableName : TABLE_NAMES) {
            try {
                Files.lines(globals.getTraceDir().resolve(tableName))
                        .skip(1)    // Skip first title line
                        .forEach(s -> {
                            Collection<Conversation> matchingConversations = getMatchingConversations(s);
                            if(matchingConversations.isEmpty())
                                return;
                            int pid = getPID(s);
                            if(!pidConversationList.containsKey(pid))
                                pidConversationList.put(pid, new LinkedList<>());

                            pidConversationList.get(pid).addAll(matchingConversations);
                        });
            } catch (IOException e) {
                logger.warn("IOException while reading {}", tableName);
            }
        }

        pidConversationList.remove(UNKNOWN_PID);
        Set<Conversation> knownConversations = pidConversationList.values().stream()
                .flatMap(s -> s.stream()).collect(toSet());
        List<Conversation> unknownConversations = conversations.stream()
                        .filter(c -> !knownConversations.contains(c))
                        .collect(toList());

        pidConversationList.put(UNKNOWN_PID, new LinkedList<>());

        // HACK! Try to map unknown conversations to a pid using known conversations
        unknownConversations.forEach(c -> bestEffortMapToPid(c, pidConversationList));
        
        pidConversationList.put(PHONE_PID,
                conversations.stream().collect(toList()));

        return pidConversationList;
    }

    @VisibleForTesting
    void bestEffortMapToPid(Conversation unknownConversation, Map<Integer, List<Conversation>> pidConversationMap) {
        Preconditions.checkArgument(pidConversationMap != null);
        Preconditions.checkArgument(pidConversationMap.containsKey(UNKNOWN_PID));
        int selectedPid = mapPidByRemoteHostName(unknownConversation, pidConversationMap);
        if(selectedPid == UNKNOWN_PID)
            selectedPid = mapPidByRemoteIPPrefix(unknownConversation, pidConversationMap);

        pidConversationMap.get(selectedPid).add(unknownConversation);
    }

    @VisibleForTesting
    /**
     * Hack : If remote IP of an unknown conversation has long common prefix (<=> same subnet) with remote IP
     * of known conversation
     */
    int mapPidByRemoteIPPrefix(Conversation unknownConversation, Map<Integer, List<Conversation>> pidConversationMap) {
        int selectedPid = UNKNOWN_PID;
        String unknownRemoteIpBitString = NetworkUtils.toBytes(unknownConversation.remoteIP);
        int commonPrefixLength = (70 * unknownRemoteIpBitString.length()) / 100;  // prefix should be at least 70% of length
        for(Map.Entry<Integer, List<Conversation>> entry : pidConversationMap.entrySet()) {
            if(entry.getKey() == UNKNOWN_PID)
                continue;
            List<Conversation> pidConversations = entry.getValue();
            final int minCommonPrefixLength = commonPrefixLength;
            OptionalInt longestPrefixLength =
                    pidConversations.stream()
                    .filter(c -> c.localIP.equals(unknownConversation.localIP))
                    .map(c -> NetworkUtils.toBytes(c.remoteIP))
                    .mapToInt(s -> StringUtils.getCommonPrefix(s, unknownRemoteIpBitString).length())
                    .filter(d -> d > minCommonPrefixLength)
                    .max();
            if (longestPrefixLength.isPresent()) {
                commonPrefixLength = longestPrefixLength.getAsInt();
                selectedPid = entry.getKey();
            }
        }
        return selectedPid;
    }

    @VisibleForTesting
    /**
     * Hack : if remote hostname of an unknown conversation is very close a known conversation
     * Example: mediaserver-cont-dc6-2-v4.pandora.com, mediaserver-cont-dc6-1-v4.pandora.com
     */
    int mapPidByRemoteHostName(Conversation unknownConversation, Map<Integer, List<Conversation>> pidConversationMap) {
        // unknown conversation doesn't have a remote name
        if(! unknownConversation.hasRemoteName())
            return UNKNOWN_PID;

        int selectedPid = UNKNOWN_PID;

        // Maximum allowable distance is 20% of the remote host name length
        int distance = 1 + unknownConversation.remoteName.length() / 5;
        final LevenshteinDistance ld = LevenshteinDistance.getDefaultInstance();

        for(Map.Entry<Integer, List<Conversation>> entry : pidConversationMap.entrySet()) {
            if(entry.getKey() == UNKNOWN_PID)
                continue;
            List<Conversation> pidConversations = entry.getValue();

            final int maxAcceptableDistance = distance; // Needed here for stream to work
            OptionalInt shortestDistance = pidConversations.stream()
                    .filter(c -> c.localIP.equals(unknownConversation.localIP))
                    .filter(Conversation::hasRemoteName)  // Does have a remote name
                    .mapToInt(c -> ld.apply(c.remoteName, unknownConversation.remoteName))
                    // Remove conversations whose remote name is too far from the unknown conversation's remote name
                    .filter(d -> d < maxAcceptableDistance)
                    .min();
            if (shortestDistance.isPresent()) {
                selectedPid = entry.getKey();
                distance = shortestDistance.getAsInt();
            }
        }
        return selectedPid;
    }

    /**
     * This method matches a line in the mapping file logged from /proc/net/<tcp, tcp6, udp, udp6>
     * with conversations that are found from tcpdump file, capture.pcap.
     *
     * It performs the matching in following steps:
     * 1. Parse the local (localIP:localPort) and remote (remoteIP:remotePort) from the input line.
     * 2. If {@link #conversationTable} contains conversations with same local and remote, return those conversations
     * 3. For UDP connection, remote host is just 0000. In such cases, return all conversations that match the local IP
     * and local port
     *
     * Once a conversation is matched to a line, it's deleted from the {@link #conversationTable} so that it can't be
     * matched to another line.
     *
     * @param s a line from the mapping file.
     *          eg:   215: 9101A8C0:AE8F A408D9AC:01BB 01 00000000:00000000 00:00000000 00000000 10045        0 52335 2 00000000 0
     * @return conversations that match this line
     */
    @VisibleForTesting
    Collection<Conversation> getMatchingConversations(String s) {
        String[] tokens = s.trim().split("\\s+");
        String localHex = tokens[1];
        String remoteHex = tokens[2];

        String local;
        String remote;
        if(localHex.length() > 15 && remoteHex.length() > 15 && !localHex.startsWith("0000")) {  // IPv6
            local = hexToIP6(localHex);
            remote = hexToIP6(remoteHex);
        } else {    // IPv4
            local = hexToIP4(localHex);
            remote = hexToIP4(remoteHex);
        }

        if (conversationTable.contains(local, remote)) {
            List<Conversation> conversations = conversationTable.get(local, remote);
            conversationTable.remove(local, remote);
            return conversations;
        }

        LinkedList<Conversation> ret = new LinkedList<>();
        /**
         * For UDP connections, remote is just 0000 and it fails to match any connection. In such cases,
         * we return all connections with same local host and port.
         */
        if(conversationTable.containsRow(local)) {
            ret.addAll(conversationTable.row(local).values()
                    .parallelStream().flatMap(Collection::stream).collect(toList()));
            conversationTable.row(local).clear();
        }

        /**
         * Sometimes the local and remote gets interchanged in the conversation. So, we also match in the
         * column of {@link #conversationTable}.
         */
        if(conversationTable.containsColumn(local)) {
            ret.addAll(conversationTable.column(local).values()
                    .parallelStream().flatMap(Collection::stream).collect(toList()));
            conversationTable.column(local).clear();
        }
        return ret;
    }


    @VisibleForTesting
    String getUID(String line) {
        String uid = Arrays.asList(line.split("\\s+")).get(UID_INDEX);
        int uidNum = Integer.parseInt(uid);
        if (uidNum == 0) return String.valueOf(UNKNOWN_PID);
        if (uidNum >= 10000) {
            uid = "app_" + uidNum % 10000;
        }
        return uid;
    }

    @VisibleForTesting
    int getPID(String line) {
        String uid = getUID(line);
        Optional<TaskData> userProc = globals.getTaskDataMap().values().stream()
                .filter(TaskData::isProc)
                .filter(taskData -> taskData.userName.equals(uid))
                .sorted(comparing(taskData -> taskData.taskName))
                .findFirst();
        if (!userProc.isPresent()) {
            logger.warn("Process not found for user {}", uid);
            return UNKNOWN_PID;
        } else {
            return userProc.get().taskId;
        }
    }

    @VisibleForTesting
    String hexToIP4(String hex) {
        String[] tokens = hex.split(":");
        String address = tokens[0];
        if(address.length() > 8) {
            // Addresses in tcp6 and udp6 can also be ipv4 addresses
            address = address.substring(address.length()-8);
        }
        String port = tokens[1];
        String ip = "";
        for (int i = 0; i < address.length(); i += 2) {
            ip = Integer.parseInt(address.substring(i, i + 2), 16) + "." + ip;
        }
        return ip.substring(0, ip.length() - 1) + ":" + Integer.parseInt(port, 16);
    }

    @VisibleForTesting
    String hexToIP6(String hex) {
        Preconditions.checkArgument(!hex.startsWith("0000"));
        String[] tokens = hex.split(":");
        String address = tokens[0].toLowerCase();
        String port = tokens[1];
        String ip = "";

        /**
         * Break into pieces of 4
         * 2430032600e2030ee4244210a108dac9 -> 2430 0326 00e2 030e e424 4210 a108 dac9
         */
        String[] addressTokens = address.split("(?<=\\G.{4})");

        /**
         * Rearrange each token of four
         * 2430 0326 00e2 030e e424 4210 a108 dac9 -> 3024 2603 e200 0e03 24e4 1042 08a1 c9da
        */
        addressTokens = Arrays.stream(addressTokens).map(s -> s.substring(2,4) + s.substring(0,2)).toArray(String[]::new);

        /**
        * Pair-wise flip
        * 3024 2603 e200 0e03 24e4 1042 08a1 c9da -> 2603 3024 0e03 e200 1042 24e4 c9da 08a1
        */
        for(int i = 0; i+1 < addressTokens.length; i += 2) {
            String tmp = addressTokens[i];
            addressTokens[i] = addressTokens[i+1];
            addressTokens[i+1] = tmp;
        }

        /**
        * Remove zeros from beginning of each address token
        * 0e03 -> e03, 0000 -> 0
        */
        for(int i = 0; i < addressTokens.length; i ++) {
            while(addressTokens[i].startsWith("0"))
                addressTokens[i] = addressTokens[i].substring(1);   // Remove the zero
            if(addressTokens[i].length() == 0)
                addressTokens[i] = "0";
        }
        address = Arrays.stream(addressTokens).collect(joining(":"));

        return address + ":" + String.valueOf(Integer.parseInt(port, 16));
    }
}
