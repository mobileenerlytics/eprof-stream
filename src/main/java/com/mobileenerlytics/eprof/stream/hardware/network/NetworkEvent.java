package com.mobileenerlytics.eprof.stream.hardware.network;

import com.mobileenerlytics.symbolic.IEvent;

public class NetworkEvent implements IEvent<Long> {
    private long time;
    private long bytes;

    public NetworkEvent(long time, long bytes) {
        this.time = time;
        this.bytes = bytes;
    }

    @Override
    public long getTimeUs() {
        return this.time;
    }

    @Override
    public Long getValue() {
        return this.bytes;
    }

    @Override
    public String toString() {
        return String.format("%ld, %ld", time, bytes);
    }
}
