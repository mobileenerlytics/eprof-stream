package com.mobileenerlytics.eprof.stream.hardware;

import com.google.common.base.Verify;
import com.mobileenerlytics.symbolic.IEvent;
import com.mobileenerlytics.symbolic.MyDouble;

public class PowerEvent implements IEvent<MyDouble> {
    public int taskId;
    public MyDouble current;
    public long time;

    public PowerEvent(int taskId, MyDouble current, long time) {
        Verify.verify(current.isUnmodifiable());
        this.taskId = taskId;
        this.current = current;
        this.time = time;
    }

    public int getTaskId() {
        return taskId;
    }

    @Override
    public String toString() {
        return String.format("%d, %s", time, current);
    }

    @Override
    public long getTimeUs() {
        return time;
    }

    @Override
    public MyDouble getValue() {
        return current;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof PowerEvent)) return false;
        PowerEvent otherPe = (PowerEvent) other;
        return otherPe.taskId == taskId && otherPe.time == time && otherPe.current.equals(current);
    }
}
