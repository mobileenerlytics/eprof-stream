package com.mobileenerlytics.eprof.stream.hardware;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.writers.CsvWriter;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public abstract class HardwareComponent {
    String name;

    @Autowired
    Globals globals;

    @Autowired
    CsvWriter csvWriter;

    protected HardwareComponent(String name) {
        this.name = name;
    }

    protected abstract boolean build(NodeList models);

    protected abstract Map<Integer, List<PowerEvent>> parseLogs()
            throws IOException, ExecutionException, InterruptedException;

    protected void dump(Map<Integer, List<PowerEvent>> taskCurrent)
            throws IOException, ExecutionException, InterruptedException {
        Pair<Long, Long> globalTime = globals.getGlobalTimeUs();
        Path outDir = globals.getOutDir();
        Path csvPath = outDir.resolve(name + ".csv");
        csvWriter.writePowerCsv(csvPath, taskCurrent, globalTime);
    }
}
