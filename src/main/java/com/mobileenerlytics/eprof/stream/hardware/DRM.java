package com.mobileenerlytics.eprof.stream.hardware;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import com.mobileenerlytics.eprof.stream.util.AtraceHelper;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.util.PowerEventsToFgTasks;
import com.mobileenerlytics.eprof.stream.util.XmlHelper;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.*;
import java.util.stream.StreamSupport;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static java.util.stream.Collectors.toList;

@Component
public class DRM extends HardwareComponent {
    @Autowired
    private Globals globals;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private AtraceHelper atraceHelper;

    @Autowired
    private PowerEventsToFgTasks powerEventsToFgTasks;

    @Autowired
    private MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Autowired
    private DoubleFactory doubleFactory;

    MyDouble on;
    long tailUs;
    private Logger logger = LoggerFactory.getLogger(DRM.class);

    protected DRM() {
        super("DRM");
    }

    @Override
    protected boolean build(NodeList models) {
        try {
            Node decoder = XmlHelper.getNode("drm", models);
            on = doubleFactory.newSymbolicUnmodifiableDouble(XmlHelper.getNodeAttDouble("on", decoder), "DRM_on");
            tailUs = 1000 * XmlHelper.getNodeAttLong("tailms", decoder);
            logger.info("Built DRM");
            return true;
        } catch(NullPointerException npe) {
            logger.warn("Failed to build DRM from power model");
            return false;
        }
    }

    @Override
    protected Map<Integer, List<PowerEvent>> parseLogs() {
        OptionalInt optDrmPid = globals.getTaskDataMap().entrySet().parallelStream()
                .filter(e -> e.getValue().taskName.equals("mediadrmserver"))
                .mapToInt(Map.Entry::getKey)
                .findFirst();
        if(!optDrmPid.isPresent())
            return new HashMap<>();

        int drmPid = optDrmPid.getAsInt();
        Pair<Long, Long> globalTimeUs = globals.getGlobalTimeUs();

        List<PowerEvent> powerEvents = StreamSupport.stream(ctx.getBean(DbgSpliterator.class), true)
                .filter(s -> atraceHelper.inBounds(s, globalTimeUs))
                .filter(s -> s.endsWith("next_pid=" + drmPid))
                .map(this::getOnPowerEvent)
                .collect(toList());

        // Add off event at the start of test
        long startTimeUs = globalTimeUs.getLeft();
        powerEvents.add(0, new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(0), startTimeUs));

        long prevTimeUs = startTimeUs;
        ListIterator<PowerEvent> iter = powerEvents.listIterator();
        while(iter.hasNext()) {
            PowerEvent pe = iter.next();
            if(pe.time - prevTimeUs > tailUs) {
                // Move iterator back before the current position
                iter.previous();
                // Add an off event
                iter.add(new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(0), prevTimeUs + tailUs));
                // Move iterator beyond the "current" event
                iter.next();
            }
            prevTimeUs = pe.time;
        }

        // Add off event at the end
        long endTimeUs = globalTimeUs.getRight();
        if(endTimeUs - prevTimeUs > tailUs) {
            iter.add(new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(0), prevTimeUs + tailUs));
        }
        iter.add(new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(0), endTimeUs));

        Map<Integer, List<PowerEvent>> taskCurrent = new HashMap<Integer, List<PowerEvent>>() {{
            put(PHONE_PID, powerEvents);
        }};
        taskCurrent = powerEventsToFgTasks.accountTaskEnergy(taskCurrent);
        taskCurrent = movingAvgSamplingCollector.movingAverageByTasks(taskCurrent);
        return taskCurrent;
    }

    private PowerEvent getOnPowerEvent(String s) {
        long time = atraceHelper.getTime(s);
        return new PowerEvent(PHONE_PID, on, time);
    }
}
