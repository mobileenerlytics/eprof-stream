package com.mobileenerlytics.eprof.stream.hardware;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.readers.CoreReader;
import com.mobileenerlytics.eprof.stream.util.Extrapolator;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.util.TaskMerger;
import com.mobileenerlytics.eprof.stream.util.XmlHelper;
import com.mobileenerlytics.eprof.stream.writers.CsvWriter;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;

@Component
@Scope(value="prototype")
public class Core {
    private final Logger logger;

    @VisibleForTesting
    SortedMap<Long, MyDouble> activeFreq2Current;

    MyDouble idleCurrent;

    final int idx;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private Globals globals;

    @Autowired
    private TaskMerger taskMerger;

    @Autowired
    MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Autowired
    CsvWriter csvWriter;

    @Autowired
    CoreReader coreReader;

    @Autowired
    DoubleFactory doubleFactory;

    public Core(int i, int numOfCores) {
        Preconditions.checkArgument(i < numOfCores);
        idx = i;
        activeFreq2Current = new TreeMap<>();
        logger = LoggerFactory.getLogger(Core.class.getSimpleName() + i);
    }

    void addState(Node coreNode) {
        // read <core></core> entry here
//        String modeStr = xmlHelper.getNodeAtt("mode", coreNode);
        long freq = XmlHelper.getNodeAttLong("freq", coreNode);
        MyDouble active = doubleFactory.newSymbolicUnmodifiableDouble(
                XmlHelper.getNodeAttDouble("active", coreNode), "Core_active_" + freq);
        activeFreq2Current.put(freq, active);

        logger.debug( "Model entry: id {} freq {} active {}", idx, freq, active);
    }

    void setIdleCurrent(MyDouble idleCurrent) {
        this.idleCurrent = idleCurrent;
    }

    Map<Integer, List<PowerEvent>> parseLogs() {
        final Map<Integer, TaskData> taskDataMap = globals.getTaskDataMap();

        // Parse frequency, idle lines to get phone power events
        NavigableMap<Long, PowerEvent> phonePowerEventsList = coreReader.getPhonePowerEvents(this);

        // Parse context switch lines to get task power events. This doesn't get power events for the "phone task"
        Map<Integer, List<PowerEvent>> taskToPowerEventsListMap = coreReader.getTaskToPowerEventsListMap(this, phonePowerEventsList);

        taskToPowerEventsListMap.keySet().stream()
                .filter(k -> !taskDataMap.containsKey(k))
                .forEach(k -> logger.warn("Unrecognized task " + k));

        taskToPowerEventsListMap.put(PHONE_PID, new LinkedList<PowerEvent>(){{
            addAll(phonePowerEventsList.values());
        }});

        taskToPowerEventsListMap = movingAvgSamplingCollector.movingAverageByTasks(taskToPowerEventsListMap);
        return taskToPowerEventsListMap;
    }

    void dump(Map<Integer, List<PowerEvent>> procCurrent) throws IOException {
        taskMerger.mergeTasks(procCurrent);
        final Pair<Long, Long> globalTime = globals.getGlobalTimeUs();
        Path outDir = globals.getOutDir();
        Path freqCsvPath = outDir.resolve("CPU-" + idx + ".csv");
        csvWriter.writePowerCsv(freqCsvPath, procCurrent, globalTime);
    }

    private static final Pattern p_freq = Pattern
            .compile("cpu_frequency: state=(\\d+)");

    /**
     * Gets active current from input string containing frequency
     * @param s input string with cpu_frequency line
     * @return
     */
    public MyDouble getActiveCurrent(String s) {
        Matcher m_freq = p_freq.matcher(s);
        Verify.verify(m_freq.find(), "getCurrent when string is not a frequency string %s", s);
        long freq = Long.parseLong(m_freq.group(1));
        if(!activeFreq2Current.containsKey(freq))
            extrapolate(freq);
        return activeFreq2Current.get(freq);
    }

    private static final Pattern p_idle = Pattern
            .compile("cpu_idle: state=(\\d+)");

    /**
     *
     * @param s Input string from which idle state can be extracted
     * @param current active current, before core went to idle
     * @return
     */
    public MyDouble getIdleCurrent(String s, MyDouble current) {
        if(s.endsWith("next_pid=0"))
            return idleCurrent;
        if(s.contains("prev_pid=0"))
            return current;

        Matcher m_idle = p_idle.matcher(s);
        Verify.verify(m_idle.find(), "getCurrent when string is not a frequency string %s", s);
        long state = Long.parseLong(m_idle.group(1));
        if(state == 4294967295L)    // Means cpu is coming out of idle state
            return current;
        return idleCurrent;
    }

    /**
     *
     * @param freq Extrapolate power model to this frequency
     */
    private void extrapolate(long freq) {
        Extrapolator.extrapolateCurrent(activeFreq2Current, freq);
    }

    /**
     * Verify that the core now has active and idle currents for each frequency.
     * @param freqs array of frequency that needs to be verified
     */
    void verifyFreqs(long[] freqs) {
        for(long freq: freqs) {
            Verify.verify(activeFreq2Current.containsKey(freq));
        }
    }

    public int idx() {
        return idx;
    }
}
