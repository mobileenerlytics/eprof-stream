package com.mobileenerlytics.eprof.stream.hardware;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.batterystats.EventData;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.util.TimeHelper;
import com.mobileenerlytics.eprof.stream.util.XmlHelper;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Stream;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static java.util.stream.Collectors.*;

@Component
class Gps extends HardwareComponent {
    @Autowired
    private Globals globals;

    @Autowired
    private TimeHelper timeHelper;

    @Autowired
    private DoubleFactory doubleFactory;

    private Logger logger = LoggerFactory.getLogger(Gps.class);

    @VisibleForTesting
    MyDouble activeCurrent;

    protected Gps() {
        super("GPS");
    }

    @Override
    protected boolean build(NodeList models) {
        try {
            Node gpsmodel = XmlHelper.getNode("gpsmodel", models);
            Node gps = XmlHelper.getNode("gps", gpsmodel.getChildNodes());
            activeCurrent = doubleFactory.newSymbolicUnmodifiableDouble(
                    XmlHelper.getNodeAttDouble("active", gps), "GPS_active");
            return true;
        } catch(NullPointerException npe) {
            logger.warn("Failed to build Gps from power model");
            return false;
        }
    }

    @Override
    protected Map<Integer, List<PowerEvent>> parseLogs() throws IOException {
        List<EventData> eventList = globals.getTaskDataMap().get(PHONE_PID).getEvents()
                .filter(e -> e.eventName.equals("gps"))
                .collect(toList());
        if(eventList.isEmpty())
            return new HashMap<>();

        Map<Integer, List<PowerEvent>> powerMap = new HashMap<>();
        powerMap.put(PHONE_PID, eventList.stream()
                .flatMap(e -> getPowerEvents(e, PHONE_PID, 1))
                .filter(pe -> timeHelper.within(pe.time, globals.getGlobalTimeUs()))
                .collect(toList()));

        // First find the tasks that used GPS using the location file
        List<TaskData> gpsTasks = findTasksThatUsedGps();
        gpsTasks.stream().forEach(t -> powerMap.put(t.taskId, new LinkedList<>()));
        for(EventData eventData : eventList) {
            // Distribute event's energy in the ratio of total amount of time
            // tasks held LocationManagerService wakelock
            Map<Integer, Long> locationWakelockAcquired = gpsTasks.stream()
                    .flatMap(TaskData::getEvents)
                    .filter(e -> e.eventName.equals("wake_lock_in"))
                    .filter(e -> e.name.equals("LocationManagerService") || e.name.equals("*location*"))
                    .collect(groupingBy(e -> e.getPid(),
                            mapping(e -> timeHelper.overlap(Pair.of(e.getStartTimeMs(), e.getEndTimeMs()),
                                    Pair.of(eventData.getStartTimeMs(), eventData.getEndTimeMs())), summingLong(l -> l))
                    ));

            long totalWakelockAcquired = locationWakelockAcquired.values().stream().collect(summingLong(l -> l));
            locationWakelockAcquired.entrySet().stream().forEach(e -> {
                double ratio = ((double)e.getValue()) /  totalWakelockAcquired;
                if(ratio < 0.01)
                    return;
                int taskId = e.getKey();
                powerMap.get(taskId).addAll(getPowerEvents(eventData, taskId, ratio).collect(toList()));
            });
        }
        return powerMap;
    }

    // Uses location file, searches in "Historical Records by Provider:" section
    private List<TaskData> findTasksThatUsedGps() throws IOException {
        return Files.lines(globals.getTraceDir().resolve("location"))
                .filter(s -> s.contains(": gps: "))
                .map(String::trim)
                .map(s -> s.split(" ")[0])
                .map(s -> s.substring(0, s.length()-1))
                .map(s -> globals.getTaskDataMap().values().stream().filter(td -> td.taskName.equals(s)).findAny())
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());
    }

    private Stream<PowerEvent> getPowerEvents(EventData e, int taskId, double ratio) {
        Preconditions.checkArgument(ratio <= 1.01, "ratio mustn't be greater than 1 : %s", ratio);
        List<PowerEvent> powerEvents = new LinkedList<PowerEvent>() {{
            add(new PowerEvent(taskId, activeCurrent.multiply(ratio), 1000 * e.getStartTimeMs()));
            add(new PowerEvent(taskId, DoubleFactory.newUnmodifiableDouble(0), 1000 * e.getEndTimeMs()));
        }};
        return powerEvents.stream();
    }
}
