package com.mobileenerlytics.eprof.stream.hardware.network;

import com.att.aro.core.IAROService;
import com.att.aro.core.configuration.IProfileFactory;
import com.att.aro.core.configuration.pojo.Profile;
import com.att.aro.core.packetanalysis.pojo.PacketInfo;
import com.att.aro.core.packetanalysis.pojo.Session;
import com.att.aro.core.pojo.AROTraceData;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.batterystats.EventData;
import com.mobileenerlytics.eprof.stream.hardware.HardwareComponent;
import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.util.GsonHelper;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.writers.CsvWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.function.Function;

import static com.att.aro.core.settings.SettingsUtil.retrieveBestPractices;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Component
public class Network extends HardwareComponent {
    static final String TOTAL_REMOTE_HOST = "205.233.11.32";
    static final int TOTAL_PORT = 65535;

    @Autowired
    private IAROService serv;

    @Autowired
    IProfileFactory profileFactory;

    @Autowired
    Globals globals;

    @Autowired
    NetworkTablesParser networkParser;

    @Autowired
    ApplicationContext context;

    @Autowired
    MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Autowired
    CsvWriter csvWriter;

    Logger logger = LoggerFactory.getLogger(Network.class.getSimpleName());

    @Autowired
    private GsonHelper gsonHelper;

    protected Network() {
        super("Network");
    }

    @VisibleForTesting
    Session createSession(InetAddress remoteIP, int remotePort, InetAddress localIP, int localPort) {
        return new Session(remoteIP, remotePort, localIP, localPort);
    }

    @VisibleForTesting
    Session combineSessions(Session a, Session b) {
        Preconditions.checkArgument(a.isUDP(), "Do not support combining TCP sessions.");
        Preconditions.checkArgument(b.isUDP(), "Do not support combining TCP sessions.");

        Preconditions.checkArgument(a.getRemoteIP().getHostAddress().equals(b.getRemoteIP().getHostAddress()),
            "Two sessions do not have the same destinations: "
                    + a.getRemoteIP().getHostAddress() + ":" + b.getRemoteIP().getHostAddress());
        Preconditions.checkArgument(a.getRemotePort() == b.getRemotePort(),
            "Two sessions do not have the same port: "
                    + a.getRemotePort() + ":" + b.getRemotePort());

        List<PacketInfo> packets = Arrays.stream(new Session[]{a, b})
                    .flatMap(s -> s.getAllPackets().stream())
                    .sorted(comparing(PacketInfo::getTimeStamp))
                    .collect(toList());
        Session session = createSession(a.getRemoteIP(), a.getRemotePort(), a.getLocalIP(), a.getRemotePort());
        session.setUdpPackets(packets);
        session.setUdpOnly(true);
        session.setBytesTransferred(a.getBytesTransferred() + b.getBytesTransferred());
        return session;
    }

    @Override
    protected boolean build(NodeList models) {
        // TODO
        return true;
    }

    private Profile getWiFi() throws IOException {
        // TODO: Detect 802.11 type
        try {
            File file = File.createTempFile("WiFi-ac", ".conf");
            InputStream src = this.getClass().getClassLoader().getResourceAsStream("models/WiFi-ac.conf");
            Files.copy(src, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            logger.info("Using WiFi-ac.conf");
            return profileFactory.createWiFiFromFilePath(file.getAbsolutePath());
        } catch(IOException e) {
            logger.error("Unable to use WiFi-ac.conf", e);
            return profileFactory.createWiFiFromDefaultResourceFile();
        }
    }

    @VisibleForTesting
    Profile getNetworkProfile() throws IOException {
        TaskData phoneTask = globals.getTaskDataMap().get(TaskData.PHONE_PID);
        if(phoneTask.getEvents() == null) {
            // Assume WiFi
            return getWiFi();
        }
        Optional<EventData> wifi = phoneTask.getEvents()
                .filter(e -> e.eventName.equals("wifi"))
                .filter(e -> e.getDurationMs() > 0)
                .findAny();
        if (wifi.isPresent()) {
            return getWiFi();
        }
        // TODO: Is the network component LTE or 3G
        return profileFactory.createLTEFromDefaultResourceFile();
    }

    /**
     * Combine UDP sessions with same remote ips and ports
     */
    protected List<Conversation> getConversations() throws IOException {
        List<Session> sessions = readSessions();
        if(sessions == null)
            return null;
        List<Session> tcpSessions = sessions.parallelStream().filter(s -> !s.isUDP()).collect(toList());
        List<Session> udpSessions = new ArrayList<>(sessions.parallelStream()
                .filter(Session::isUDP)
                .collect(toMap(
                        s -> (s.getRemoteIP().getHostAddress() + ":" + s.getRemotePort()),
                        Function.identity(),
                        this::combineSessions
                )).values());
        tcpSessions.addAll(udpSessions);

        return tcpSessions.stream().map(s -> context.getBean(Conversation.class, s))
                .filter(c -> c.calcAndGetTotalBytes() > 0)     // Filter out empty conversations
                .collect(toList());
    }

    private List<Session> readSessions() throws IOException {
        Path pcapFile = globals.getTraceDir().resolve("capture.pcap");
        if (!pcapFile.toFile().exists())
            return null;

        AROTraceData traceData = serv.analyzeFile(retrieveBestPractices(),
                pcapFile.toAbsolutePath().toString(),
                // TODO: The Network component created should be from our power model, not ARO's default
                getNetworkProfile(),
                null);

        List<Session> sessions = traceData.getAnalyzerResult().getSessionlist();

        if(sessions != null)
            Verify.verify(!sessions.stream().flatMap(s -> s.getAllPackets().stream())
                    .filter(p -> p.getStateRangeList() != null)
                    .flatMap(p -> p.getStateRangeList().stream())
                    .filter(r -> r.getEnergy() < 0 || r.getBeginTime() > r.getEndTime())
                    .findAny().isPresent());
        return sessions;
    }

    @Override
    protected Map<Integer, List<PowerEvent>> parseLogs() throws IOException {
        // Get conversations from sessions
        List<Conversation> conversations = getConversations();

        if (conversations == null || conversations.isEmpty())
            return new HashMap<>();

        // A mapping of pid to list of conversations. Each conversation should belong to a process or app.
        Map<Integer, List<Conversation>> pidConversations = networkParser.matchProcesses(conversations);
        for (List<Conversation> pidConversationList : pidConversations.values()) {
            Conversation totalConversation = getTotalConversation(pidConversationList);
            pidConversationList.add(totalConversation);
            conversations.add(totalConversation);
        }

        // A map in which the keys are the conversation ids and the values are lists of networkevents.
        // Each networkevent represents a point in the traffic graph, a traffic graph can be drawn using a list of network
        // events. Used to generate the traffic csv file.
        Map<Integer, List<NetworkEvent>> eventsData = conversations.parallelStream().collect(toMap(Conversation::getId,
                Conversation::getNetworkEvents));

        // Generate traffic csv file.
        Path outDir = globals.getOutDir();
        Function<NetworkEvent, String> formatter = e -> String.format("%d, %d", e.getTimeUs(), e.getValue());
        csvWriter.writeCsv(outDir.resolve("network_traffic.csv"), eventsData, 0, formatter);

        // Generate conversations json file.
        Files.write(outDir.resolve("network_conversations.json"), gsonHelper.toJsonBytes(pidConversations));

        // Generate PowerEvents for each conversation
        Map<Integer, List<PowerEvent>> conversationsPower = conversations.parallelStream().collect(toMap(
                Conversation::getId, Conversation::getPowerEvents));

        // Apply moving average to network power
        final Map<Integer, List<PowerEvent>> avgConversationsPower =
                movingAvgSamplingCollector.movingAverageByTasks(conversationsPower);

        // Generate power csv file
        csvWriter.writePowerCsv(outDir.resolve("network_power.csv"), avgConversationsPower, globals.getGlobalTimeUs());

        // pidConversations contains list of conversations for each pid. The last element in the list is the
        // "total conversation" which is thus used here for calculating pid's total network energy
        Map<Integer, List<PowerEvent>> powerEvents = new HashMap<>();
        for(Map.Entry<Integer, List<Conversation>> entry: pidConversations.entrySet()) {
            int pid = entry.getKey();
            List<Conversation> conversationsOfPid = entry.getValue();
            Conversation totalConversation = conversationsOfPid.get(conversationsOfPid.size() - 1);
            powerEvents.put(pid, avgConversationsPower.get(totalConversation.id));
        }
        return powerEvents;
    }

    @Override
    public void dump(Map<Integer, List<PowerEvent>> unused) {
        // Do nothing
    }

    @VisibleForTesting
    Conversation getTotalConversation(List<Conversation> conversations) {
        try {
            Session totalSession = createSession(InetAddress.getByName(TOTAL_REMOTE_HOST), TOTAL_PORT,
                    InetAddress.getByName(TOTAL_REMOTE_HOST), TOTAL_PORT);
            totalSession.setPackets(conversations.stream()
                    .map(c -> c.session)
                    .flatMap(s -> s.getAllPackets().stream())
                    .sorted(comparing(PacketInfo::getTimeStamp))
                    .collect(toList()));
            final long totalBytesTransferred = conversations.stream().map(c -> c.session)
                    .mapToLong(s -> s.getBytesTransferred()).sum();
            totalSession.setBytesTransferred(totalBytesTransferred);
            Conversation conversation = context.getBean(Conversation.class, totalSession);
            conversation.calcAndGetTotalBytes();
            return conversation;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

}
