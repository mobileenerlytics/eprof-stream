package com.mobileenerlytics.eprof.stream.hardware;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Verify;
import com.google.common.collect.RowSortedTable;
import com.google.common.collect.TreeBasedTable;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import com.mobileenerlytics.eprof.stream.util.*;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.StreamSupport;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static java.util.stream.Collectors.groupingBy;

@Component
public class GPU extends HardwareComponent {
    final Logger logger = LoggerFactory.getLogger(GPU.class.getSimpleName());

    @VisibleForTesting
    // Long row is frequency, Integer column is memory bus, and the cell Double is the current
    RowSortedTable<Long, Integer, MyDouble> activeFreq2Current, napFreq2Current, slumberFreq2Current, initFreq2Current;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private Globals globals;

    @Autowired
    private AtraceHelper atraceHelper;

    @Autowired
    private MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Autowired
    private PowerEventsToFgTasks powerEventsToFgTasks;

    @Autowired
    private DoubleFactory doubleFactory;

    enum State {
        INIT, ACTIVE, SLUMBER, NAP
    }

    State currState = State.INIT;
    long currFreq;
    int currBus = 1;

    protected GPU() {
        super("GPU");
    }

    @Override
    public boolean build(NodeList models) {
        try {
            Node gpumodel = XmlHelper.getNode("gpumodel", models);
            NodeList gpuList = gpumodel.getChildNodes();
            if (gpuList.getLength() == 0) {
                return false;
            }

            if(gpuList.getLength() > 1) {
                logger.error("GPU model currently assumes only 1 gpumodel tag. Failed to build.");
                return false;
            }

            for (int i = 0; i < gpuList.getLength(); i++) {
                Node gpuNode = gpuList.item(i);
                String id = XmlHelper.getNodeAtt("id", gpuNode);

                activeFreq2Current = TreeBasedTable.create();
                napFreq2Current = TreeBasedTable.create();
                slumberFreq2Current = TreeBasedTable.create();
                initFreq2Current = TreeBasedTable.create();

                NodeList gpuFreqList = gpuNode.getChildNodes();
                for(int f = 0; f < gpuFreqList.getLength(); f ++) {
                    Node gpuFreqNode = gpuFreqList.item(f);
                    long freq = XmlHelper.getNodeAttLong("freq", gpuFreqNode);

                    NodeList gpuStateList = gpuFreqNode.getChildNodes();

                    for (int j = 0; j < gpuStateList.getLength(); j++) {
                        Node gpuStateNode = gpuStateList.item(j);
                        int bus = XmlHelper.getNodeAttInt("bus", gpuStateNode);

                        Verify.verify(!activeFreq2Current.contains(freq, bus));
                        Verify.verify(!napFreq2Current.contains(freq, bus));
                        Verify.verify(!slumberFreq2Current.contains(freq, bus));
                        Verify.verify(!initFreq2Current.contains(freq, bus));

                        String suffix = freq + "_" + bus;
                        activeFreq2Current.put(freq, bus,
                                doubleFactory.newSymbolicUnmodifiableDouble(
                                        XmlHelper.getNodeAttDouble("active", gpuStateNode),
                                        "GPU_active_" + suffix));

                        napFreq2Current.put(freq, bus,
                                doubleFactory.newSymbolicUnmodifiableDouble(
                                        XmlHelper.getNodeAttDouble("nap", gpuStateNode),
                                        "GPU_nap_" + suffix));

                        slumberFreq2Current.put(freq, bus,
                                doubleFactory.newSymbolicUnmodifiableDouble(
                                        XmlHelper.getNodeAttDouble("slumber", gpuStateNode),
                                        "GPU_slumber_" + suffix));

                        initFreq2Current.put(freq, bus,
                                doubleFactory.newSymbolicUnmodifiableDouble(
                                    XmlHelper.getNodeAttDouble("init", gpuStateNode),
                                        "GPU_init_" + suffix));

                        logger.debug("Putting state {} lvl {} active {} nap {} slumber {} init {}", id, freq,
                                activeFreq2Current.get(freq, bus), napFreq2Current.get(freq, bus),
                                slumberFreq2Current.get(freq, bus), initFreq2Current.get(freq, bus));
                    }
                }
            }
            currFreq = activeFreq2Current.rowKeySet().first();
            logger.info("Built GPU");
            return true;
        } catch (NullPointerException npe) {
            // WE are being lazy and not checking if any of the node is null.
            // We just catch them all here.
            logger.warn("Failed to build, skipping");
            return false;
        }
    }

    @Override
    public Map<Integer, List<PowerEvent>> parseLogs() {
        Pair<Long, Long> globalTime = globals.getGlobalTimeUs();

        Map<Integer, List<PowerEvent>> taskCurrent = StreamSupport.stream(ctx.getBean(DbgSpliterator.class), false)
                .filter(s -> s.contains(": kgsl_"))
                .filter(s -> atraceHelper.inBounds(s, globalTime))
                .map(this::getPowerEvent)
                .collect(groupingBy(PowerEvent::getTaskId));

        taskCurrent = powerEventsToFgTasks.accountTaskEnergy(taskCurrent);
        taskCurrent = movingAvgSamplingCollector.movingAverageByTasks(taskCurrent);
        return taskCurrent;
    }


    private static final Pattern p_freq = Pattern.compile("freq=(\\d+)");
    private static final Pattern p_state = Pattern.compile("state=(.+)");
    private static final Pattern p_bus = Pattern.compile("bus=(\\d+)");

    /**
     * Convert string containing GPU frequency switches to power
     *
     * @param s input string
     * @return
     */
    private PowerEvent getPowerEvent(String s) {
        long time = atraceHelper.getTime(s);
        Matcher m_freq = p_freq.matcher(s);
        Matcher m_state = p_state.matcher(s);
        Matcher m_bus = p_bus.matcher(s);
        if (m_freq.find()) {
            currFreq = Long.parseLong(m_freq.group(1));
            if (!activeFreq2Current.containsRow(currFreq)) {
                Extrapolator.extrapolateCurrent(activeFreq2Current, currFreq);
                Extrapolator.extrapolateCurrent(napFreq2Current, currFreq);
                Extrapolator.extrapolateCurrent(slumberFreq2Current, currFreq);
                Extrapolator.extrapolateCurrent(initFreq2Current, currFreq);
            }
        }

        if (m_state.find()) {
            try {
                currState = State.valueOf(m_state.group(1));
            } catch (IllegalArgumentException e) {
                currState = State.INIT;
            }
        }

        if(m_bus.find())
            currBus = Integer.parseInt(m_bus.group(1));

        MyDouble current = DoubleFactory.newUnmodifiableDouble(0);
        switch (currState) {
            case ACTIVE:
                current = activeFreq2Current.get(currFreq, currBus);
                break;
            case NAP:
                current = napFreq2Current.get(currFreq, currBus);
                break;
            case SLUMBER:
                current = slumberFreq2Current.get(currFreq, currBus);
                break;
            case INIT:
                current = initFreq2Current.get(currFreq, currBus);
                break;
        }
        return new PowerEvent(PHONE_PID, current, time);
    }

}
