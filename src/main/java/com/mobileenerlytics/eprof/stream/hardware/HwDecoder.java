package com.mobileenerlytics.eprof.stream.hardware;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import com.mobileenerlytics.eprof.stream.util.AtraceHelper;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.util.PowerEventsToFgTasks;
import com.mobileenerlytics.eprof.stream.util.XmlHelper;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static java.util.stream.Collectors.groupingBy;

@Component
public class HwDecoder extends HardwareComponent {
    @Autowired
    private Globals globals;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private AtraceHelper atraceHelper;

    @Autowired
    private PowerEventsToFgTasks powerEventsToFgTasks;

    @Autowired
    private MovingAvgSamplingCollector movingAvgSamplingCollector;

    MyDouble on, mdss_rot, mdss_fb;
    private Logger logger = LoggerFactory.getLogger(HwDecoder.class.getSimpleName());

    @Autowired
    private DoubleFactory doubleFactory;

    protected HwDecoder() {
        super("HwDecoder");
    }

    @Override
    protected boolean build(NodeList models) {
        try {
            Node decoder = XmlHelper.getNode("hwdecodermodel", models);
            on = doubleFactory.newSymbolicUnmodifiableDouble(
                    XmlHelper.getNodeAttDouble("on", decoder), "HwDecoder_on");
            mdss_rot = doubleFactory.newSymbolicUnmodifiableDouble(
                    XmlHelper.getNodeAttDouble("mdss_rot", decoder), "HwDecoder_rot");
            mdss_fb = doubleFactory.newSymbolicUnmodifiableDouble(
                    XmlHelper.getNodeAttDouble("mdss_fb", decoder), "HwDecoder_fb");
            logger.info("Built HwDecoder");
            return true;
        } catch(NullPointerException npe) {
            logger.warn("Failed to build hardware decoder from power model");
            return false;
        }
    }

    @Override
    protected Map<Integer, List<PowerEvent>> parseLogs() {
        Pair<Long, Long> globalTime = globals.getGlobalTimeUs();

        Map<Integer, List<PowerEvent>> taskCurrent = StreamSupport.stream(ctx.getBean(DbgSpliterator.class), true)
                .filter(s -> atraceHelper.inBounds(s, globalTime))
                .filter(s -> s.contains("msm_vidc:") || s.contains("sync_timeline: "))
                .map(this::getPowerEvent)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(groupingBy(PowerEvent::getTaskId));

        taskCurrent = powerEventsToFgTasks.accountTaskEnergy(taskCurrent);
        taskCurrent = movingAvgSamplingCollector.movingAverageByTasks(taskCurrent);
        return taskCurrent;
    }

    private Optional<PowerEvent> getPowerEvent(String s) {
        long time = atraceHelper.getTime(s);
        if(s.contains("name=kgsl-3d0_surfaceflinger"))
            return Optional.of(new PowerEvent(PHONE_PID, mdss_rot, time));
        if(s.contains("name=mdss_rot_"))
            return Optional.of(new PowerEvent(PHONE_PID, on, time));
        if(s.contains("name=mdss_fb0_retire"))
            return Optional.of(new PowerEvent(PHONE_PID, mdss_fb, time));
        if(s.contains("name=mdss_fb_0"))
            return Optional.of(new PowerEvent(PHONE_PID, on, time));
        if(s.contains("processing cmd:"))
            return Optional.of(new PowerEvent(PHONE_PID, on, time));
        if(s.contains("Start destroying"))
            return Optional.of(new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(0), time));
        if(s.contains("waiting for task buffers."))
            return Optional.of(new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(0), time));
        return Optional.empty();
    }
}
