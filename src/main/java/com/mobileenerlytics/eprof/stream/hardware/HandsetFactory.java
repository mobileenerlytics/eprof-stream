package com.mobileenerlytics.eprof.stream.hardware;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.readers.PropsReader;
import com.mobileenerlytics.eprof.stream.util.EntityManager;
import com.mobileenerlytics.eprof.stream.util.XmlHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;


/**
 * Returns appropriate handset depending on build.prop file in traces.
 * <p>
 * When a new handset is added, add its name and full class address in
 * handsets.properties. No need to edit this file !
 *
 * @author jindal0
 */

@SuppressWarnings("rawtypes")
@Component
public final class HandsetFactory {
    private static Logger logger = LoggerFactory.getLogger(HandsetFactory.class.getSimpleName());

    @VisibleForTesting
    @Autowired
    List<HardwareComponent> allComponents;

    @Autowired
    Globals globals;

    @Autowired
    ApplicationContext context;

    @Autowired
    PropsReader propsReader;

    private InputStream findHandsetModel(String handsetName, String cores) {
        InputStream is = null;
        while (handsetName.length() != 0) {
            is = HandsetFactory.class.getClassLoader().getResourceAsStream("models/" + cores + "/" + handsetName + ".xml");
            if (is != null)
                return is;
            handsetName = handsetName.substring(0, handsetName.length() - 1);
        }

        // WE couldn't find this handset. Use the default handset now.
        try (InputStream defIs = HandsetFactory.class.getClassLoader().getResourceAsStream("models/" + cores + "/default");
             BufferedReader bufDefIs = new BufferedReader(new InputStreamReader(defIs));) {
            String defaultHandset = bufDefIs.readLine();
            is = HandsetFactory.class.getClassLoader().getResourceAsStream("models/" + cores + "/" + defaultHandset + ".xml");
        } catch (IOException ioe) {
        }
        Verify.verify(is != null, "Can't find power model for this handset");
        return is;
    }

    /**
     * Appropriately build the Handset
     */
    public Handset buildHandset() throws ParserConfigurationException, IOException, SAXException {
        // Default handset name, if no build.prop is found
        String handsetName = propsReader.getProperty("ro.product.device", "d2");
        String cores = propsReader.getProperty("cpu.cores", "2");
        //String modelFile = "src/main/resources/models/" + handsetName + ".xml";
        InputStream modelStream;
        modelStream = findHandsetModel(handsetName, cores);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        //dbFactory.setValidating(true);
        dbFactory.setValidating(false);
        dbFactory.setIgnoringComments(true);
        dbFactory.setIgnoringElementContentWhitespace(true);
        logger.debug("Ignore comments: {}. Ignore text: {}. Validate: {}",
                dbFactory.isIgnoringComments(),
                dbFactory.isIgnoringElementContentWhitespace(),
                dbFactory.isValidating());

        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        dBuilder.setEntityResolver(new EntityManager());
        Document doc = dBuilder.parse(modelStream);
        NodeList root = doc.getChildNodes();
        logger.debug("root has {} children", root.getLength());
        Node model = XmlHelper.getNode("model", root);
        NodeList models = model.getChildNodes();
        logger.debug("model has {} children", models.getLength());

        Handset handset = context.getBean(Handset.class);
        //jVerify.verify(handset.cpu.build(models),
        //"CPU couldn't be built. Check %s format", handsetName);

        for (HardwareComponent comp : allComponents) {
            if (comp.build(models)) {
                handset.hardwares.add(comp);
            }
        }
        handset.checkValid();
        return handset;
    }
}
