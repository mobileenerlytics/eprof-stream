package com.mobileenerlytics.eprof.stream.hardware;

import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.util.EnergyCollector;
import com.mobileenerlytics.eprof.stream.util.PowerSumCalculator;
import com.mobileenerlytics.eprof.stream.util.TaskMerger;
import com.mobileenerlytics.eprof.stream.writers.CsvWriter;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ExecutionException;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.*;
import static java.util.stream.Collectors.toList;


@Component
public class Handset {
    @Autowired
    Globals globals;

    @Autowired
    TaskMerger taskMerger;

    @Autowired
    PowerSumCalculator powerSumCalculator;

    @Autowired
    CsvWriter csvWriter;

    @Autowired
    ApplicationContext context;

    private final Logger logger = LoggerFactory.getLogger(Handset.class.getSimpleName());

    // Made it a set to prevent some component added twice
    Set<HardwareComponent> hardwares;

    protected Handset() {
        // Don't make it a HashSet or TreeSet. We should ensure
        // insertion order iteration for expected results
        hardwares = new LinkedHashSet<>();
    }

    public void checkValid() {
        for (HardwareComponent hardware : hardwares)
            if (hardware instanceof CPU)
                return;
        Verify.verify(false, "Handset CPU couldn't be built");
    }

    /**
     * Returns list of error power events
     * @return
     */
    public List<PowerEvent> parseLogs() {
        Pair<Long, Long> globalTimeUs = globals.getGlobalTimeUs();
        Map<String, Map<Integer, List<PowerEvent>>> componentProcCurrents = new HashMap<>();
        for (HardwareComponent hardware : hardwares) {
            try {
                Map<Integer, List<PowerEvent>> procCurrent = hardware.parseLogs();
                if (procCurrent == null || procCurrent.isEmpty())
                    continue;
                componentProcCurrents.put(hardware.name, procCurrent);
            } catch (IOException | InterruptedException | ExecutionException e) {
                logger.warn("Error parsing logs {}", e);
            }
        }

        Map<Integer, TaskData> taskDataMap = globals.getTaskDataMap();

        // Remove numbers-- rename Thread-123 to Thread-N
        taskDataMap.values().parallelStream()
                .filter(td -> !td.isProc() || td.userName.equals("root"))
                .forEach(td -> td.taskName = td.taskName.replaceAll("[0-9]+", "N"));

        taskMerger.init(taskDataMap);

        for(HardwareComponent hardware: hardwares) {
            if(! componentProcCurrents.containsKey(hardware.name))
                continue;
            Map<Integer, List<PowerEvent>> procCurrent = componentProcCurrents.get(hardware.name);

            taskMerger.mergeTasks(procCurrent);
            try {
                hardware.dump(procCurrent);

                Map<Integer, MyDouble> energyMap = new TreeMap<>();
                for (Map.Entry<Integer, List<PowerEvent>> entry : procCurrent.entrySet()) {
                    energyMap.put(entry.getKey(), EnergyCollector.calcEnergy(context, globalTimeUs, entry.getValue()));
                }
                energyMap.entrySet().stream()
                        .forEach(e -> {
                            int task = e.getKey();
                            taskDataMap.get(task).perComponentEnergy.put(hardware.name, e.getValue());
                        });
            } catch (IOException | InterruptedException | ExecutionException e) {
                logger.warn("Error dumping component {}", e);
            }
        }


        Map<Integer, List<PowerEvent>> sumProcCurrent =
                powerSumCalculator.sumPowerByTasks(componentProcCurrents.values());

        sumProcCurrent.keySet().stream()
                .filter(taskDataMap::containsKey)
                .map(k -> taskDataMap.get(k))
                .filter(td -> td.parentTaskId == UNKNOWN_PID)
                .forEach(td ->
                        logger.warn("Task in unknown process: " + td.taskName + ":" + td.taskId));

        Path outDir = globals.getOutDir();
        Path totalCsvPath = outDir.resolve("TOTAL.csv");
        try {
            csvWriter.writePowerCsv(totalCsvPath, sumProcCurrent, globalTimeUs);
            return writeError(componentProcCurrents, sumProcCurrent);
        } catch (IOException e) {
            logger.warn("Error writing {}", e);
        }
        return null;
    }

    /**
     * Write error in ERROR.csv file
     * @param componentProcCurrents (map of hardware type -> (map of process id -> (list of power events)))
     * @param sumProcCurrent map of process id -> total predicted power
     * @return list of power events with error = many samples of (estimated current - current sensor)
     * @throws IOException
     */
    private List<PowerEvent> writeError(Map<String, Map<Integer, List<PowerEvent>>> componentProcCurrents,
                            Map<Integer, List<PowerEvent>> sumProcCurrent) throws IOException {
        if(!componentProcCurrents.containsKey("Current Sensor")) {
            logger.warn("No current sensor. Returning null errors");
            return null;
        }

        // Add list of power events (each multiplied with -1) from current sensor and add to ERROR_PID process
        List<PowerEvent> sensorPower = componentProcCurrents.get("Current Sensor")
                .get(SENSOR_PID).parallelStream()
                .map(pe -> new PowerEvent(ERROR_PID, pe.current.multiply(-1), pe.time))
                .collect(toList());

        // Add list of estimated power events to ERROR_PID process
        List<PowerEvent> estimatedPower = sumProcCurrent.get(PHONE_PID)
                .parallelStream()
                .map(pe -> new PowerEvent(ERROR_PID, pe.current, pe.time))
                .collect(toList());

        Map<Integer, List<PowerEvent>> errorPowerMap = powerSumCalculator.sumPowerByTasks(
                new LinkedList<Map<Integer, List<PowerEvent>>>() {{
                    add(new HashMap<Integer, List<PowerEvent>>() {{
                        put(ERROR_PID, sensorPower);
                    }});
                    add(new HashMap<Integer, List<PowerEvent>>() {{
                        put(ERROR_PID, estimatedPower);
                    }});
                }});
        Path outDir = globals.getOutDir();
        Path errorCsvPath = outDir.resolve("ERROR.csv");
        csvWriter.writePowerCsv(errorCsvPath, errorPowerMap, globals.getGlobalTimeUs());
        return errorPowerMap.get(ERROR_PID);
    }
}
