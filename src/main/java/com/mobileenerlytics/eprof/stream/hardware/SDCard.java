package com.mobileenerlytics.eprof.stream.hardware;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import com.mobileenerlytics.eprof.stream.util.AtraceHelper;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.util.XmlHelper;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static java.util.stream.Collectors.groupingBy;

@Component
public class SDCard extends HardwareComponent {
    private MyDouble write;
    private Logger logger = LoggerFactory.getLogger(SDCard.class.getSimpleName());

    @Autowired
    private Globals globals;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private AtraceHelper atraceHelper;

    @Autowired
    private MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Autowired
    private DoubleFactory doubleFactory;

    protected SDCard() {
        super("SDCard");
    }

    @Override
    protected boolean build(NodeList models) {
        try {
            Node sdcard = XmlHelper.getNode("sdcardmodel", models);

            write = doubleFactory.newSymbolicUnmodifiableDouble(
                    XmlHelper.getNodeAttDouble("write", sdcard),
                    "SDcard_write");

            logger.info("Built SDCard");
            return true;
        } catch(NullPointerException npe) {
            logger.warn("Failed to build sdcard from power model");
            return false;
        }
    }

    @Override
    protected Map<Integer, List<PowerEvent>> parseLogs() {
        Pair<Long, Long> globalTime = globals.getGlobalTimeUs();

        Map<Integer, List<PowerEvent>> taskCurrent = StreamSupport.stream(ctx.getBean(DbgSpliterator.class), true)
                .filter(s -> atraceHelper.inBounds(s, globalTime))
                .filter(s -> s.contains("writeback_"))
                .map(this::getPowerEvent)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(groupingBy(PowerEvent::getTaskId));

        taskCurrent = movingAvgSamplingCollector.movingAverageByTasks(taskCurrent);
        return taskCurrent;
    }

    private Optional<PowerEvent> getPowerEvent(String s) {
        long time = atraceHelper.getTime(s);
        if(s.contains("writeback_wake_background"))
            return Optional.of(new PowerEvent(PHONE_PID, write, time));
        if(s.contains("writeback_start"))
            return Optional.of(new PowerEvent(PHONE_PID, write, time));
        if(s.contains("writeback_write_inode_start"))
            return Optional.of(new PowerEvent(PHONE_PID, write, time));
        if(s.contains("writeback_write_inode_start"))
            return Optional.of(new PowerEvent(PHONE_PID, write, time));
        if(s.contains("writeback_single_inode_start"))
            return Optional.of(new PowerEvent(PHONE_PID, write, time));
        if(s.contains("writeback_written"))
            return Optional.of(new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(0), time));
        return Optional.empty();
    }
}
