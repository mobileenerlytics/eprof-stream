package com.mobileenerlytics.eprof.stream.hardware;

import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import com.mobileenerlytics.eprof.stream.util.*;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.StreamSupport;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static java.util.stream.Collectors.groupingBy;

@Component
public class MaliGPU extends HardwareComponent {
    Logger logger = LoggerFactory.getLogger(MaliGPU.class.getSimpleName());

    SortedMap<Long, MyDouble> activeFreq2Current;

    @Autowired
    private Globals globals;

    @Autowired
    private AtraceHelper atraceHelper;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Autowired
    private PowerEventsToFgTasks powerEventsToFgTasks;

    @Autowired
    private DoubleFactory doubleFactory;

    public MaliGPU() {
        super("GPU");
    }

    @Override
    public boolean build(NodeList models) {
        try {
            Node malimodel = XmlHelper.getNode("mali-model", models);
            NodeList gpuList = malimodel.getChildNodes();
            if (gpuList.getLength() == 0) {
                return false;
            }
            activeFreq2Current = new TreeMap<>();
            for (int i = 0; i < gpuList.getLength(); i++) {
                Node gpuNode = gpuList.item(i);

                NodeList gpuStateList = gpuNode.getChildNodes();

                for (int j = 0; j < gpuStateList.getLength(); j++) {
                    Node gpuStateNode = gpuStateList.item(j);
                    long freq = XmlHelper.getNodeAttLong("freq", gpuStateNode);
                    double activeDouble = XmlHelper.getNodeAttDouble("active", gpuStateNode);
                    MyDouble active = doubleFactory.newSymbolicUnmodifiableDouble(activeDouble,
                            MaliGPU.class.getSimpleName() + "_active_" + freq);
                    activeFreq2Current.put(freq, active);
                    logger.debug("Putting freq {} active {}", freq, active);
                }
            }
            logger.info("Built MaliGPU");
            return true;
        } catch (NullPointerException npe) {
            // WE are being lazy and not checking if any of the node is null.
            // We just catch them all here.
            logger.warn("Failed to build, skipping");
            return false;
        }
    }

    @Override
    public Map<Integer, List<PowerEvent>> parseLogs() {
        Pair<Long, Long> globalTime = globals.getGlobalTimeUs();
        Map<Integer, List<PowerEvent>> taskCurrent = StreamSupport.stream(ctx.getBean(DbgSpliterator.class), false)
                .filter(s -> s.contains("tracing_mark_write: gpu "))
                .filter(s -> atraceHelper.inBounds(s, globalTime))
                .map(this::getPowerEvent)
                .filter(Optional::isPresent)
                .map(pe -> pe.get())
                .collect(groupingBy(PowerEvent::getTaskId));

        taskCurrent = powerEventsToFgTasks.accountTaskEnergy(taskCurrent);
        taskCurrent = movingAvgSamplingCollector.movingAverageByTasks(taskCurrent);
        return taskCurrent;
    }

    private static final Pattern p_pwrtable = Pattern
            .compile("tracing_mark_write: gpu (.+)");
    private long prevEpochTime;
    private MyDouble prevCurrent;

    private Optional<PowerEvent> getPowerEvent(String s) {
        long time = atraceHelper.getTime(s);
        Optional<MyDouble> current = getCurrent(time, s);
        if (current.isPresent())
            return Optional.of(new PowerEvent(PHONE_PID, current.get(), time));
        return Optional.empty();
    }

    private Optional<MyDouble> getCurrent(long epochTime, String s) {
        Matcher m_pwrtable = p_pwrtable.matcher(s);
        Verify.verify(m_pwrtable.find());
        Map<Long, Integer> freqTime = new HashMap<>();
        String table = m_pwrtable.group(1).trim();
        String[] freqTokens = table.split(",");
        for (String freqToken : freqTokens) {
            String[] tokens = freqToken.trim().split("-");
            if (tokens.length == 2) {
                long frequency = Long.parseLong(tokens[0]);
                int time = Integer.parseInt(tokens[1]);
                freqTime.put(frequency, time);
            }
        }
        if (prevEpochTime != 0) {
            long timeInMs = (epochTime - prevEpochTime) / 1000;
            MyDouble current = getCurrentFromFreqTable(freqTime, timeInMs);
            if (! prevCurrent.equals(current)) {
                prevCurrent = current;
                return Optional.of(current);
            }
        }
        prevEpochTime = epochTime;
        return Optional.empty();
    }

    private MyDouble getCurrentFromFreqTable(Map<Long, Integer> freqTime, long time) {
        MyDouble current = doubleFactory.newSymbolicUnmodifiableDouble(0, "gpu_table_" + time);
        long totalTime = 0;
        for (Integer timeInFreq : freqTime.values()) {
            totalTime += timeInFreq;
        }
        if (totalTime > time) {
            logger.warn("getState time {} < totalTime {}", time, totalTime);
            time = totalTime;
        }

        for (Map.Entry<Long, Integer> entry  : freqTime.entrySet()) {
            long freq = entry.getKey();
            int timeInFreq = entry.getValue();
            if (!activeFreq2Current.containsKey(freq))
                Extrapolator.extrapolateCurrent(activeFreq2Current, freq);
            double ratio = (1.0 * timeInFreq)/ time;
            current.add(activeFreq2Current.get(freq).multiply(ratio));
        }
        //Assert.MyAssert(totalTime <= time, "GPU spent {} in states while only {} has elapsed", totalTime, time);
        logger.debug("getState map: {} time: {} current: {}", freqTime, time, current);
        return current;
    }
}
