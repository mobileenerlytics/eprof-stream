package com.mobileenerlytics.eprof.stream.hardware.network;

import com.att.aro.core.packetanalysis.pojo.PacketInfo;
import com.att.aro.core.packetanalysis.pojo.RrcStateRange;
import com.att.aro.core.packetanalysis.pojo.Session;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Verify;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.util.TimeHelper;
import com.mobileenerlytics.symbolic.DoubleFactory;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static com.mobileenerlytics.eprof.stream.hardware.network.Network.TOTAL_PORT;
import static com.mobileenerlytics.eprof.stream.hardware.network.Network.TOTAL_REMOTE_HOST;
import static java.util.stream.Collectors.*;

@Component
@Scope(value="prototype")
/**
 * Class for holding one network conversation (one unique pair of {@link #localIP}, {@link #localPort},
 * {@link #remoteIP}, {@link remotePort})
 */
public class Conversation {

    @Autowired
    TimeHelper timeHelper;

    @Expose
    int id;
    @Expose @VisibleForTesting
    public String localIP;
    @Expose @VisibleForTesting
    public String localPort;
    @VisibleForTesting
    public String remoteIP;
    @SerializedName("remoteIP") @Expose @VisibleForTesting
    public String remoteName;
    @Expose @VisibleForTesting
    public String remotePort;
    @Expose
    long totalBytes;
    @Expose
    boolean isUDP;
    @Nullable
    Session session;

    @Autowired
    Globals globals;

    static final AtomicInteger idCtr =  new AtomicInteger(0);

    Conversation() {
        this.id = idCtr.incrementAndGet();
    }

    @VisibleForTesting
    void setSession(Session session) {
       if(session.getRemoteIP().getHostAddress().equals(TOTAL_REMOTE_HOST) && session.getRemotePort() == TOTAL_PORT) {
            this.localIP = "*";
            this.localPort = "*";
            this.remoteIP = "*";
            this.remoteName = "*";
            this.remotePort = "*";
        } else {
            this.localIP = session.getLocalIP().getHostAddress();
            this.localPort = session.getLocalPort() + "";
            this.remoteIP = session.getRemoteIP().getHostAddress();
            this.remoteName = session.getRemoteIP().getHostName();
            this.remotePort = session.getRemotePort() + "";
        }
       /** {@link totalBytes} is set later in {@link #calcAndGetTotalBytes} */
//        this.totalBytes = session.getBytesTransferred();
        this.isUDP = session.isUDP();
        this.session = session;
    }

    Conversation(Session session) {
        this();
        setSession(session);
    }

    @Override
    public String toString() {
        return String.format("%s:%s %s:%s", localIP, localPort, remoteName, remotePort);
    }

    int getId() {
        return id;
    }

    /**
     * Returns a list of {@link NetworkEvent} for this conversation. {@link NetworkEvent} represents a point in the
     * traffic graph, a traffic graph can be drawn using this list.
     * @return
     */
    List<NetworkEvent> getNetworkEvents() {
        List<PacketInfo> packets = session.getAllPackets().parallelStream()
                .filter(p -> timeHelper.within(stateTimeToTimeUs(p.getTimeStamp()), globals.getGlobalTimeUs()))
                .collect(toList());
        long windowSizeMs = (long) (MovingAvgSamplingCollector.windowSizeUs(globals.getGlobalTimeUs())/1e3);
        Map<Integer, Long> rawData = packets.parallelStream().collect(
                groupingBy(p -> new Double((p.getTimeStamp() * 1e3 / windowSizeMs)).intValue(),
                        TreeMap::new, summingLong(PacketInfo::getLen)));
        List<NetworkEvent> networkEvents = rawData.entrySet().parallelStream()
                .map(entry -> new NetworkEvent(windowSizeMs * entry.getKey(), entry.getValue()))
                .collect(toList());
        return networkEvents;
    }

    List<PowerEvent> getPowerEvents() {
        return calculatePower(session.getAllPackets());
    }

    List<PowerEvent> calculatePower(List<PacketInfo> packetInfos) {
        SortedMap<Double, Double> timeToCurrentMap = packetInfos.parallelStream()
                .filter(p -> p.getStateRangeList() != null)
                .flatMap(p -> p.getStateRangeList().parallelStream())
                .filter(r -> r.getEndTime() != r.getBeginTime())
                .flatMap(r -> Stream.of(Pair.of(r.getBeginTime(), getCurrentFromRrcStateRange(r)),
                        Pair.of(r.getEndTime(), 0.0)))
                .collect(groupingBy(p -> (Double) p.getKey(),
                        TreeMap::new, summingDouble(p -> (Double) p.getValue())));

        return timeToCurrentMap.entrySet().parallelStream()
                .map(e -> (new PowerEvent(id, DoubleFactory.newUnmodifiableDouble(e.getValue()), stateTimeToTimeUs(e.getKey()))))
                .filter(pe -> timeHelper.within(pe.time, globals.getGlobalTimeUs()))
                .collect(toList());
    }

    private double getCurrentFromRrcStateRange(RrcStateRange stateRange) {
        double current = stateRange.getEnergy() * (1000 / 3.7) / (stateRange.getEndTime() - stateRange.getBeginTime());
        Verify.verify(current >= 0.0, "Negative current! " + current);
        Verify.verify(current <= 1000.0, "Huge current values! " + current);
        return current;
    }

    private long stateTimeToTimeUs(double stateTime) {
        return Math.round(stateTime * 1e6) + globals.getGlobalTimeUs().getLeft();
    }

    boolean hasRemoteName() {
        if(remoteName == null || remoteName.length() == 0)
            return false;
        if(remoteName.equals(remoteIP))
            return false;
        return true;
    }

    public long calcAndGetTotalBytes() {
        if(totalBytes != 0)
            return totalBytes;
        List<PacketInfo> packets = session.getAllPackets().parallelStream()
                .filter(p -> timeHelper.within(stateTimeToTimeUs(p.getTimeStamp()), globals.getGlobalTimeUs()))
                .collect(toList());
        totalBytes = packets.parallelStream().mapToInt(PacketInfo::getLen).sum();
        return totalBytes;
    }
}
