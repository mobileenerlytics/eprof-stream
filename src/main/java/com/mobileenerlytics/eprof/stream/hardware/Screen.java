package com.mobileenerlytics.eprof.stream.hardware;

import com.google.common.base.Preconditions;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.readers.PropsReader;
import com.mobileenerlytics.eprof.stream.util.*;
import com.mobileenerlytics.symbolic.DoubleFactory;
import org.apache.commons.lang3.tuple.Pair;
import org.bytedeco.javacpp.indexer.UByteRawIndexer;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_videoio;
import org.bytedeco.javacv.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static java.util.stream.Collectors.toList;
import static org.bytedeco.javacpp.avcodec.AV_CODEC_ID_H264;
import static org.bytedeco.javacpp.opencv_imgproc.resize;
import static org.bytedeco.javacpp.opencv_videoio.CAP_PROP_FRAME_HEIGHT;
import static org.bytedeco.javacpp.opencv_videoio.CAP_PROP_FRAME_WIDTH;
import static org.opencv.imgproc.Imgproc.INTER_NEAREST;

@Component
public class Screen extends HardwareComponent {
    static final Logger logger = LoggerFactory.getLogger(Screen.class.getSimpleName());

    @Autowired
    Globals globals;

    @Autowired
    MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Autowired
    VideoUtil videoUtil;

    @Autowired
    TimeHelper timeHelper;

    @Autowired
    PowerEventsToFgTasks powerEventsToFgTasks;

    @Autowired
    PropsReader propsReader;

    double brightnessRatio = 1.0;   // 1.0 = full brightness (255)

    /**
     * The length of a side of grid cube in power model
     */
    int size;

    /**
     * Maximum r or g or b values
     */
    final int MAX_COLOR_VAL = 255;
    private double avgCurrent;

    /**
     * The power model is expressed as a grid in the RGB cube
     */
    final Map<RoundedRGB, OledCurrent> currentMap = new HashMap<>();

    class OledCurrent {
        final double current, slopeR, slopeG, slopeB;

        OledCurrent(double current, double slopeR, double slopeG, double slopeB) {
            this.current = current;
            this.slopeR = slopeR;
            this.slopeG = slopeG;
            this.slopeB = slopeB;
        }

        public double getCurrent(int r, int g, int b) {
            int roundr = (r/size) * size;
            int roundg = (g/size) * size;
            int roundb = (b/size) * size;

            int rr = r - roundr;
            int rg = g - roundg;
            int rb = b - roundb;
            return current + rr * slopeR + rg * slopeG + rb * slopeB;
        }

        @Override
        public String toString() {
            return String.format("[%.2f <%.2f, %.2f, %.2f>]", current, slopeR, slopeG, slopeB);
        }
    }

    /**
     * This class stores rounded r, g, b values to facilitate lookup from the {@link Screen#currentMap}
     */
    class RoundedRGB {
        final int roundr, roundg, roundb;

        RoundedRGB(int r, int g, int b) {
            this.roundr = (r/size) * size;
            this.roundg = (g/size) * size;
            this.roundb = (b/size) * size;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RoundedRGB rgb = (RoundedRGB) o;
            return roundr == rgb.roundr &&
                    roundg == rgb.roundg &&
                    roundb == rgb.roundb;
        }

        @Override
        public int hashCode() {
            return Objects.hash(roundr, roundg, roundb);
        }

        @Override
        public String toString() {
            return String.format("[%d, %d, %d]", roundr, roundg, roundb);
        }
    }

    public Screen() {
        super("SCREEN");
    }

    @Override
    protected boolean build(NodeList models) {
        try {
            Node screenmodel = XmlHelper.getNode("screenmodel", models);
            size = XmlHelper.getNodeAttInt("size", screenmodel);
            NodeList grids = screenmodel.getChildNodes();
            for(int i = 0; i < grids.getLength(); i ++) {
                Node grid = grids.item(i);
                int R = XmlHelper.getNodeAttInt("R", grid);
                int G = XmlHelper.getNodeAttInt("G", grid);
                int B = XmlHelper.getNodeAttInt("B", grid);

                /**
                 * Screen power model is not made symbolic since there are just way too many
                 * parameters in the screen power model. Making it symbolic will give our
                 * minimizer a really hard time. Moreover, we currently do not know of any "hidden"
                 * parameters that can impact screen model, thus having screen model symbolic
                 * is unnecessary.
                 */
                double current = XmlHelper.getNodeAttDouble("current", grid);
                double slopeR = XmlHelper.getNodeAttDouble("slopeR", grid);
                double slopeG = XmlHelper.getNodeAttDouble("slopeG", grid);
                double slopeB = XmlHelper.getNodeAttDouble("slopeB", grid);

                RoundedRGB rgb = new RoundedRGB(R, G, B);
                if(currentMap.containsKey(rgb)) {
                    logger.error("Duplicate model parameter for rgb {}", rgb);
                    return false;
                }
                currentMap.put(rgb, new OledCurrent(current, slopeR, slopeG, slopeB));
            }
            if(!verify())
                return false;

            avgCurrent = currentMap.values().parallelStream().mapToDouble(o -> o.current).average().getAsDouble();
            logger.info("Built Screen");
            return true;
        } catch (NullPointerException npe) {
            logger.error("Skipping. Failed to build screen");
            return false;
        }
    }

    private boolean verify() {
        for(int r = 0; r <= MAX_COLOR_VAL; r += size)
            for(int g = 0; g <= MAX_COLOR_VAL; g += size)
                for(int b = 0; b <= MAX_COLOR_VAL; b += size) {
                    RoundedRGB rgb = new RoundedRGB(r, g, b);
                    if (!currentMap.containsKey(rgb)) {
                        logger.error("Map doesn't contain current for r={}, g={}, b={}", r, g, b);
                        return false;
                    }
                }
        return true;
    }

    /**
     * Concatenate multiple input videos together to create one output video and return screen power events
     * obtained from processing frames
     *
     * @param screenOnTimesUs list of time pairs where screen was on. For example [(0,5), (10,15)] means screen was on from
     *                     0 to 5 and then from 10 to 15
     * @param inputPaths Path to multiple input videos
     * @param outputPath Path to concatenated output video
     * @throws FrameRecorder.Exception
     * @throws FrameGrabber.Exception
     * return screen PowerEvents from input screen video
     */
    public List<PowerEvent> appendVideos(List<Pair<Long, Long>> screenOnTimesUs,
                                         List<Path> inputPaths, Path outputPath)
            throws FrameRecorder.Exception, FrameGrabber.Exception {
        Preconditions.checkArgument(!inputPaths.isEmpty());

        final Pair<Long, Long> globalTimeUs = globals.getGlobalTimeUs();
        final List<PowerEvent> powerEvents = new LinkedList<>();

        /**
         * On OSX, we use opencv.VideoCapture to get width and height
         * If we use FFmpegFrameGrabber, the FrameRecorder.create crashes for some reason
        **/
        opencv_videoio.VideoCapture capture = new opencv_videoio.VideoCapture(inputPaths.get(0).toAbsolutePath().toString());
        int width = (int)capture.get(CAP_PROP_FRAME_WIDTH);
        int height = (int)capture.get(CAP_PROP_FRAME_HEIGHT);
        capture.release();

        if(width == 0 || height == 0) {
            /** Comes in here for linux */
            FrameGrabber frameGrabber = FrameGrabber.create(FFmpegFrameGrabber.class, File.class, inputPaths.get(0).toFile());
            frameGrabber.start();
            Frame frame = frameGrabber.grabFrame();
            width = frame.imageWidth;
            height = frame.imageHeight;
            frameGrabber.stop();
        }

        logger.info("width {} height {}", width, height);
        FrameRecorder recorder = FrameRecorder.create(FFmpegFrameRecorder.class, String.class,
                outputPath.toAbsolutePath().toString(), width, height);

        // To create a video that can be streamed to the timelines page on frontend
        recorder.setVideoCodec(AV_CODEC_ID_H264);
        recorder.setOption("movflags", "faststart");

        final double outFPS = recorder.getFrameRate();  // 30 by default
        recorder.start();

        long outFrameTimestamp = 0;

        // Iterate over all input videos
        for(Path inputPath: inputPaths) {
            FrameGrabber frameGrabber = FrameGrabber.create(FFmpegFrameGrabber.class, File.class, inputPath.toFile());
            frameGrabber.start();
            long lastFrameTimestamp = 0;

            // Grab a frame from input video
            Frame frame = frameGrabber.grabFrame();
            while(frame != null) {
                long grabbedFrameTimeStamp = frame.timestamp;
                Frame clone = frame.clone();
                frame = frameGrabber.grabFrame();

                if(grabbedFrameTimeStamp != 0) {
                    // Not the first frame of the video
                    if(grabbedFrameTimeStamp - lastFrameTimestamp < 1e6/outFPS)
                        // In case, input videos have higher fps than the output video,
                        // skip frames to accommodate for low fps in output
                        continue;

                    // There may be large delays between input video's frame timestamps when
                    // the screen is not updating, for example. We thus set the
                    // timestamp in the output video
                    outFrameTimestamp += (grabbedFrameTimeStamp - lastFrameTimestamp);
                    lastFrameTimestamp = grabbedFrameTimeStamp;
                    recorder.setTimestamp(outFrameTimestamp);
                }
                long timeUs = outFrameTimestamp + globalTimeUs.getLeft();
                double current = 0;
                if(timeHelper.withins(timeUs, screenOnTimesUs) ) {
                    // Screen on
                    recorder.record(clone);
                    current = currentFromFrame(clone);
                } else {
                    // Screen off
                    recorder.record(videoUtil.getBlackFrame(width, height));
                }
                if(timeHelper.within(timeUs, globalTimeUs)) {
                    PowerEvent pe = new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(current), timeUs);
                    powerEvents.add(pe);
                }

                outFrameTimestamp = recorder.getTimestamp() - (long)(1e6/outFPS);
            }
            frameGrabber.stop();
        }
        recorder.stop();

        return powerEvents;
    }

    @Override
    protected Map<Integer, List<PowerEvent>> parseLogs() throws IOException {
        brightnessRatio = Double.parseDouble(propsReader.getProperty("brightness", "255")) / 255.0;

        Path traceDir = globals.getTraceDir();
        Path outDir = globals.getOutDir();

        // Append screen videos
        int i = 0;
        List<Path> screenFiles = new LinkedList<>();
        while (true) {
            Path screenVideo = traceDir.resolve(String.format("screen%d.mp4", i++));
            File screenFile = screenVideo.toFile();
            if (!screenFile.exists())
                break;
            screenFiles.add(screenVideo);
        }
        Path outputVideo = outDir.resolve("screen.mp4");

        List<Pair<Long, Long>> screenOnTimesUs = globals.getTaskDataMap().get(PHONE_PID).getEvents()
                .filter(e -> e.eventName.equals("screen"))
                .map(e -> Pair.of((long) (e.getStartTimeMs() * 1e3), (long)(e.getEndTimeMs() * 1e3)))
                .collect(toList());

        List<PowerEvent> screenPowerEvents;
        if(!screenFiles.isEmpty()) {
            screenPowerEvents = appendVideos(screenOnTimesUs, screenFiles, outputVideo);
        } else {
            screenPowerEvents = screenOnTimesUs.stream().flatMap(e -> screenEventToPowerEvents(e)).collect(toList());
        }
        Map<Integer, List<PowerEvent>> taskCurrents = new HashMap<>();

        taskCurrents.put(PHONE_PID, screenPowerEvents);
        taskCurrents = powerEventsToFgTasks.accountTaskEnergy(taskCurrents);
        taskCurrents = movingAvgSamplingCollector.movingAverageByTasks(taskCurrents);
        return taskCurrents;
    }

    private Stream<PowerEvent> screenEventToPowerEvents(Pair<Long, Long> e) {
        return Arrays.stream(new PowerEvent[] {
                new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(avgCurrent), e.getLeft()),
                new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(0), e.getRight()),
        });
    }

    final double RATIO = 0.1;
    final OpenCVFrameConverter.ToMat converter = new OpenCVFrameConverter.ToMat();

    /**
     * Returns current consumed by frame by applying power model to the frame
     * @param frame
     * @return
     */
    private double currentFromFrame(Frame frame) {
        opencv_core.Mat matF = converter.convert(frame);
        opencv_core.Mat mat = new opencv_core.Mat();

        // Resize the frame to 10% on both height and width so we'll have to
        // process lesser number of pixels to apply the power model
        resize(matF, mat, new opencv_core.Size(), RATIO, RATIO, INTER_NEAREST);
        double current = 0;
        UByteRawIndexer idx = mat.createIndexer();
        for ( int i=0; i<mat.rows(); i++) {
            for ( int j=0; j<mat.cols(); j++) {
                int r = idx.get(i, j, 2);
                int g = idx.get(i, j, 1);
                int b = idx.get(i, j, 0);
                RoundedRGB rgb = new RoundedRGB(r, g, b);
                OledCurrent oledCurrent = currentMap.get(rgb);
                current += oledCurrent.getCurrent(r, g, b);
            }
        }

        // The currents in power model are assuming that the entire frame color was of the same color
        // so we normalize the total current computed above by the area (number of pixels)
        double frameCurrent = current/mat.size().area();

        // Scale the frame's current according to the brightness
        RoundedRGB base = new RoundedRGB(0,0,0);
        OledCurrent baseCurrent = currentMap.get(base);
        return brightnessRatio * (frameCurrent - baseCurrent.current) + baseCurrent.current;
    }
}
