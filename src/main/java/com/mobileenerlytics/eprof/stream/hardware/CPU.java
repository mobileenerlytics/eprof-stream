package com.mobileenerlytics.eprof.stream.hardware;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.util.PowerSumCalculator;
import com.mobileenerlytics.eprof.stream.util.XmlHelper;
import com.mobileenerlytics.eprof.stream.writers.CsvWriter;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

@Component
public class CPU extends HardwareComponent {
    private final Logger logger = LoggerFactory.getLogger(CPU.class.getSimpleName());
    //    private int mNumOfCores;

    @VisibleForTesting
    Core[] cores;

    @Autowired
    private Globals globals;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private PowerSumCalculator powerSumCalculator;

    @Autowired
    private CsvWriter csvWriter;

    @Autowired
    private DoubleFactory doubleFactory;

    protected CPU() {
        super("CPU");
    }

    @Override
    public boolean build(NodeList models) {
        try {
            Node cpumodel = XmlHelper.getNode("cpumodel", models);

            // Read num of cores and initialize fields
            int numOfCores = XmlHelper.getNodeAttInt("corenum", cpumodel);
            logger.debug("{} cores", numOfCores);
            cores = new Core[numOfCores];

            for (int i = 0; i < numOfCores; i++)
                cores[i] = ctx.getBean(Core.class, i, numOfCores);

            NodeList coreGroups = cpumodel.getChildNodes();
            Verify.verify(coreGroups.getLength() > 0);
            // Read frequencies
            for(int cg = 0; cg < coreGroups.getLength(); cg ++) {
                Node coreGroup = coreGroups.item(cg);
                NodeList coreGroupChild = coreGroup.getChildNodes();
                Node freqsNode = XmlHelper.getNode("freqs", coreGroupChild);
                NodeList freqList = freqsNode.getChildNodes();

                int numFreq = freqList.getLength();
                long freqs[] = new long[numFreq];
                {
                    for (int i = 0; i < numFreq; i++) {
                        Node freqNode = freqList.item(i);
                        long freq = XmlHelper.getNodeAttLong("val", freqNode);
                        if (i > 0)
                            Verify.verify(freqs[i - 1] < freq,
                                    "Frequencies should be sorted. Prev. {}. Now {}",
                                    freqs[i - 1], freq);
                        freqs[i] = freq;
                        logger.debug("freq[{}] {}", i, freq);
                    }
                }

                {    // Read power model
                    Node coresNode = XmlHelper.getNode("cores", coreGroupChild);
                    MyDouble idleCurrent = doubleFactory.newSymbolicUnmodifiableDouble(
                            XmlHelper.getNodeAttDouble("idle", coresNode), "Core_idle");

                    for(Core core: cores)
                        core.setIdleCurrent(idleCurrent);

                    NodeList coresList = coresNode.getChildNodes();

                    for (int i = 0; i < coresList.getLength(); i++) {
                        Node coreNode = coresList.item(i);
                        int id = XmlHelper.getNodeAttInt("id", coreNode);
                        NodeList statesList = coreNode.getChildNodes();
                        for (int s = 0; s < statesList.getLength(); s++) {
                            Node stateNode = statesList.item(s);
                            cores[id].addState(stateNode);
                        }
                        // Verify that each core now has states for each frequency
                        cores[id].verifyFreqs(freqs);
                    }
                }
            }
//            rt = new CpuTracesReader(trace, cpufsms);
            logger.info("Built CPU");
//            Energy.enableComponent(EComponent.CPU);
            return true;
        } catch (NullPointerException npe) {
            // WE are being lazy and not checking if any of the node is null.
            // We just catch them all here.
            logger.warn("Failed to build CPU!");
            return false;
        }
    }

    List<Map<Integer, List<PowerEvent>>> taskCurrents;

    @Override
    public Map<Integer, List<PowerEvent>> parseLogs()
            throws ExecutionException, InterruptedException {
        taskCurrents = new LinkedList<>();
        List<Future<Map<Integer, List<PowerEvent>>>> coreFutures = new LinkedList<>();
        for (Core core : cores) {
//        Core core = cores[3];
            Future<Map<Integer, List<PowerEvent>>> future = ForkJoinPool.commonPool()
                    .submit(() -> core.parseLogs());
            coreFutures.add(future);
        }
        for (Future<Map<Integer, List<PowerEvent>>> f : coreFutures)
            taskCurrents.add(f.get());

        return powerSumCalculator.sumPowerByTasks(taskCurrents);
    }

    @Override
    protected void dump(Map<Integer, List<PowerEvent>> totalCurrents) throws ExecutionException, InterruptedException {
        for (int i = 0; i < cores.length; i++) {
            final int coreIndex = i;
            ForkJoinPool.commonPool().submit(() -> {
                try {
                    cores[coreIndex].dump(taskCurrents.get(coreIndex));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).get();
        }
        try {
            Path outDir = globals.getOutDir();
            Path freqCsvPath = outDir.resolve("CPU.csv");
            csvWriter.writePowerCsv(freqCsvPath, totalCurrents, globals.getGlobalTimeUs());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
