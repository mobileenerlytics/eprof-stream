package com.mobileenerlytics.eprof.stream.writers;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.util.GsonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;

@Component
public class AppJsonWriter {
    @Autowired
    Globals globals;

    @Autowired
    private GsonHelper gsonHelper;

    public void write(Map<Integer, TaskData> taskDataMap) throws IOException {
        // Dump to app.json
        Path outDir = globals.getOutDir();
        Path appJsonPath = outDir.resolve("app.json");
        Files.deleteIfExists(appJsonPath);
        Files.createFile(appJsonPath);

        List<TaskData> outTasks = taskDataMap.values().stream()
                .sorted(comparingInt(td -> td.taskId))
                .collect(toList());
        Files.write(appJsonPath, gsonHelper.toJsonBytes(outTasks));
    }
}
