package com.mobileenerlytics.eprof.stream.writers;

import com.mobileenerlytics.eprof.stream.util.FileOps;
import com.mobileenerlytics.symbolic.IEvent;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Component
public class CsvWriter {
    public <T extends IEvent, K> void writePowerCsv(Path csvPath, Map<K, List<T>> powerEventsMap, Pair<Long, Long> globalTime)
            throws IOException {
        Function<T, String> formatter = e -> String.format("%d, %s", (e.getTimeUs() - globalTime.getLeft())/1000L, e.getValue());
        writeCsv(csvPath, powerEventsMap, globalTime.getLeft(), formatter);
    }

    public <T extends IEvent, K> void writeCsv(Path csvPath, Map<K, List<T>> eventsMap, long startTime, Function<T, String> formatter)
            throws IOException {
        Files.deleteIfExists(csvPath);
        Files.createFile(csvPath);

        try (FileWriter fw = new FileWriter(csvPath.toFile(), true)) {
            for (Map.Entry<K, List<T>> entry : eventsMap.entrySet()) {
                K key = entry.getKey();
                List<T> events = entry.getValue();
                if(events.isEmpty())
                    continue;
                fw.append(String.format("%s : {%n", key.toString()));

                if(events.get(0).getTimeUs() != startTime)
                    fw.append(String.format("0, 0%n"));

                events.stream().forEachOrdered(s -> {
                    FileOps.writeToFile(fw, formatter.apply(s));
                });
                fw.append("}\n");
            }
        }
    }
}
