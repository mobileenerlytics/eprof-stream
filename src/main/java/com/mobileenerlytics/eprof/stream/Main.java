package com.mobileenerlytics.eprof.stream;

import com.mobileenerlytics.eprof.stream.batterystats.BatteryStatsReader;
import com.mobileenerlytics.eprof.stream.hardware.Handset;
import com.mobileenerlytics.eprof.stream.hardware.HandsetFactory;
import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.ps.PsReader;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.readers.LogcatReader;
import com.mobileenerlytics.eprof.stream.readers.PropsReader;
import com.mobileenerlytics.eprof.stream.strace.StraceReader;
import com.mobileenerlytics.eprof.stream.util.AtraceHelper;
import com.mobileenerlytics.eprof.stream.writers.AppJsonWriter;
import com.mobileenerlytics.symbolic.DoubleFactory;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static java.util.stream.Collectors.joining;

@Component
public class Main {
    @Autowired
    private BatteryStatsReader batteryStatsReader;

    @Autowired
    private PsReader psReader;

    @Autowired
    private PropsReader propsReader;

    @Autowired
    private LogcatReader logcatReader;

    @Autowired
    private StraceReader straceReader;

    @Autowired
    private Globals globals;

    @Autowired
    private HandsetFactory handsetFactory;

    @Autowired
    private AtraceHelper atraceHelper;

    @Autowired
    private AppJsonWriter appJsonWriter;

    @Autowired
    private DoubleFactory doubleFactory;

    public void main() throws IOException, ParserConfigurationException, SAXException,
            ExecutionException, InterruptedException {
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();

        globals.setContext(context);

        atraceHelper.findTimeOffset();

        Map<Integer, TaskData> taskDataMap = psReader.read();
        globals.setTaskDataMap(taskDataMap);

        propsReader.read();
        globals.setProps();
        propsReader.writeGlobalProps();

        batteryStatsReader.read();
        batteryStatsReader.dumpBugreport();

        Handset handset = handsetFactory.buildHandset();
        List<PowerEvent> errorPowerEvents = handset.parseLogs();

        // Dump to app.json
        appJsonWriter.write(taskDataMap);
        logcatReader.write();
        straceReader.write();
        doubleFactory.finish(errorPowerEvents);

        stopwatch.stop();
        System.out.println(stopwatch.getTime());
    }

    static ApplicationContext context;
    public static void main(String args[]) throws InterruptedException, ParserConfigurationException, SAXException, ExecutionException, IOException {
        context = new ClassPathXmlApplicationContext("META-INF/config.xml");
        String profiles = Arrays.stream(context.getEnvironment().getActiveProfiles()).collect(joining(", "));
        System.out.println("Active profiles: " + profiles);
        Main m = context.getBean(Main.class);
        m.main();
    }

}
