package com.mobileenerlytics.eprof.stream.ps;

import com.google.gson.annotations.Expose;
import com.mobileenerlytics.eprof.stream.batterystats.EventData;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Component
@Scope(value = "prototype")
public class TaskData implements Comparable<TaskData> {
    public static final int UNKNOWN_PID = -1;
    public static final int PHONE_PID = -2;
    public static final int SENSOR_PID = -3;
    public static final int ERROR_PID = -4;
    private List<EventData> events;

    @Autowired
    ApplicationContext context;

    @Autowired
    private DoubleFactory doubleFactory;

    @Expose
    public int taskId;
    @Expose
    public String taskName;
    @Expose
    public String userName;
    @Expose
    boolean mIsProc = false;
    @Expose
    public Integer parentTaskId;

    @Expose
    final public Map<String, MyDouble> perComponentEnergy;

    public TaskData(int taskId, int parentTaskId, String taskName, String userName) {
        this.taskId = taskId;
        this.parentTaskId = parentTaskId;
        this.taskName = taskName;
        this.userName = userName;
        perComponentEnergy = new HashMap<>();
    }

    public boolean isProc() {
        return mIsProc;
    }

    public EventData addEvent(long timeMs, String eventName, String name) {
        if (events == null)
            events = new LinkedList<>();
        EventData eventData = context.getBean(EventData.class,this, eventName, name, timeMs);
        events.add(eventData);
        return eventData;
    }

    public Stream<EventData> getEvents() {
        if (events == null)
            return Stream.empty();
        return events.stream();
    }

    public MyDouble totalEnergy() {
        MyDouble total = doubleFactory.newSymbolicDouble(0);
        perComponentEnergy.values().stream().forEach(e -> total.add(e));
        return total;
    }

    @Override
    public int compareTo(TaskData td2) {
        return (taskId <= td2.taskId) ? -1 : 1;
    }

    public void setIsProc() {
        mIsProc = true;
    }
}