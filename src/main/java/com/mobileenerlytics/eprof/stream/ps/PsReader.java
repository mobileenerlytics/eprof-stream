package com.mobileenerlytics.eprof.stream.ps;

import com.google.common.base.Verify;
import com.google.common.collect.Sets;
import com.mobileenerlytics.eprof.stream.Globals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.*;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

@Component
public class PsReader {
    private static final Logger logger = LoggerFactory.getLogger(PsReader.class.getSimpleName());
    Set<Integer> parentProcs = Sets.newHashSet(0, 1, 2);
    private ConcurrentMap<Integer, TaskData> tasks;

    @Autowired
    Globals globals;

    @Autowired
    ApplicationContext context;

    @Autowired
    List<IFixTasks> bestEffortFindTaskParents;

    private AtomicInteger procParentFixesApplied = new AtomicInteger(0);
    private AtomicInteger numProcesses = new AtomicInteger(0);

    public Map<Integer, TaskData> read() {
        Path traceDir = globals.getTraceDir();
        Path psPath = traceDir.resolve("ps_out");
        // Create task id to tasks mapping

        tasks = new ConcurrentHashMap<>();
        tasks.put(0, context.getBean(TaskData.class, 0, 0, "init", "root"));
        tasks.put(UNKNOWN_PID, context.getBean(TaskData.class, UNKNOWN_PID, UNKNOWN_PID, "unknown", "root"));
        tasks.put(PHONE_PID, context.getBean(TaskData.class, PHONE_PID, PHONE_PID, "Phone", "root"));
        tasks.put(SENSOR_PID, context.getBean(TaskData.class, SENSOR_PID, PHONE_PID, "Current Sensor", "root"));


        try {
            ConcurrentMap<Integer, TaskData> psMap = Files.lines(psPath)
                    .skip(1)
                    .map(this::parseToTaskData)
                    .collect(toMap(td -> td.taskId,
                            identity(),
                            (a, b) -> {
                                throw new RuntimeException("Found two processes in ps_out with same id" + a.taskId);
                            },
                            ConcurrentHashMap::new));

            tasks.putAll(psMap);

            // Find zygote processes
            List<Integer> zygoteProcs = tasks.values().parallelStream()
                    .filter(td -> td.taskName.equals("zygote")
                            || td.taskName.equals("zygote64")
                            || td.taskName.startsWith("daemonsu")
                    )
                    .map(td -> td.taskId)
                    .collect(Collectors.toList());
            Verify.verify(!zygoteProcs.isEmpty(), "zygote proc wasn't found in ps");
            parentProcs.addAll(zygoteProcs);
        } catch (IOException ioe) {
            logger.error("No ps file found. Task wise breakdown may not work");
        }

        // If parent is zygote or 0, 1 ,2-- make self as parent. used later to identify processes
        tasks.values().parallelStream()
                .filter(td -> parentProcs.contains(td.parentTaskId))
                .forEach(td -> td.parentTaskId = td.taskId);

        // Assign parents
        tasks.values().parallelStream()
                .forEach(this::fixParentOfPsTask);

        // Try best effort to find more tasks and fill their parents
        for(IFixTasks fixTasks: bestEffortFindTaskParents)
            fixTasks.findAndFixTasks(tasks);

        // Mark processes
        tasks.values().parallelStream()
                .filter(td -> td.taskId == td.parentTaskId)
                .forEach(td -> {
                    numProcesses.incrementAndGet();
                    td.mIsProc = true;
                });
        logger.info("Found {} processes in {} tasks", numProcesses.get(), tasks.size());

        // Parent of a task should not be a thread. It should be a processes
        tasks.values().parallelStream()
                .forEach(this::fixParentShouldBeProc);
        logger.info("Fixed parents of {} tasks", procParentFixesApplied.get());

        return tasks;
    }

    private TaskData parseToTaskData(String strLine) {
        String[] tokens = strLine.split("\\s+");
        String userName = tokens[0];
        int pid = Integer.parseInt(tokens[1]);
        int ppid = UNKNOWN_PID;
        String procName = "";
        try {
            ppid = Integer.parseInt(tokens[2]);
            procName = getProcName(tokens);
        } catch (ArrayIndexOutOfBoundsException aiobe) {
            // Sometimes ps_out is malformed and is missing the parentTask.
            // :'(
            logger.warn("Missing parent task for {}", pid);
        }
        return context.getBean(TaskData.class, pid, ppid, procName, userName);
    }

    private String getProcName(String[] tokens) {
        // Idiocy ! NAME can have spaces "GL Updater" "Signal Catcher"
        // !! Name may even be empty giving just 3 tokens :'(
        String procName = "";
        if (tokens.length >= 4) {
            procName = tokens[3];
            procName = procName + IntStream.range(4, tokens.length) .mapToObj(i -> tokens[i])
                                    .collect(joining(" "));
        }
        if (procName.equals("system_server")) {
            // Idiotic - app is called system_process, but in ps it
            // is shown as system_server
            procName = "system_process";
        }
        return procName;
    }

    private void fixParentOfPsTask(TaskData td) {
        if (td.parentTaskId == 0) return;

        if (tasks.containsKey(td.parentTaskId)) {
            return;
        }

        logger.info("Found a task from potentially broken ps line {}-{} ({})",
                td.taskName, td.taskId, td.parentTaskId);

        // :'( This may happen because of bad shape of ps output
        // A sample ps_out trace--

        /** root      27830 1     app_process
         * root      27887 27830 FinalizerWatchd
         * root      27888 27
         * app_30    30036 2249  TooboxThread #1
         */

        // It comes here at "root 27888 27" since it is unable to find task 27,
        // but in reality, the actual line was "root 27888 27830 proc".
        // We try best effort to fix this here.
        String broken_ppid = String.valueOf(td.parentTaskId);
        td.parentTaskId = tasks.values().parallelStream()
                .filter(ptd -> ptd.mIsProc) // Parent must be a process
                .filter(ptd -> ptd.userName.equals(td.userName)) // Parent should have same username
                .filter(ptd -> String.valueOf(ptd.taskId).startsWith(broken_ppid))
                .mapToInt(ptd -> ptd.taskId)
                .filter(taskId -> taskId < td.taskId) // Parent appears before me
                .reduce((first, second) -> second) // Gets the last element
                .orElse(UNKNOWN_PID); // return unknown task if none found
    }

    /**
     * Sometimes a thread's parent might be another thread due to problems in reconstructing tasks.
     * @param taskData
     */
    private void fixParentShouldBeProc(TaskData taskData) {
        if (taskData.isProc())
            return;

        if(! tasks.containsKey(taskData.parentTaskId)) {
            logger.warn("Didn't observe parent {} of TaskData {}-{}. Marking parent as unknown",
                    taskData.parentTaskId, taskData.taskName, taskData.taskId);
            taskData.parentTaskId = UNKNOWN_PID;
            return;
        }

        TaskData originalParent = tasks.get(taskData.parentTaskId);
        if(originalParent.isProc())
            return;

        TaskData p = originalParent;
        while (p != null && !p.isProc()) {
            p = tasks.get(p.parentTaskId);
        }
        if(p != null) {
            taskData.parentTaskId = p.taskId;
            logger.warn("TaskData {}-{} had a thread parent {}-{}. Fixed to {}-{}", taskData.taskName, taskData.taskId,
                    originalParent.taskName, originalParent.taskId, p.taskName, p.taskId);
            procParentFixesApplied.incrementAndGet();
        } else {
            taskData.parentTaskId = UNKNOWN_PID;
            logger.warn("TaskData {}-{} had a thread parent {}-{}. Changed to unknown", taskData.taskName, taskData.taskId,
                    originalParent.taskName, originalParent.taskId);
        }
    }
}
