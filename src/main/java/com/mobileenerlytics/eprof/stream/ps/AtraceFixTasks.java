package com.mobileenerlytics.eprof.stream.ps;

import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.StreamSupport;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.UNKNOWN_PID;

/**
 * ps_out is imperfect as it misses tasks. This class parses each atrace line to patch up
 * tasks that were missed by ps.
 */
@Component
public class AtraceFixTasks implements IFixTasks {
    @Autowired
    ApplicationContext ctx;

    Logger logger = LoggerFactory.getLogger(AtraceFixTasks.class.getSimpleName());

    // Thread safe counters
    AtomicInteger newTasks = new AtomicInteger(0);
    AtomicInteger foundParents = new AtomicInteger(0);

    @Override
    public void findAndFixTasks(ConcurrentMap<Integer, TaskData> tasks) {
        StreamSupport.stream(ctx.getBean(DbgSpliterator.class), true)
                .forEach(s -> {
                    if(! findTaskAndFixParent(s, tasks))
                        findUnknownTasks(s, tasks);
                });
        logger.info("Found {} new tasks and fixed {} parents", newTasks.get(), foundParents.get());
    }

    /**
     * Pattern extracts task name, task id and process id from an atrace line
     * Example--   EnsureDelegate-3786  ( 3767)
     *             ^^^^^^^^^^^^^^-^^^^  ( ^^^^)
     *                 Task name   ID     PID
     */
    private final Pattern patternWithPid = Pattern.compile("^(.*?)-(\\d{1,5})\\s+?\\(\\s{0,4}(\\d{1,5})\\) ");

    private boolean findTaskAndFixParent(String s, ConcurrentMap<Integer, TaskData> tasks) {
        Matcher matcher = patternWithPid.matcher(s);
        if(!matcher.find())
            return false;

        String taskName = matcher.group(1).trim();
        int taskId = Integer.parseInt(matcher.group(2));
        int parentTaskId = Integer.parseInt(matcher.group(3));

        if(!tasks.containsKey(taskId)) {
            // We've found a new task that was not observed in ps_out
            String user = "unknown";
            if(tasks.containsKey(parentTaskId))
                user = tasks.get(parentTaskId).userName;
            tasks.put(taskId, ctx.getBean(TaskData.class, taskId, parentTaskId, taskName, user));
            logger.debug("Found new task {}, {} in proc {}, user {}", taskId, taskName, parentTaskId, user);
            newTasks.incrementAndGet();
        } else {
            TaskData taskData = tasks.get(taskId);
            if(taskData.parentTaskId != parentTaskId) {
                logger.debug("Update parent task of {} from {} to {}", taskId, taskData.parentTaskId, parentTaskId);
                taskData.parentTaskId = parentTaskId;
                foundParents.incrementAndGet();
            }
        }
        return true;
    }

    /**
     *    HandlerThread-12622 [003] ...2 14017.848425: sched_switch: prev_pid=12622 next_pid=0
     */
    private final Pattern patternWithoutPid = Pattern.compile("^(.*?)-(\\d{1,5}) ");
    private void findUnknownTasks(String s, ConcurrentMap<Integer, TaskData> tasks) {
        Matcher matcher = patternWithoutPid.matcher(s);
        if(!matcher.find())
            return;

        String taskName = matcher.group(1).trim();
        int taskId = Integer.parseInt(matcher.group(2));
        if(tasks.containsKey(taskId))
            return;

        logger.warn("Found an unknown task {}, {}", taskId, taskName, UNKNOWN_PID, "unknown");
        tasks.put(taskId, ctx.getBean(TaskData.class, taskId, UNKNOWN_PID, taskName, "unknown"));
        newTasks.incrementAndGet();
    }
}
