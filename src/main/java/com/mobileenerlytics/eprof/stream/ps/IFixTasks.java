package com.mobileenerlytics.eprof.stream.ps;

import java.util.concurrent.ConcurrentMap;

/**
 * ps_out is imperfect as it misses tasks. Implementors of this interface
 * attempt to use other input trace files such as atrace to find missing
 * tasks and fix them. They can fix various properties such as a process
 * might be incorrectly marked as a thread, the parent task of a task
 * might be incorrect etc.
 */
public interface IFixTasks {

    /**
     * Find tasks and fix them
     * @param tasks Tasks that have been found so far. This map might
     *              get modified by this call to add more tasks.
     */
    void findAndFixTasks(ConcurrentMap<Integer, TaskData> tasks);
}
