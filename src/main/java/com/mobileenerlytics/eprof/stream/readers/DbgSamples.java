package com.mobileenerlytics.eprof.stream.readers;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.util.AtraceHelper;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.writers.CsvWriter;
import com.mobileenerlytics.symbolic.IEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toMap;

@Component
public class DbgSamples {
    @Autowired
    private Globals globals;

    @Autowired
    private AtraceHelper atraceHelper;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    CsvWriter csvWriter;

    static class Sample implements IEvent<Integer> {
        final long time;
        int numSamples;

        Sample(long time, int numSamples) {
            this.time = time;
            this.numSamples = numSamples;
        }

        @Override
        public long getTimeUs() {
            return time;
        }

        @Override
        public Integer getValue() {
            return numSamples;
        }
    }

    public void read() throws IOException {
        Path outDir = globals.getOutDir();
        long startTime = globals.getGlobalTimeUs().getLeft();

        long windowSizeMs = (long) (MovingAvgSamplingCollector.windowSizeUs(globals.getGlobalTimeUs())/1e3);
        Map<Long, Sample> count = StreamSupport.stream(ctx.getBean(DbgSpliterator.class), false)
                .filter(s -> s.contains("sched_switch:"))
                .filter(l -> atraceHelper.inBounds(l, globals.getGlobalTimeUs()))
                .map(l -> new Sample((atraceHelper.getTime(l) - startTime)/1000, 1))
                .collect(toMap(
                        s -> (s.getTimeUs() / windowSizeMs),
                        Function.identity(),
                        (a, b) -> {
                            a.numSamples += b.numSamples;
                            return a;
                        },
                        TreeMap::new
                ));

        Map<Integer, List<Sample>> sampleCount = new HashMap<Integer, List<Sample>>() {{
            put(TaskData.PHONE_PID, new LinkedList<>(count.values()));
        }};

        Path samplesFile = outDir.resolve("samples.csv");
        Function<Sample, String> formatter = e -> String.format("%d, %d", e.getTimeUs(), e.getValue());
        csvWriter.writeCsv(samplesFile, sampleCount, 0, formatter);
    }
}
