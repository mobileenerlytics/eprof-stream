package com.mobileenerlytics.eprof.stream.readers;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.util.GsonHelper;
import com.mobileenerlytics.eprof.stream.util.TimeHelper;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Component
public class LogcatReader {
    private static final String LOGCAT = "logcat_out";
    private static final String LOGCAT_JSON = "logcat.json";
    private static Logger logger = LoggerFactory.getLogger(LogcatReader.class.getSimpleName());

    @Autowired
    Globals globals;

    @Autowired
    TimeHelper timeHelper;

    private static final Pattern pattern_logcat = Pattern.compile("(\\d+)-(\\d+) (\\d+):(\\d+):(\\d+).(\\d+)");

    @Autowired
    private GsonHelper gsonHelper;

    private static OptionalLong parseLogcatTimeUs(String s, ZoneOffset zoneOffset) {
        Matcher matcher = pattern_logcat.matcher(s);
        if (matcher.find()) {
            int month = Integer.parseInt(matcher.group(1));
            int day = Integer.parseInt(matcher.group(2));
            int hour = Integer.parseInt(matcher.group(3));
            int minute = Integer.parseInt(matcher.group(4));
            int second = Integer.parseInt(matcher.group(5));
            int nanosecond = 1000_000 * Integer.parseInt(matcher.group(6));
            long timeStampMs = TimeUnit.MILLISECONDS.convert(
                    LocalDateTime.of(Year.now().getValue(), month, day, hour, minute, second, nanosecond)
                            .toEpochSecond(zoneOffset), TimeUnit.SECONDS);
            return OptionalLong.of(1000 * timeStampMs);
        }
        logger.warn("Failed to parse time in {}", s);
        return OptionalLong.empty();
    }

    private static final Pattern pattern_proc = Pattern.compile("(\\d+)\\):");

    private static OptionalInt parseProc(String s) {
        Matcher matcher = pattern_proc.matcher(s);
        if (matcher.find()) {
            return OptionalInt.of(Integer.parseInt(matcher.group(1)));
        }
        logger.warn("Failed to parse process in {}", s);
        return OptionalInt.empty();
    }

    public void write() throws IOException {
        Path traceDir = globals.getTraceDir();
        Path outDir = globals.getOutDir();
        Pair<Long, Long> globalTime = globals.getGlobalTimeUs();
        Map<Integer, TaskData> taskDataMap = globals.getTaskDataMap();

        ZoneOffset zoneOffset = globals.getZoneId().getRules().getOffset(Instant.now());

        Path logcatPath = traceDir.resolve(LOGCAT);
        Path outputPath = outDir.resolve(LOGCAT_JSON);
        long startTime = globalTime.getLeft();
        Map<Integer, TreeMap<Long, List<String>>> procLogs = Files.lines(logcatPath)
                .filter(s -> shouldAddToOutput(s, globalTime, taskDataMap, zoneOffset))
                .collect(groupingBy(s -> parseProc(s).getAsInt(),
                        groupingBy(s -> (parseLogcatTimeUs(s, zoneOffset).getAsLong() - startTime)/1000, TreeMap::new, toList())));


        TreeMap<Long, List<String>> phoneLogs = Files.lines(logcatPath)
                .filter(s -> shouldAddToOutput(s, globalTime, taskDataMap, zoneOffset))
                .collect(groupingBy(s -> (parseLogcatTimeUs(s, zoneOffset).getAsLong() - startTime)/1000, TreeMap::new, toList()));
        procLogs.put(PHONE_PID, phoneLogs);

        Files.write(outputPath, gsonHelper.toJsonBytes(procLogs));
    }

    private boolean shouldAddToOutput(String s, Pair<Long, Long> globalTime, Map<Integer, TaskData> taskDataMap, ZoneOffset zoneOffset) {
        OptionalLong time = parseLogcatTimeUs(s, zoneOffset);
        if (!time.isPresent())
            return false;
        if (!timeHelper.within(time.getAsLong(), globalTime)) {
//            logger.warn("Ignoring {} since time {} not in range ({}-{})",
//                    s, time.getAsLong(), globalTime.getLeft(), globalTime.getRight());
            return false;
        }

        OptionalInt optionalProc = parseProc(s);
        if (!optionalProc.isPresent()) {
            return false;
        }
        int proc = optionalProc.getAsInt();
        if(!taskDataMap.containsKey(proc)) {
            logger.warn("Ignoring unrecognized process in " + s);
            return false;
        }
        if(!taskDataMap.get(proc).isProc()) {
            logger.warn("Logcat by a thread! " + s);
            return false;
        }
        return true;
    }
}
