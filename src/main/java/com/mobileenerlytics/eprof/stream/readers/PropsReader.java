package com.mobileenerlytics.eprof.stream.readers;

import com.mobileenerlytics.eprof.stream.Globals;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;

@Component
public class PropsReader {
    @Autowired
    Globals globals;

    Properties props = new Properties();

    public String getProperty(String key, String defaultValue) {
        return props.getProperty(key, defaultValue);
    }

    public String getProperty(String key) {
        return props.getProperty(key);
    }

    public void read() throws IOException {
        Path tracesDir = globals.getTraceDir();
        Path propPath = tracesDir.resolve("build.prop");
        try(FileReader reader = new FileReader(propPath.toFile())) {
            props.load(reader);
        }
    }

    public void writeGlobalProps() throws IOException {
        Path outDir = globals.getOutDir();
        Pair<Long, Long> globalTimeUs = globals.getGlobalTimeUs();
        String implVersion = this.getClass().getPackage().getImplementationVersion();
        if(implVersion == null)
            implVersion = "unknown";
        props.put("postproc.version", implVersion);
        props.put("postproc.time", "" + System.currentTimeMillis());
        props.put("startTime", "" + globalTimeUs.getLeft());
        props.put("stopTime", "" + globalTimeUs.getRight());
        Path propsFile = outDir.resolve("global.prop");
        try(FileWriter writer = new FileWriter(propsFile.toFile())) {
            props.store(writer, null);
        }
    }
}
