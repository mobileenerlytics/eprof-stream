package com.mobileenerlytics.eprof.stream.readers;

import com.mobileenerlytics.eprof.stream.Globals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.zip.GZIPInputStream;

@Component
@Scope(value="prototype")
public class DbgSpliterator extends Spliterators.AbstractSpliterator<String> implements AutoCloseable {
    private final LinkedList<BufferedReader> readers = new LinkedList<>();
    private BufferedReader currReader;

    @Autowired
    private Globals globals;

    @Autowired
    private ApplicationContext ctx;

    public DbgSpliterator() {
        super(Long.MAX_VALUE, 0);
    }

    public void initReaders() throws FileNotFoundException {
        Path traceDir = globals.getTraceDir();
        File dbgOut = traceDir.resolve("dbg_out").toFile();
        try {
            if (dbgOut.exists()) {
                readers.add(new BufferedReader(new FileReader(dbgOut)));
            } else {
                int ctr = 0;
                while (true) {
                    // energylogger must compress all the files and shouldn't have
                    // uncompressed files hanging around. We add this here <just in case> ^_^
                    File atraceUncFile = traceDir.resolve("atrace_unc." + ctr).toFile();
                    if (atraceUncFile.exists()) {
                        readers.add(new BufferedReader(new FileReader(atraceUncFile)));
                    } else {
                        File atraceFile = traceDir.resolve("atrace_" + ctr + ".gz").toFile();
                        if (!atraceFile.exists()) {
                            atraceFile = traceDir.resolve("atrace_z." + ctr).toFile();
                            if (!atraceFile.exists()) break;
                        }
                        readers.add(new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(atraceFile)))));
                    }
                    ctr++;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (readers.isEmpty())
            throw new FileNotFoundException("Neither dbg_out nor atrace_z files exist");

        currReader = readers.poll();
    }

    public String readLine() throws IOException {
        if(currReader == null)
            initReaders();
        if (currReader == null)
            return null;
        String line = null;
        line = currReader.readLine();
        while (line == null) {
            currReader = readers.poll();
            if (currReader == null)
                return null;
            line = currReader.readLine();
        }
        return line;
    }

    @Override
    public void close() throws IOException {
        for (Reader reader : readers)
            reader.close();
    }

    @Override
    public boolean tryAdvance(Consumer<? super String> action) {
        try {
            String line = readLine();
            if (line == null)
                return false;
            action.accept(line);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public int characteristics() {
        return ORDERED + IMMUTABLE + DISTINCT + NONNULL;
    }

    public DbgSpliterator trySplit() {
        int n = readers.size();
        if (n <= 1)
            return null;
        int splitIdx = n / 2;
        List<BufferedReader> subList = readers.subList(0, splitIdx);
        DbgSpliterator spliterator = ctx.getBean(DbgSpliterator.class);
        spliterator.readers.addAll(readers);
        spliterator.currReader = currReader;
        subList.clear();
        currReader = readers.poll();
        return spliterator;
    }
}
