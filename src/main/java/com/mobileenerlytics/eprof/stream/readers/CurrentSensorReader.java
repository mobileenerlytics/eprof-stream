package com.mobileenerlytics.eprof.stream.readers;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.exception.PhoneChargingException;
import com.mobileenerlytics.eprof.stream.hardware.HardwareComponent;
import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.util.TimeHelper;
import com.mobileenerlytics.eprof.stream.writers.CsvWriter;
import com.mobileenerlytics.symbolic.DoubleFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.SENSOR_PID;
import static java.util.stream.Collectors.toList;

@Component
public class CurrentSensorReader extends HardwareComponent {
    @Autowired
    Globals globals;

    @Autowired
    CsvWriter csvWriter;

    @Autowired
    MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Autowired
    TimeHelper timeHelper;

    @Autowired
    DoubleFactory doubleFactory;

    private static final Logger logger = LoggerFactory.getLogger(CurrentSensorReader.class.getSimpleName());

    protected CurrentSensorReader() {
        super("Current Sensor");
    }

    long prevTimeUs = 0;

    Optional<PowerEvent> parse(String s) {
        String[] tokens = s.split(",");
        long startTimeUs = globals.getGlobalTimeUs().getLeft();
        try {
            double currentMA = Double.parseDouble(tokens[1]) / (-1_000);  // Convert to positive number in mA
            if(currentMA < 0) {
                logger.warn("Negative current sensor value read {}", currentMA);
                throw new PhoneChargingException();
            }
            if(prevTimeUs == 0)
                prevTimeUs = startTimeUs + 1000 * Long.parseLong(tokens[0]);
            /**
             * All the PowerEvents generated from our power predictions are "future looking" i.e, we assume that the
             * phone component stays in this power until we have a new PowerEvent. Thus all the other processing done on
             * such PowerEvents make that assumption, from calculating moving average to how we display it in the
             * timelines. However, the current sensor readings are "past looking" i.e, at the time of sampling, the
             * sensor had updated the value we observed sometime in the past. We thus shift the current sensor readings
             * by 1 sample in the past to make it "future looking" and consistent with other PowerEvents in the code.
             */
            Optional<PowerEvent> optPowerEvent = Optional.of(new PowerEvent(SENSOR_PID,
                    doubleFactory.newUnmodifiableDouble(currentMA), prevTimeUs));
            prevTimeUs = startTimeUs + 1000 * Long.parseLong(tokens[0]);
            return optPowerEvent;
        } catch(NumberFormatException | ArrayIndexOutOfBoundsException npe) {
            logger.error("Bad string format '{}'", s);
            return Optional.empty();
        }
    }


    @Override
    protected boolean build(NodeList models) {
        return true;
    }

    @Override
    protected Map<Integer, List<PowerEvent>> parseLogs() {
        try {
            Path traceDir = globals.getTraceDir();
            Path currentSensorIn = traceDir.resolve("current_sensor");

            List<PowerEvent> powerEventList = Files.lines(currentSensorIn)
                    .map(this::parse)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .filter(pe -> timeHelper.within(pe.time, globals.getGlobalTimeUs()))
                    .collect(toList());
            Map<Integer, List<PowerEvent>> powerEventMap = new HashMap<Integer, List<PowerEvent>>() {{
                put(SENSOR_PID, powerEventList);
            }};
            powerEventMap = movingAvgSamplingCollector.movingAverageByTasks(powerEventMap);
            return powerEventMap;
        } catch(IOException | PhoneChargingException ioe) {
            logger.warn(ioe.toString());
        }
        return new HashMap<>();
    }
}
