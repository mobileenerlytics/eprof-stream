package com.mobileenerlytics.eprof.stream.readers;

import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.hardware.Core;
import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.util.AtraceHelper;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.StreamSupport;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static com.mobileenerlytics.eprof.stream.ps.TaskData.UNKNOWN_PID;
import static java.util.Collections.unmodifiableNavigableMap;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

@Component
public class CoreReader {
    @Autowired
    private Globals globals;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private AtraceHelper atraceHelper;

    private static final Logger logger = LoggerFactory.getLogger(CoreReader.class.getSimpleName());

    /**
     * Parse frequency, idle lines to get phone power events
     * @param core core for which we're reading the file
     * @return time sorted power events all belonging to "phone task"
     */
    public NavigableMap<Long, PowerEvent> getPhonePowerEvents(final Core core) {
        final long startTime = globals.getGlobalTimeUs().getLeft();
        final long stopTime = globals.getGlobalTimeUs().getRight();

        TreeMap<Long, PowerEvent> freqPhoneCurrentList = StreamSupport.stream(ctx.getBean(DbgSpliterator.class), false)
//                .filter(s -> DbgSpliterator.inBounds(s, globalTime))
                // don't filter here using inBounds.
                // current less than startTime is important for extractCurrents to work
                .filter(s -> s.contains("cpu_frequency: state="))
                .filter(s -> s.contains("cpu_id=" + core.idx()))
                .filter(s -> atraceHelper.getTime(s) < stopTime)
                .collect(toMap(s -> atraceHelper.getTime(s),
                        s -> new PowerEvent(PHONE_PID, core.getActiveCurrent(s), atraceHelper.getTime(s)),
                        (s1, s2) -> s2, // Pick the one observed last
                        TreeMap::new));

        if(freqPhoneCurrentList.isEmpty()) {
            PowerEvent startPE = new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(0), startTime);
            freqPhoneCurrentList.put(startTime, startPE);
        }

        // This doesn't have to be a treemap since we'll anyways add it to the freqPhoneCurrentList treemap constructed above
        Map<Long, PowerEvent> idlePhoneCurrentList = StreamSupport.stream(ctx.getBean(DbgSpliterator.class), false)
                .filter(s -> s.contains("[00" + core.idx() + "]")) // Filter my core
                .filter(s -> s.contains("sched_switch"))
                .filter(s -> s.contains("prev_pid=0") || s.endsWith("next_pid=0"))
                .filter(s -> atraceHelper.inBounds(s, Pair.of(freqPhoneCurrentList.firstKey(), stopTime)))
                .collect(toMap(s -> atraceHelper.getTime(s),
                        s -> new PowerEvent(PHONE_PID,
                                core.getIdleCurrent(s, freqPhoneCurrentList.floorEntry(atraceHelper.getTime(s)).getValue().current),
                                atraceHelper.getTime(s)),
                        (s1, s2) -> s2,     // Pick the one observed last
                        HashMap::new));

        freqPhoneCurrentList.putAll(idlePhoneCurrentList);

        // Put power events at start and stop time
        PowerEvent startPE = new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(0), startTime);
        if(freqPhoneCurrentList.floorEntry(startTime) != null) {
            startPE = freqPhoneCurrentList.floorEntry(startTime).getValue();
            startPE.time = startTime;
        }
        freqPhoneCurrentList.put(startTime, startPE);
        freqPhoneCurrentList.put(stopTime, new PowerEvent(PHONE_PID, DoubleFactory.newUnmodifiableDouble(0), stopTime));


        // Delete power events which are less than startTime
        freqPhoneCurrentList.headMap(startTime).clear();

        return unmodifiableNavigableMap(freqPhoneCurrentList);
    }

    /**
     * Parse context switch lines to get task power events.
     *
     * @param core parse traces and generate events for this core
     * @param phonePowerEventsList time sorted phone power events
     * @return List of PowerEvents indexed by task id. This doesn't get power events for the "phone task".
     */
    public Map<Integer, List<PowerEvent>> getTaskToPowerEventsListMap(final Core core, final NavigableMap<Long, PowerEvent> phonePowerEventsList) {
        Pair<Long, Long> globalTime = globals.getGlobalTimeUs();
        // Time sorted list of power events
        TreeMap<Long, PowerEvent> taskCurrentList = StreamSupport.stream(ctx.getBean(DbgSpliterator.class), false)
                .filter(s -> s.contains("[00" + core.idx() + "]")) // Filter my core
                .filter(s -> s.contains("sched_switch"))
                .filter(s -> atraceHelper.inBounds(s, globalTime))
//                .sorted(Comparator.comparingLong(s -> atraceHelper.getTime(s)))
                .map(s -> extractTaskCurrentFromSw(s, phonePowerEventsList))
                .collect(toMap(pe -> pe.getTimeUs(),
                        Function.identity(),
                        (s1, s2) -> s2, // Pick the one observed last
                        TreeMap::new));

        // Doesn't have to be a TreeMap since it will be merged with taskCurrentList above
        // which is a TreeMap
        Map<Long, PowerEvent> freqTaskCurrentList =  StreamSupport.stream(ctx.getBean(DbgSpliterator.class), false)
                .filter(s -> s.contains("[00" + core.idx() + "]")) // Filter my core
                .filter(s -> s.contains("cpu_frequency: state=") || s.contains("cpu_idle: state="))
                .filter(s -> atraceHelper.inBounds(s, globalTime))
                .map(s -> extractTaskCurrentFromFreq(s, phonePowerEventsList, taskCurrentList))
                .collect(toMap(pe -> pe.getTimeUs(), Function.identity(), (s1, s2) -> s2));

        taskCurrentList.putAll(freqTaskCurrentList);
        List<PowerEvent> completeTaskCurrentList = addZeroPEs(new LinkedList<>(taskCurrentList.values()));

        return completeTaskCurrentList.stream().collect(groupingBy(pe -> pe.getTaskId()));

    }

    /**
     * Returns PowerEvent on a task due to a change in frequency or idle etc
     * The task that is currently on the CPU Core will have a change in power due to change in
     * frequency of idle state.
     *
     * @param s Frequency or idle state change line from atrace
     * @param phonePowerEventsList time sorted power events of the whole phone
     * @param taskPowerEventsList time sorted power events for tasks (doesn't include the "phone task")
     * @return Corresponding PowerEvent for the task on the core at the time of frequency/idle change
     */
    private PowerEvent extractTaskCurrentFromFreq(String s, NavigableMap<Long, PowerEvent> phonePowerEventsList,
                                          NavigableMap<Long, PowerEvent> taskPowerEventsList) {
        Verify.verify(! s.contains("sched_switch"));
        long time = atraceHelper.getTime(s);
        //  It's a change in frequency or idle
        // The current of task on the core should change accordingly

        int taskId = UNKNOWN_PID;
        if(taskPowerEventsList.floorEntry(time) != null) {
            taskId = taskPowerEventsList.floorEntry(time).getValue().getTaskId();
            Verify.verify(taskId != PHONE_PID, "task list shouldn't contain phone task");
        }

        MyDouble current = DoubleFactory.newUnmodifiableDouble(0);
        if (phonePowerEventsList.floorEntry(time) != null) {
            PowerEvent pe = phonePowerEventsList.floorEntry(time).getValue();
            Verify.verify(pe.taskId == PHONE_PID, "phone list must only contain phone task");
            current = pe.current;
        }

        return new PowerEvent(taskId, current, time);
    }

    private static final Pattern p_next = Pattern.compile("next_pid=(\\d+)");
    /**
     * Returns power event given a context switch line
     * Only the task ctx switched in has the power event
     *
     * @param s line containing context switch
     * @param phonePowerEventsList time sorted list of PowerEvent of whole phone
     * @return
     */
    private PowerEvent extractTaskCurrentFromSw(String s, NavigableMap<Long, PowerEvent> phonePowerEventsList) {
        long time = atraceHelper.getTime(s);
        MyDouble current = DoubleFactory.newUnmodifiableDouble(0);
        if (phonePowerEventsList.floorEntry(time) != null) {
            PowerEvent pe = phonePowerEventsList.floorEntry(time).getValue();
            Verify.verify(pe.taskId == PHONE_PID);
            current = pe.current;
        }

        Matcher m_next = p_next.matcher(s);

        int taskId = UNKNOWN_PID;
        if(m_next.find()) {
            // Found taskId
            taskId = Integer.parseInt(m_next.group(1));
        } else {
            // This is guaranteed to be a context switch line since we filtered for it
            logger.warn("Will account to unknown- unmatched context switch line {}! ", s);
        }

        return new PowerEvent(taskId, current, time);
    }

    /**
     * Add zero current power event to the "previous task" the task that get context switched out.
     * Now that we've a time sorted task power events, we can safely put a zero current power event
     * for tasks switching out
     *
     * @param timeSortedTaskPEs time sorted power events. Doesn't contain
     * @return
     */
    private List<PowerEvent> addZeroPEs(List<PowerEvent> timeSortedTaskPEs) {
        int prevTaskId = UNKNOWN_PID;
        long prevTime = globals.getGlobalTimeUs().getLeft();

        ListIterator<PowerEvent> peIterator = timeSortedTaskPEs.listIterator();
        while(peIterator.hasNext()) {
            PowerEvent pe = peIterator.next();
            int taskId = pe.getTaskId();
            long time = pe.getTimeUs();

            Verify.verify(taskId != PHONE_PID, "Input should only have contained task power events");
            Verify.verify(time >= prevTime, "Input must've been time sorted");

            // taskId and prevTaskId can be equal if some context switch line was lost in the middle
            if(taskId != prevTaskId) {
                // Add a zero power event to the prev task
                peIterator.add(new PowerEvent(prevTaskId, DoubleFactory.newUnmodifiableDouble(0), time));
            }
            prevTime = time;
            prevTaskId = taskId;
        }

        long stopTime = globals.getGlobalTimeUs().getRight();
        peIterator.add(new PowerEvent(prevTaskId, DoubleFactory.newUnmodifiableDouble(0), stopTime));
        return timeSortedTaskPEs;
    }

}
