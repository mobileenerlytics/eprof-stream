package com.mobileenerlytics.eprof.stream;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:eprof.properties")
public class EprofConfig {
}
