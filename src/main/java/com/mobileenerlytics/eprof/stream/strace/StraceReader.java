package com.mobileenerlytics.eprof.stream.strace;

import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.readers.PropsReader;
import com.mobileenerlytics.eprof.stream.util.GsonHelper;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.util.TimeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.*;

@Component
public class StraceReader {
    @Autowired
    Globals globals;

    @Autowired
    TimeHelper timeHelper;

    @Autowired
    private GsonHelper gsonHelper;

    @Autowired
    PropsReader propsReader;

    private Logger logger = LoggerFactory.getLogger(StraceReader.class.getSimpleName());

    public void write() {
        try {
            long startTimeUs = globals.getGlobalTimeUs().getLeft();
            LocalDateTime ldt = globals.getLocalDateTime();
            int year = ldt.getYear();
            int month = ldt.getMonthValue();
            int day = ldt.getDayOfMonth();
            long windowSizeUs = MovingAvgSamplingCollector.windowSizeUs(globals.getGlobalTimeUs());
            TreeMap<Long, Map<Integer, Summaries>> straceLogs = Files.lines(globals.getTraceDir().resolve("strace"))
                    .filter(s -> timeHelper.within(parseStraceTimeUs(s, year, month, day), globals.getGlobalTimeUs()))
                    .collect(
                            groupingBy(s -> ((parseStraceTimeUs(s, year, month, day) - startTimeUs) / windowSizeUs),
                                    TreeMap::new,
                                    groupingBy(this::parseStraceThreadid, mapping(this::clipStrace, new StraceSummarizer()))));

            String pkgName = propsReader.getProperty("packageName");
            int pkgPid = globals.getTaskDataMap().entrySet().stream()
                            .filter(e -> e.getValue().isProc())
                            .filter(e -> e.getValue().taskName.equals(pkgName))
                            .map(e -> e.getKey())
                            .findFirst()
                            .get();

            straceLogs = straceLogs.entrySet().parallelStream()
                    .collect(
                            toMap(e -> (windowSizeUs * e.getKey() / 1000L),
                                    Map.Entry::getValue,
                                    (a, b) -> {throw new RuntimeException();},
                                    TreeMap::new));

            Map<Integer, TreeMap<Long, Map<Integer, Summaries>>> pidStraceLog = new HashMap<>();
            pidStraceLog.put(pkgPid, straceLogs);

            Files.write(globals.getOutDir().resolve("strace.json"), gsonHelper.toJsonBytes(pidStraceLog));
        } catch(IOException e) {
            logger.warn(e.toString());
        }
    }

    // Remove pid, timestamp
    private String clipStrace(String s) {
        return Arrays.stream(s.split("\\s+")).skip(2).collect(joining(" "));
    }

    private int parseStraceThreadid(String s) {
        String[] tokens = s.split("\\s+");
        return Integer.parseInt(tokens[0]);
    }

    final static Pattern pattern = Pattern.compile("(\\d\\d):(\\d\\d):(\\d\\d).(\\d+)");
    private long parseStraceTimeUs(String s, int year, int month, int day) {
        Matcher matcher = pattern.matcher(s);
        Verify.verify(matcher.find());

        int hour = Integer.parseInt(matcher.group(1));
        int minute = Integer.parseInt(matcher.group(2));
        int second = Integer.parseInt(matcher.group(3));
        int nanosecond = Integer.parseInt(matcher.group(4)) * 1000;
        long us = TimeUnit.MICROSECONDS.convert(
                LocalDateTime.of(year, month, day, hour, minute, second, nanosecond).toEpochSecond(ZoneOffset.UTC),
                TimeUnit.SECONDS
        );
        us += nanosecond/1000;
        return us;
    }
}
