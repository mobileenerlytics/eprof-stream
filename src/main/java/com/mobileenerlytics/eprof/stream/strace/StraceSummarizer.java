package com.mobileenerlytics.eprof.stream.strace;

import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

class StraceSummarizer implements Collector<String, Summaries, Summaries> {
    @Override
    public Supplier<Summaries> supplier() {
        return () -> new Summaries();
    }

    @Override
    public BiConsumer<Summaries, String> accumulator() {
        return (summary, s) -> {
            Optional<String> optError = Optional.empty();
            int errIndex = s.indexOf(" E");
            if(errIndex != -1) {
                optError = Optional.of(s.substring(errIndex).trim());
            }
            if(! s.contains(" resumed")) {
                String[] tokens = s.split("\\(");
                if (tokens.length > 1) {
                    summary.newSyscall(tokens[0], optError);
                }
            } else {
                summary.resumedSyscall(optError);
            }
        };
    }

    @Override
    public BinaryOperator<Summaries> combiner() {
            return (a, b) -> {
            a.combine(b);
            return a;
        };
    }

    @Override
    public Function<Summaries, Summaries> finisher() {
        return Function.identity();
    }

    @Override
    public Set<Characteristics> characteristics() {
        return EnumSet.of(Characteristics.IDENTITY_FINISH);
    }
}
