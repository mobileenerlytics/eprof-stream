package com.mobileenerlytics.eprof.stream.strace;

import com.google.common.base.Preconditions;
import com.google.gson.JsonArray;
import com.google.gson.JsonSerializer;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.google.common.base.Predicates.not;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

public class Summaries {
    Map<String, SyscallSummary> summaryMap = new HashMap<>();
    String lastSyscall = null;

    public static JsonSerializer<Summaries> getJsonSerializer() {
        return (src, typeOfSrc, context) -> {
            JsonArray arr = new JsonArray();
            src.summaryMap.values().stream()
                    .map(SyscallSummary::toString)
                    .forEach(arr::add);
            return arr;
        };
    }

    static class SyscallSummary {
        String syscall;
        int count;
        Map<String, Integer> errCounts = new HashMap<>();

        SyscallSummary(String syscall) {
            this.syscall = syscall;
        }

        void combine(SyscallSummary summary) {
            Preconditions.checkArgument(summary.syscall.equals(syscall));
            count += summary.count;
            summary.errCounts.forEach((k, v) -> errCounts.merge(k, v, Integer::sum));
        }

        void visitedError(String error) {
            int numErrors = 0;
            if(errCounts.containsKey(error)) {
                numErrors = errCounts.get(error);
            }
            errCounts.put(error, ++numErrors);
        }

        @Override
        public String toString() {
            StringBuilder stringBuilder = new StringBuilder(String.format("%s : %d : ", syscall, count));
            String errString = errCounts.entrySet().stream()
                    .map(e -> String.format("%s - %s", e.getValue(), e.getKey()))
                    .collect(joining(" "));
            stringBuilder.append(errString);
            return stringBuilder.toString();
        }
    }

    void newSyscall(String syscall, Optional<String> optError) {
        if(!summaryMap.containsKey(syscall))
            summaryMap.put(syscall, new SyscallSummary(syscall));

        lastSyscall = syscall;
        SyscallSummary syscallSummary = summaryMap.get(syscall);
        syscallSummary.count++;
        if(optError.isPresent()) {
            String error = optError.get();
            syscallSummary.visitedError(error);
        }
    }

    void resumedSyscall(Optional<String> optError) {
        if(lastSyscall == null)
            return;
        if(optError.isPresent()) {
            SyscallSummary summary = summaryMap.get(lastSyscall);
            String error = optError.get();
            summary.visitedError(error);
        }
    }

    void combine(Summaries other) {
        Map<String, SyscallSummary> mergedSummaryMap = new HashMap<>();
        mergedSummaryMap.putAll(summaryMap);
        mergedSummaryMap.entrySet().stream().filter(e -> other.summaryMap.containsKey(e.getKey()))
                .forEach(e -> e.getValue().combine(other.summaryMap.get(e.getKey())));

        Map<String, SyscallSummary> uniqSyscalls = other.summaryMap.entrySet().stream()
                .filter(not(mergedSummaryMap::containsKey))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
        mergedSummaryMap.putAll(uniqSyscalls);

        summaryMap = mergedSummaryMap;
    }
}
