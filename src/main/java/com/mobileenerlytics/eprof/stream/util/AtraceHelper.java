package com.mobileenerlytics.eprof.stream.util;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.StreamSupport;

@Component
public class AtraceHelper {
    @Autowired
    ApplicationContext ctx;

    @Autowired
    TimeHelper timeHelper;

    @VisibleForTesting
    public long timeOffsetUs = Long.MAX_VALUE;

    public void findTimeOffset() {
        Verify.verify(timeOffsetUs == Long.MAX_VALUE);
        String realtime_line = StreamSupport.stream(ctx.getBean(DbgSpliterator.class), true)
                .filter(s -> s.contains("trace_event_clock_sync: realtime_ts="))
                .findFirst()
                .get();

        long dbgTimeUs = getTimeWithoutOffset(realtime_line);

        long realTimeUs = (long) (1e3 * extractTS(realtime_line));

        timeOffsetUs = realTimeUs - dbgTimeUs;
    }

    public long getTime(String s) throws NumberFormatException {
        Verify.verify(timeOffsetUs != Long.MAX_VALUE,
                "time offset was not set");
        return timeOffsetUs + getTimeWithoutOffset(s);
    }

    public boolean inBounds(String s, Pair<Long, Long> globalTime) {
        try {
            long time = getTime(s);
            return timeHelper.within(time, globalTime);
        } catch(NumberFormatException nfe) {
            // Could not parse the time from the line, just throw it away
            return false;
        }
    }


    private static final Pattern p_dbg = Pattern.compile(" (\\d+\\.\\d+): ");

    public static long getTimeWithoutOffset(String s) throws NumberFormatException {
        Matcher m_dbg = p_dbg.matcher(s);
        Verify.verify(m_dbg.find());
        return (long) (Double.parseDouble(m_dbg.group(1)) * 1e6);
    }

    private static final Pattern tsPattern = Pattern.compile("_ts=([\\d\\.]+)");

    private static Double extractTS(String s) {
        Matcher matcher = tsPattern.matcher(s);
        Verify.verify(matcher.find());
        return Double.parseDouble(matcher.group(1));
    }
}
