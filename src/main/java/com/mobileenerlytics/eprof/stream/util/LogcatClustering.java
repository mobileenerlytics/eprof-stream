package com.mobileenerlytics.eprof.stream.util;

import com.google.gson.Gson;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toMap;

@Component
public class LogcatClustering {
    static ApplicationContext context;

    @Autowired
    GsonHelper gsonHelper;

    private static final int THRESHOLD = 10;

    public void run(String contents, String proc) {
        Gson gson = gsonHelper.getGson();
        Map<String, Map<Long, List<String>>> allLogs = gson.fromJson(contents, Map.class);

        List<String> procLogs = allLogs.get(proc).values().parallelStream().flatMap(l -> l.parallelStream()).collect(Collectors.toList());
        List<String> nonEmptyLogs = procLogs.parallelStream()
                .map(s -> s.replaceAll(".*\\):", ""))
                .map(String::trim)
                .filter(s -> s.length() > 0)
                .collect(Collectors.toList());

        TreeMap<String, Integer> occurenceMap = nonEmptyLogs.parallelStream()
                .collect(
                        toMap(
                                Function.identity(),
                                s -> 1,
                                (v1, v2) -> v1 + v2,
                                TreeMap::new));

        Map<String, Integer> fuzzedOccurenceMap = new HashMap<>();
        String s = "";
        int count = 0;
        LevenshteinDistance ld = LevenshteinDistance.getDefaultInstance();
        // Fuzzy merge
        for(Entry<String, Integer> e: occurenceMap.entrySet()) {
            if(ld.apply(s, e.getKey()) < s.length()/THRESHOLD) {
                System.out.printf("Merging %s with %s%n", s, e.getKey());
                count += e.getValue();
            } else {
                fuzzedOccurenceMap.put(s, count);
                count = e.getValue();
            }
            s = e.getKey();
        }
        fuzzedOccurenceMap.put(s, count);

        System.out.println("--- occurence map ---");
        occurenceMap.entrySet().stream()
                .sorted(comparingInt(Entry::getValue))
                .skip(occurenceMap.size() - 10)
                .forEach(e -> System.out.println(e.getValue() + " -> " + e.getKey()));

        System.out.println("--- fuzzed occurence map ---");
        fuzzedOccurenceMap.entrySet().stream()
                .sorted(comparingInt(Entry::getValue))
                .skip(fuzzedOccurenceMap.size() - 10)
                .forEach(e -> System.out.println(e.getValue() + " -> " + e.getKey()));

        System.out.printf("Merged by fuzzing: %d%n", occurenceMap.size() - fuzzedOccurenceMap.size());
        System.out.printf("Empty lines: %d%n", procLogs.size() - nonEmptyLogs.size());
        System.out.printf("Total lines: %d%n", procLogs.size());
    }

    public static void main(String args[]) throws IOException {
        String contents = new String(Files.readAllBytes(Paths.get(args[0])));
        String proc = args[1];

        context = new ClassPathXmlApplicationContext("META-INF/config.xml");

        final Path traceDir = FileSystems.getDefault().getPath(args[0]);
        LogcatClustering m = context.getBean(LogcatClustering.class);
        m.run(contents, proc);
    }
}
