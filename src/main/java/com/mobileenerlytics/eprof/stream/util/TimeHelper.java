package com.mobileenerlytics.eprof.stream.util;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TimeHelper {
    public boolean within(long point, Pair<Long, Long> range) {
        return point >= range.getLeft() && point <= range.getRight();
    }

    public boolean withins(long point, List<Pair<Long, Long>> ranges) {
        if(ranges == null || ranges.isEmpty())
            return false;
        return ranges.stream()
                .map(range -> within(point, range)).reduce((a, b) -> a || b)
                .get();
    }

//    public static long getTimeZoneAdjustedMs(long ms, TimeZone timeZone) {
//        Date date = new Date();
//        boolean daylight = timeZone.inDaylightTime(date);
//        if(daylight)
//            ms += timeZone.getDSTSavings();
//        return ms + timeZone.getRawOffset();
//    }

    public long overlap(Pair<Long, Long> pair1, Pair<Long, Long> pair2) {
        long overlap = Math.min(pair1.getValue(), pair2.getValue()) - Math.max(pair1.getKey(), pair2.getKey());
        if(overlap > 0)
            return overlap;
        return 0;
    }
}
