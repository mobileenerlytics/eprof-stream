package com.mobileenerlytics.eprof.stream.util;

//Interface
public interface  SortedList<T> {
  
  /**
   * Inserts an element to the list keeping the list sorted.
   */
  public void add(T element);

  public T pollFirst();

  public int size();

}