package com.mobileenerlytics.eprof.stream.util;

import com.google.common.base.Verify;

import java.util.Arrays;

import static java.util.stream.Collectors.joining;

public class NetworkUtils {
    public static String toBytes(String ip) {
        int size = 4;
        int radix = 10; // decimal
        String[] tokens;
        if(ip.contains(":")) {
            // IPv6
            size = 16;
            radix = 16; // hexadecimal
            tokens = ip.split(":");
            for(int i = 0; i < tokens.length; i ++)
                // Some tokens might be less than size 4, add zeroes in the beginning
                while(tokens[i].length() < 4)
                    tokens[i] = "0" + tokens[i];

            // Break tokens of size 4 into sizes of 2. "fe21" -> "fe", "21"
            tokens = Arrays.stream(tokens).collect(joining()).split("(?<=\\G.{2})");
        } else {
            // IPv4
            tokens = ip.split("\\.");
        }
        Verify.verify(tokens.length == size);
        int[] ipSections = new int[size];
        for (int i = 0; i < size; i++) {
            ipSections[i] = Integer.parseInt(tokens[i], radix);
        }

        return Arrays.stream(ipSections)
                .mapToObj(b -> String.format("%8s", Integer.toBinaryString(b)).replace(' ', '0'))
                .collect(joining());
    }
}
