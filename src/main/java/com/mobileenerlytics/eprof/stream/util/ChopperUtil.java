package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.TreeMap;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Component
public class ChopperUtil {
    @Autowired
    TimeHelper timeHelper;

    /**
     *
     * @param taskId Task for which we're doing this calculation
     * @param powerEvents overall list of power events
     * @param intervals time intervals where <code>taskId</code> was "active" and should get power from <code>powerEvents</code>
     * @param globalTime global start-stop time
     * @return list of power events owned by <code>taskId</code> for <code>intervals</code>
     */
    public List<PowerEvent> chop(int taskId, List<PowerEvent> powerEvents,
                                        List<Pair<Long, Long>> intervals, Pair<Long, Long> globalTime) {
        final TreeMap<Long, PowerEvent> inputMap = powerEvents.stream().collect(
                toMap(pe -> pe.time,
                    pe -> new PowerEvent(taskId, pe.current, pe.time),
                    (v1, v2) -> v2,
                    TreeMap::new));
        if(!inputMap.containsKey(globalTime.getLeft()))
            inputMap.put(globalTime.getLeft(),
                    new PowerEvent(taskId, DoubleFactory.newUnmodifiableDouble(0), globalTime.getLeft()));

        final TreeMap<Long, PowerEvent> outputMap = new TreeMap<>();
        outputMap.put(globalTime.getLeft(),
                new PowerEvent(taskId, DoubleFactory.newUnmodifiableDouble(0), globalTime.getLeft()));
        intervals.stream()
                .forEach(bracket -> {
                    outputMap.putAll(inputMap.subMap(bracket.getLeft(), bracket.getRight()));
                    if (timeHelper.within(bracket.getLeft(), globalTime)) {
                        MyDouble current = inputMap.floorEntry(bracket.getLeft()).getValue().current;
                        outputMap.put(bracket.getLeft(), new PowerEvent(taskId, current, bracket.getLeft()));
                    }
                    if (timeHelper.within(bracket.getRight(), globalTime))
                        outputMap.put(bracket.getRight(),
                                new PowerEvent(taskId, DoubleFactory.newUnmodifiableDouble(0), bracket.getRight()));
                });
        outputMap.put(globalTime.getRight(),
                new PowerEvent(taskId, DoubleFactory.newUnmodifiableDouble(0), globalTime.getRight()));
        return outputMap.values().stream().collect(toList());
    }
}
