package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

/**
 * AddingCollector is the dual of DifferencingCollector. Turns differences into
 * original as follows
 * [5, 5, 10, -15] --> [5, 10, 20, 5]
 */
public class AddingCollector extends AbstractPowerEventCollector<List<PowerEvent>> {
    PowerEvent lastPe;
    MyDouble lastCurrent = DoubleFactory.newDouble(0);

    @Override
    public Supplier<List<PowerEvent>> supplier() {
        return LinkedList::new;
    }

    @Override
    protected void processPE(List<PowerEvent> powerEvents, PowerEvent pe) {
        if(lastPe != null && lastPe.time < pe.time)
            powerEvents.add(lastPe);

        PowerEvent clonePE = new PowerEvent(pe.taskId, pe.current.add(lastCurrent), pe.time);
        lastCurrent = pe.current.add(lastCurrent);
        lastPe = clonePE;
    }

    @Override
    protected List<PowerEvent> finishPE(List<PowerEvent> powerEvents) {
        if(lastPe != null)
            powerEvents.add(lastPe);
        return powerEvents;
    }
}
