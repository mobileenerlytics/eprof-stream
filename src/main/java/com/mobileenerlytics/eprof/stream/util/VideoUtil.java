package com.mobileenerlytics.eprof.stream.util;

import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_imgproc;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.springframework.stereotype.Component;

import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
import static org.bytedeco.javacpp.opencv_core.cvPoint;
import static org.bytedeco.javacpp.opencv_imgproc.*;

@Component
public class VideoUtil {
    Frame blackFrame = null;

    public Frame getBlackFrame(int width, int height) {
        if(blackFrame != null && blackFrame.imageWidth == width && blackFrame.imageHeight == height)
            return blackFrame;

        opencv_imgproc.CvFont font = new opencv_imgproc.CvFont();
        cvInitFont(font, CV_FONT_HERSHEY_TRIPLEX, 2.0, 2.0, 2, 2, CV_AA);
        opencv_core.IplImage bgrImage = opencv_core.IplImage.create(width, height, IPL_DEPTH_8U, 3);;
        String text = "Screen OFF";

        opencv_core.CvSize textsize = new opencv_core.CvSize();
        cvGetTextSize(text, font, textsize, new int[]{1, 2});

        cvPutText(bgrImage, text, cvPoint((width - textsize.width())/2,(height - textsize.height())/2), font, opencv_core.CvScalar.WHITE);
        blackFrame = new OpenCVFrameConverter.ToIplImage().convert(bgrImage);

        return blackFrame;
    }
}