package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

/**
 * DifferencingCollector subtracts consecutive currents and turn them into differences
 * as follows
 * [5, 10, 20, 5] --> [5, 5, 10, -15]
 */
public class DifferencingCollector extends AbstractPowerEventCollector<List<PowerEvent>> {
    MyDouble lastCurrent = DoubleFactory.newDouble(0);

    @Override
    public Supplier<List<PowerEvent>> supplier() {
        return LinkedList::new;
    }

    @Override
    protected void processPE(List<PowerEvent> powerEvents, PowerEvent pe) {
        PowerEvent clonePE = new PowerEvent(pe.taskId, pe.current.subtract(lastCurrent), pe.time);
        lastCurrent = pe.current;
        powerEvents.add(clonePE);
    }

    @Override
    protected List<PowerEvent> finishPE(List<PowerEvent> powerEvents) {
        return powerEvents;
    }
}
