package com.mobileenerlytics.eprof.stream.util;

import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Supplier;

@Component
@Scope(value="prototype")
public class EnergyCollector extends AbstractPowerEventCollector<MyDouble> {
    private final long startTimeUs, stopTimeUs;
    private long prevTime;
    private MyDouble prevCurrent = DoubleFactory.newDouble(0);

    @Autowired
    private DoubleFactory doubleFactory;

    EnergyCollector(Pair<Long, Long> timeUs) {
        startTimeUs = timeUs.getLeft();
        stopTimeUs = timeUs.getRight();
        prevTime = startTimeUs;
    }

    @Override
    public Supplier<MyDouble> supplier() {
        return () -> doubleFactory.newSymbolicDouble(0);
    }

    @Override
    protected void processPE(MyDouble d, PowerEvent pe) {
        Verify.verify(pe.time >= startTimeUs);
        Verify.verify(pe.time <= stopTimeUs);
        Verify.verify(pe.time >= prevTime);
//        Verify.verify(pe.current > -0.01); // To accomodate for double precision problems
        d.add(prevCurrent.multiply(pe.time - prevTime));
        prevCurrent = pe.current;
        prevTime = pe.time;
    }

    @Override
    public MyDouble finishPE(MyDouble d) {
        Verify.verify(prevTime <= stopTimeUs);
        d.add(prevCurrent.multiply(stopTimeUs - prevTime));
        return d;
    }

    public static MyDouble calcEnergy(ApplicationContext context, Pair<Long, Long> time, List<PowerEvent> list) {
        return mAusTouAh(list.stream().collect(context.getBean(EnergyCollector.class, time)));
    }

    public static MyDouble uAhTomAus(MyDouble uAh) {
        return uAh.multiply((1e6 * 3600)/ 1e3);
    }
    public static MyDouble mAusTouAh(MyDouble mAus) {
        return mAus.multiply(1000.0 / (1e6 * 3600));
    }
}