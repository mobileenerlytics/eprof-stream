package com.mobileenerlytics.eprof.stream.util;

import java.io.FileWriter;
import java.io.IOException;

public class FileOps {
    public static void writeToFile(FileWriter fw, String dump) {
        try {
            fw.append(dump);
            fw.append("\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
