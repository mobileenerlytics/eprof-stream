package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import static java.util.Comparator.comparingLong;

public abstract class AbstractPowerEventCollector<T> implements Collector<PowerEvent, T, T> {
    // The stupid timestamps in ftrace_out are not always sorted. When core-1
    // and core-0 are logging simultaneously, there can be a timestamp disorder.
    // This causes severe problems all over eprof. We are fixing this while reading
    // ftrace output itself.

    // We, hence, keep a SortedCache of MAX_DISORDER_ENTRIES that is kept sorted by
    // time. Every PowerEvent we read is put into sortedCache and we get a PowerCache
    // by pulling from SortedCache ensuring time orderedness.
    // We here assume that the timestamps can be maximum out of order by
    // MAX_DISORDER_ENTRIES. Eprof will crash otherwise.
    private final static int MAX_DISORDER_ENTRIES = 1 << 16;
    private final SortedCache<PowerEvent> cache;

    protected abstract void processPE(T t, PowerEvent pe);

    protected abstract T finishPE(T t);

    public AbstractPowerEventCollector() {
        cache = new SortedCache<>(comparingLong(pe -> pe.time), MAX_DISORDER_ENTRIES);
    }

    @Override
    public Supplier<T> supplier() {
        throw new UnsupportedOperationException();
    }

    @Override
    public BiConsumer<T, PowerEvent> accumulator() {
        return (t, pe) -> {
            cache.put(pe);
            Optional<PowerEvent> optPe = cache.get();
            if (optPe.isPresent())
                processPE(t, optPe.get());
        };
    }

    @Override
    public BinaryOperator<T> combiner() {
        return (t1, t2) -> {
            throw new UnsupportedOperationException();
        };
    }

    @Override
    public Function<T, T> finisher() {
        return t -> {
            // Drain the sorted cache
            cache.finish();
            Optional<PowerEvent> optPe = cache.get();
            while (optPe.isPresent()) {
                processPE(t, optPe.get());
                optPe = cache.get();
            }

            return finishPE(t);
        };
    }

    @Override
    public Set<Characteristics> characteristics() {
        // ordered, not concurrent, not identity finish
        return Collections.unmodifiableSet(EnumSet.noneOf(Characteristics.class));
    }
}