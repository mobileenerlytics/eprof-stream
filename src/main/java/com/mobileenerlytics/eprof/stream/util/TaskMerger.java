package com.mobileenerlytics.eprof.stream.util;

import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toCollection;

/**
 * TaskMerger merges power timelines of similar threads together.
 * "Similar threads" have the same name and are started by same pid. One reason for similar threads are when
 * a thread gets killed and later restarted. Such thread will have same name but different task id.
 *
 * Merging similar threads together helps in keeping timelines clean. Otherwise there are multiple
 * thread labels on timeline with the same name
 */
@Component
public class TaskMerger {
    @Autowired
    PowerSumCalculator powerSumCalculator;

    private static String toKey(TaskData td) {
        return td.parentTaskId + td.taskName;
    }

    private Map<String, SortedSet<TaskData>> taskGroups;

    public void init(Map<Integer, TaskData> taskDataMap) {
        Verify.verify(taskGroups == null);
        // Make task groups that need to be merged together
        taskGroups = Collections.unmodifiableMap(taskDataMap.values().parallelStream()
                .collect(groupingBy(TaskMerger::toKey, toCollection(TreeSet::new))));
        taskGroups.values().stream().forEach(
                tasks -> tasks.stream().skip(1).forEach(td -> taskDataMap.remove(td.taskId)));
    }

    public void mergeTasks(Map<Integer, List<PowerEvent>> procCurrent) {
        Map<Integer,List<PowerEvent>> retProcCurrent = new HashMap<>();

        Map<TaskData, List<List<PowerEvent>>> mergedProcCurrent = new HashMap<>();

        for(SortedSet<TaskData> tdSet: taskGroups.values()) {
            for(TaskData td: tdSet) {
                if(!procCurrent.containsKey(td.taskId))
                    continue;
                if(tdSet.size() == 1) {
                    // Nothing to merge. Put directly into retProcCurrent
                    retProcCurrent.put(td.taskId, procCurrent.get(td.taskId));
                } else {
                    TaskData mergeToTask = tdSet.first();
                    if(!mergedProcCurrent.containsKey(mergeToTask))
                        mergedProcCurrent.put(mergeToTask, new LinkedList<>());
                    mergedProcCurrent.get(mergeToTask).add(procCurrent.get(td.taskId));
                }
                procCurrent.remove(td.taskId);
            }
        }

        for(Map.Entry<TaskData, List<List<PowerEvent>>> entry : mergedProcCurrent.entrySet()) {
            int taskId = entry.getKey().taskId;
            retProcCurrent.put(taskId, powerSumCalculator.sumPower(entry.getValue(), OptionalInt.of(taskId)));
        }
        procCurrent.clear();
        procCurrent.putAll(retProcCurrent);
    }
}
