package com.mobileenerlytics.eprof.stream.util;

import java.util.Comparator;
import java.util.Optional;

public class SortedCache<T> {
    private final int CACHE_SZ;
    private boolean mFinish = false;
    // We do not use TreeSet here, since SortedCache must allow duplicates!
    final SortedList<T> cache;

    SortedCache(Comparator<T> comparator, int size) {
        cache = new ArraySortedList<>(comparator);
        CACHE_SZ = size;
    }

    Optional<T> get() {
        if (!mFinish && cache.size() < CACHE_SZ)
            return Optional.empty();
        return Optional.ofNullable(cache.pollFirst());
    }

    void put(T t) {
        cache.add(t);
    }

    void finish() {
        mFinish = true;
    }
}