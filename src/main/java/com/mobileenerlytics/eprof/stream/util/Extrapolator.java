package com.mobileenerlytics.eprof.stream.util;

import com.google.common.base.Preconditions;
import com.google.common.base.Verify;
import com.google.common.collect.RowSortedTable;
import com.mobileenerlytics.symbolic.MyDouble;

import java.util.Map;
import java.util.SortedMap;

public class Extrapolator {
    public static void extrapolateCurrent(SortedMap<Long, MyDouble> freq2Current, long freq) {
        Preconditions.checkArgument(!freq2Current.containsKey(freq), "No need to extrapolate. Frequency " +
                "already present in the freq table");
        long firstFreq = freq2Current.firstKey();
        MyDouble firstCurrent = freq2Current.get(firstFreq);
        Verify.verify(firstCurrent.isUnmodifiable());

        long lastFreq = freq2Current.lastKey();
        MyDouble lastCurrent = freq2Current.get(lastFreq);
        Verify.verify(lastCurrent.isUnmodifiable());

        MyDouble current = firstCurrent.add(
                lastCurrent.subtract(firstCurrent).divide(lastFreq - firstFreq).multiply(freq - firstFreq));
        freq2Current.put(freq, current);
    }

    public static <T> void extrapolateCurrent(RowSortedTable<Long, T, MyDouble> freq2Current, long freq) {
        Preconditions.checkArgument(!freq2Current.containsRow(freq), "No need to extrapolate. Frequency " +
                "already present in the freq table");

        long firstFreq = freq2Current.rowKeySet().first();
        Map<T, MyDouble> firstRow = freq2Current.row(firstFreq);

        long lastFreq = freq2Current.rowKeySet().last();
        Map<T, MyDouble> lastRow = freq2Current.row(lastFreq);

        for(Map.Entry<T, MyDouble> cell: firstRow.entrySet()) {
            Verify.verify(lastRow.containsKey(cell.getKey()));
            MyDouble firstCurrent = cell.getValue();
            Verify.verify(firstCurrent.isUnmodifiable());
            MyDouble lastCurrent = lastRow.get(cell.getKey());
            Verify.verify(lastCurrent.isUnmodifiable());
            MyDouble current = firstCurrent.add(
                    lastCurrent.subtract(firstCurrent).divide(lastFreq - firstFreq).multiply(freq - firstFreq));
            freq2Current.put(freq, cell.getKey(), current);
        }
    }
}
