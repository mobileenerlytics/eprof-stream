package com.mobileenerlytics.eprof.stream.util;

import com.google.gson.*;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.batterystats.EventData;
import com.mobileenerlytics.eprof.stream.strace.Summaries;
import com.mobileenerlytics.symbolic.MyDouble;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.math.BigDecimal;

@Component
public class GsonHelper {
    @Autowired
    Globals globals;

    Gson getGson() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting()
                .registerTypeAdapter(Double.class, new JsonSerializer<Double>()  {
                    public JsonElement serialize(Double value, Type theType,
                                                 JsonSerializationContext context) {
                        if (value.isNaN()) {
                            return new JsonPrimitive(0); // Convert NaN to zero
                        } else if (value.isInfinite()) {
                            return new JsonPrimitive(value); // Leave small numbers and infinite alone
                        } else {
                            // Keep 2 decimal digits only
                            return new JsonPrimitive((new BigDecimal(value)).setScale(2,
                                    BigDecimal.ROUND_HALF_UP));
                        }
                    }
                }).registerTypeAdapter(MyDouble.class, new JsonSerializer<MyDouble>()  {
                    public JsonElement serialize(MyDouble value, Type theType,
                                                 JsonSerializationContext context) {
                        // Keep 2 decimal digits only
                        return new JsonPrimitive(value.toString());
                    }
                })
                .registerTypeAdapter(EventData.class, EventData.getJsonSerializer(globals))
                .registerTypeAdapter(Summaries.class, Summaries.getJsonSerializer())
                .create();
    }

    public byte[] toJsonBytes(Object object) {
        return getGson().toJson(object).getBytes();
    }
}
