package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static java.util.stream.Collectors.toList;

@Component
public class PowerEventsToFgTasks {
    @Autowired
    ChopperUtil chopperUtil;

    @Autowired
    Globals globals;

    /**
     * Attributes energy to tasks, populating <code>taskCurrent</code> , based on when a process
     * was in the foreground. This method is made <code>static </code> since <code>GPU</code> and
     * <code>MaliGPU</code> also call this.
     *
     * @param taskCurrent just has one key <code>PHONE_PID</code> holding list of all power events
     * @param globalTime  global start-stop time
     * @param taskEvents  all the task events captured from battery stats. This method just cares about the "top"
     *                    event, when the task was running in foreground
     * @return
     */
    public Map<Integer, List<PowerEvent>> accountTaskEnergy(Map<Integer, List<PowerEvent>> taskCurrent) {
        if(!taskCurrent.containsKey(PHONE_PID))
            return new HashMap<>();
        for (TaskData td : globals.getTaskDataMap().values()) {
            List<Pair<Long, Long>> topEventTimes = td.getEvents()
                    .filter(e -> e.eventName.equals("top"))
                    .map(e -> Pair.of(e.getStartTimeMs() * 1000, e.getEndTimeMs() * 1000))
                    .collect(toList());

            if (!topEventTimes.isEmpty())
                taskCurrent.put(td.taskId,
                        chopperUtil.chop(td.taskId, taskCurrent.get(PHONE_PID), topEventTimes, globals.getGlobalTimeUs()));
        }

        return taskCurrent;
    }
}
