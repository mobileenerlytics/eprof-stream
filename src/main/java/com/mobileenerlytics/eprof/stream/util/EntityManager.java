//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.mobileenerlytics.eprof.stream.util;

import com.google.common.base.Verify;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

public class EntityManager implements EntityResolver {
    public EntityManager() {
    }

    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
        if (systemId.equals("http://mobileenerlytics.com/eprof/dtd/model.dtd")) {
            InputStream stream = EntityManager.class.getClassLoader().getResourceAsStream("models/model.dtd");
            return new InputSource(stream);
        } else {
            Verify.verify(false, "Entity not found: public {} system {}", new Object[]{publicId, systemId});
            return null;
        }
    }
}
