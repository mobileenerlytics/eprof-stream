package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import org.springframework.stereotype.Component;

import java.util.*;

import static java.util.Comparator.comparingLong;
import static java.util.stream.Collectors.toList;

@Component
public class PowerSumCalculator {
    public Map<Integer, List<PowerEvent>> sumPowerByTasks(Collection<Map<Integer, List<PowerEvent>>> procCurrents) {
        int[] tasksIds = procCurrents.stream()
                .map(pc -> pc.keySet())
                .flatMap(set -> set.stream())
                .mapToInt(task -> task)
                .distinct()
                .toArray();

        Map<Integer, List<PowerEvent>> totalCurrents = new HashMap<>();
        for (int taskId : tasksIds) {
            List<PowerEvent> sumPe = sumPower(procCurrents.stream()
                    .filter(pc -> pc.containsKey(taskId))
                    .map(pc -> pc.get(taskId))
                    .collect(toList()), OptionalInt.empty());
            totalCurrents.put(taskId, sumPe);
        }
        return totalCurrents;
    }

    List<PowerEvent> sumPower(List<List<PowerEvent>> procCurrent, OptionalInt newTask) {
        // Converts power event currents to differences
        // [5, 10, 20, 5] --> [5, 5, 10, -15]
        List<List<PowerEvent>> diffedProcCurrents = new LinkedList<>();
        procCurrent.stream().forEach(
                v -> diffedProcCurrents.add(v.stream().collect(new DifferencingCollector()))
        );

        if (newTask.isPresent())
            diffedProcCurrents.stream().flatMap(x -> x.stream()).forEach(pe -> pe.taskId = newTask.getAsInt());

        // TODO: sorted operation is expensive. Write a sorted list merger spliterator instead
        // Dual of DifferencingCollector. Turns power event differences to currents
        // [5, 5, 10, -15] --> [5, 10, 20, 5]
        return diffedProcCurrents.stream().flatMap(x -> x.stream())
                .sorted(comparingLong(pe -> pe.time))
                .collect(new AddingCollector());
    }

}
