package com.mobileenerlytics.eprof.stream.util;

import com.google.common.base.Verify;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

@Component
public class MovingAvgSamplingCollector {
    @Autowired
    Globals globals;

    @Autowired
    DoubleFactory doubleFactory;

    class PowerEventCollector extends AbstractPowerEventCollector<List<PowerEvent>> {
        private final long startTime, stopTime;
        final int taskId;
        private long sampleTime, prevTime, windowSize;
        private MyDouble prevCurrent = DoubleFactory.newUnmodifiableDouble(0);
        private MyDouble energy = DoubleFactory.newUnmodifiableDouble(0);
        private Logger logger = LoggerFactory.getLogger(MovingAvgSamplingCollector.class.getSimpleName());

        private MyDouble loggedCurrent = DoubleFactory.newUnmodifiableDouble(0);

        public PowerEventCollector(int taskId, Pair<Long, Long> time, long windowSize) {
            this.taskId = taskId;
            this.windowSize = windowSize;
            startTime = time.getLeft();
            stopTime = time.getRight();
            sampleTime = startTime;
            prevTime = sampleTime;
        }

        @Override
        public Supplier<List<PowerEvent>> supplier() {
            return LinkedList::new;
        }

        private void addPe(List<PowerEvent> list, long time, MyDouble current) {
            if (!current.equals(loggedCurrent)) {
                list.add(new PowerEvent(taskId, current, time));
                loggedCurrent = current;
            }
            sampleTime += windowSize;
            prevTime = sampleTime;
            energy = doubleFactory.newUnmodifiableDouble(0);
        }

        @Override
        protected void processPE(List<PowerEvent> l, PowerEvent pe) {
            logger.debug("{}", this);
            logger.debug("{}", pe);
            Verify.verify(taskId == pe.taskId, "All power events must belong to the " +
                    "same task while averaging. New: %d old: %d", pe.taskId, taskId);
            Verify.verify(sampleTime < stopTime);
            if (pe.time < sampleTime)
                logger.debug("BP");
            Verify.verify(pe.time >= sampleTime,
                    "Power Event time %d < sampleTime %d", pe.time, sampleTime);
            while (pe.time >= sampleTime + windowSize) {
                // spit energy and increase sampleTime
                energy = energy.add(prevCurrent.multiply(sampleTime + windowSize - prevTime));
                addPe(l, sampleTime, energy.divide(windowSize));

                // Fall through here, use this power event for next sample
            }
            energy = energy.add(prevCurrent.multiply(pe.time - prevTime));
            prevTime = pe.time;
            prevCurrent = pe.current;
        }


        @Override
        protected List<PowerEvent> finishPE(List<PowerEvent> l) {
            while (sampleTime < stopTime) {
                long sampleStopTime = Math.min(sampleTime + windowSize, stopTime);
                energy = energy.add(prevCurrent.multiply(sampleStopTime - prevTime));
                addPe(l, sampleTime, energy.divide(sampleStopTime - sampleTime));
            }
            addPe(l, stopTime, doubleFactory.newUnmodifiableDouble(0));
            return l;
        }

        @Override
        public String toString() {
            return String.format("[time <%d, %d>, task %d, window %d] sampleTime %d -- {prevTime %d prevCurrent %.2f} energy %.2f ",
                    startTime, stopTime, taskId, windowSize, sampleTime, prevTime, prevCurrent, energy);
        }
    }

    public Map<Integer, List<PowerEvent>> movingAverageByTasks(Map<Integer, List<PowerEvent>> procCurrent) {
        Pair<Long, Long> globalTimeUs = globals.getGlobalTimeUs();
        long windowSizeUs = windowSizeUs(globalTimeUs);
        Map<Integer, List<PowerEvent>> averageProcCurrent = new HashMap<>();
        procCurrent.keySet().stream().
            forEach(pid -> averageProcCurrent.put(pid, procCurrent.get(pid).stream()
                    .collect(new PowerEventCollector(pid, globalTimeUs, windowSizeUs))));
        return averageProcCurrent;
    }

    public static long windowSizeUs(Pair<Long, Long> globalTimeUs) {
        // Generate 1000 data points at max
        return Math.max(200_000, (globalTimeUs.getRight() - globalTimeUs.getLeft())/1000);
    }

}