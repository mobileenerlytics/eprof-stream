package com.mobileenerlytics.eprof.stream;

import com.mobileenerlytics.eprof.stream.mocks.MockGlobalsBuilder;
import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DbgSpliteratorTest {
    @Mock
    private Globals globals;

    @InjectMocks
    private DbgSpliterator dbgSpliterator;

    private StringBuilder sb;

    @BeforeEach
    public void setup() throws IOException {
        final int CHARS = 100;
        final int ZFILES = 128;

        MockitoAnnotations.initMocks(this);
        MockGlobalsBuilder builder = new MockGlobalsBuilder(globals);
        sb = new StringBuilder();
        for (int i = 0; i < ZFILES; i++) {
            String random = i + " -- " + RandomStringUtils.randomAlphabetic(CHARS) + "\n";
            builder.addGzFile("atrace_z." + i, random);
            sb.append(random);
        }
    }

    @Test
    public void testSequential() {
        String sequential = StreamSupport.stream(dbgSpliterator, false)
                .collect(joining("\n"));
        assertEquals(sb.toString(), sequential + "\n");
    }

    @Test
    public void testSplit() {
        String parallel = StreamSupport.stream(dbgSpliterator, true)
                .collect(joining("\n"));
        assertEquals(sb.toString(), parallel + "\n");
    }
}
