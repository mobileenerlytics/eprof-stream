package com.mobileenerlytics.eprof.stream.mocks;

import com.mobileenerlytics.symbolic.ConcreteDouble;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class MockDoubleFactory {
    public static void mock(DoubleFactory doubleFactory) {
        when(doubleFactory.newSymbolicUnmodifiableDouble(anyDouble(), anyString())).thenAnswer(
                invocation -> new ConcreteUnmodifiableDouble((double) invocation.getArgument(0))
        );

        when(doubleFactory.cloneSymbolicUnmodifiableDouble(any(MyDouble.class))).thenAnswer(
                invocation -> new ConcreteUnmodifiableDouble(invocation.getArgument(0))
        );

        when(doubleFactory.newSymbolicDouble(anyDouble())).thenAnswer(
                invocation -> new ConcreteDouble((double) invocation.getArgument(0))
        );
    }
}
