package com.mobileenerlytics.eprof.stream.mocks;

import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class MockDbgSpliterator {
    public static void mockDbgSpliterator(String[] lines, @Nullable ApplicationContext ctx, DbgSpliterator... spyDbgSpliterators) {
        try {
            for(DbgSpliterator dbgSpliterator: spyDbgSpliterators) {
                when(dbgSpliterator.trySplit()).thenReturn(null);
                doReturn(lines[0],
                        // Append null to lines array
                        (Object[])Stream.concat(Arrays.stream(lines).skip(1), Stream.of((String) null)).toArray(String[]::new))
                        .when(dbgSpliterator).readLine();
            }

            if(ctx != null) {
                doReturn(spyDbgSpliterators[0],
                        // Append null to dbgSpliterators array
                        (Object[])Stream.concat(Arrays.stream(spyDbgSpliterators).skip(1), Stream.of((DbgSpliterator) null)).toArray(DbgSpliterator[]::new))
                        .when(ctx).getBean(DbgSpliterator.class);
            }
        } catch (IOException e) {
            // IOException should never be thrown. Fail this test.
            assertTrue(false);
        }
    }


    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testMockString() throws IOException {
        DbgSpliterator dbgSpliterator = spy(DbgSpliterator.class);
        String[] lines = {"a", "b", "c"};
        mockDbgSpliterator(lines, null, dbgSpliterator);
        String collect = StreamSupport.stream(dbgSpliterator, false).collect(joining(":"));
        assertEquals("a:b:c", collect);
    }

    @Test
    public void testMockCtx() throws IOException {
        ApplicationContext context = mock(ApplicationContext.class);
        String[] lines = {"a", "b", "c"};
        DbgSpliterator[] spliterators = new DbgSpliterator[5];
        for(int i = 0; i < spliterators.length; i ++) {
            spliterators[i] = spy(DbgSpliterator.class);
        }
        mockDbgSpliterator(lines, context, spliterators);
        for(int i = 0; i < spliterators.length; i ++) {
            String collect = StreamSupport.stream(context.getBean(DbgSpliterator.class), false).collect(joining(":"));
            assertEquals("a:b:c", collect);
        }
        assertNull(context.getBean(DbgSpliterator.class));
    }
}
