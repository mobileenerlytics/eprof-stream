package com.mobileenerlytics.eprof.stream.mocks;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.batterystats.EventData;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.lang.NonNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static com.mobileenerlytics.eprof.stream.ps.TaskData.UNKNOWN_PID;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class MockGlobalsBuilder {
    private final Globals globals;
    private final Path traceDir;

    public MockGlobalsBuilder(Globals globals) throws IOException {
        this.globals = globals;
        traceDir = Files.createTempDirectory("mockTest");
        when(globals.getTraceDir()).thenReturn(traceDir);

        Path outDir = traceDir.resolve("eprof.files");
        outDir.toFile().mkdirs();
        when(globals.getOutDir()).thenReturn(outDir);
    }

    public MockGlobalsBuilder addFile(String fileName, String data) throws IOException {
            File file = traceDir.resolve(fileName).toFile();
            file.createNewFile();
            try (FileOutputStream stream = new FileOutputStream(file)) {
                stream.write(data.getBytes());
            }
            return this;
    }

    public MockGlobalsBuilder addGzFile(String fileName, String data) throws IOException {
        File file = traceDir.resolve(fileName).toFile();
        file.createNewFile();
        try (GZIPOutputStream stream = new GZIPOutputStream(new FileOutputStream(file))) {
            stream.write(data.getBytes());
        }
        return this;
    }

    public MockGlobalsBuilder setGlobalTimeUs(@NonNull Pair<Long, Long> globalTimeUs) {
        when(globals.getGlobalTimeUs()).thenReturn(globalTimeUs);
        return this;
    }

    public MockGlobalsBuilder addTaskDataMap(Map<Integer, TaskData> taskDataMap) {
        TaskData init = mock(TaskData.class);
        init.taskId = 0;
        init.userName = "root";
        init.taskName = "init";

        TaskData phone = mock(TaskData.class);
        phone.taskId = PHONE_PID;
        phone.parentTaskId = 0;
        phone.userName = "root";
        phone.taskName = "phone";

        TaskData unknown = mock(TaskData.class);
        unknown.taskId = UNKNOWN_PID;
        unknown.parentTaskId = 0;
        unknown.userName = "root";
        unknown.taskName = "unknown";

        Map<Integer, TaskData> defaultProcs = new HashMap<Integer, TaskData>() {{
            put(0, init);
            put(UNKNOWN_PID, unknown);
            put(PHONE_PID, phone);
        }};
        defaultProcs.values().stream().forEach(td -> td.setIsProc());
        taskDataMap.putAll(defaultProcs);
        when(globals.getTaskDataMap()).thenReturn(taskDataMap);
        return this;
    }

    public MockGlobalsBuilder addEventDataToTask(int taskId, List<EventData> events) {
        TaskData phoneTd = globals.getTaskDataMap().get(taskId);
        assertNotNull(phoneTd);
        doReturn(events.stream()).when(phoneTd).getEvents();
        return this;
    }
}
