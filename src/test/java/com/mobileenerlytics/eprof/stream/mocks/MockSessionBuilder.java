package com.mobileenerlytics.eprof.stream.mocks;

import com.att.aro.core.packetanalysis.pojo.PacketInfo;
import com.att.aro.core.packetanalysis.pojo.Session;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class MockSessionBuilder {
    public static final String LOCAL_IP = "1.1.1.1";
    public static final String REMOTE_IP = "2.2.2.2";
    public static final int LOCAL_PORT = 1;
    public static final int REMOTE_PORT = 2;
    private static final String REMOTE_HOSTNAME = "test";

    private String localIp = LOCAL_IP, remoteIp = REMOTE_IP;
    private int localPort = LOCAL_PORT, remotePort = REMOTE_PORT;
    private boolean isUDP = true;
    private long bytes = 100L;
    private List<PacketInfo> packetInfoList = new LinkedList<>();

    public MockSessionBuilder setLocalPort(int localPort) {
        this.localPort = localPort;
        return this;
    }

    public MockSessionBuilder setRemotePort(int remotePort) {
        this.remotePort = remotePort;
        return this;
    }

    public MockSessionBuilder setUDP(boolean UDP) {
        isUDP = UDP;
        return this;
    }

    public MockSessionBuilder setBytes(long bytes) {
        this.bytes = bytes;
        return this;
    }

    public MockSessionBuilder setPacketInfoList(List<PacketInfo> packetInfoList) {
        this.packetInfoList = packetInfoList;
        return this;
    }

    public MockSessionBuilder setLocalIp(String localIp) {
        this.localIp = localIp;
        return this;
    }

    public MockSessionBuilder setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
        return this;
    }

    public void build(Session s) throws UnknownHostException {
        doReturn(isUDP).when(s).isUDP();

        doReturn(InetAddress.getByName(localIp)).when(s).getLocalIP();
        doReturn(localPort).when(s).getLocalPort();

        doReturn(InetAddress.getByName(remoteIp)).when(s).getRemoteIP();
        doReturn(remotePort).when(s).getRemotePort();

        doReturn(REMOTE_HOSTNAME).when(s).getRemoteHostName();

        doReturn(packetInfoList).when(s).getAllPackets();
        doReturn(bytes).when(s).getBytesTransferred();
    }

    public static PacketInfo mockPacketInfo(double time) {
        PacketInfo packetInfo = mock(PacketInfo.class);
        doReturn(time).when(packetInfo).getTimeStamp();
        return packetInfo;
    }
}
