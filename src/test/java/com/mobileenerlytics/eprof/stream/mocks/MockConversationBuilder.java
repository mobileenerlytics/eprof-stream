package com.mobileenerlytics.eprof.stream.mocks;

import com.mobileenerlytics.eprof.stream.hardware.network.Conversation;

public class MockConversationBuilder {
    public static void buildConversation(Conversation conversation, String localIP, String localPort,
                                         String remoteIP, String remotePort, String remoteName) {
        conversation.localIP = localIP;
        conversation.localPort = localPort;
        conversation.remoteIP = remoteIP;
        conversation.remotePort = remotePort;
        conversation.remoteName = remoteName;
    }
}
