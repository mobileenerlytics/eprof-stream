package com.mobileenerlytics.eprof.stream;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Tag("SkipCI")  // Takes a long time. Processes video files which aren't handled by bitbucket pipeline
public class PostProcessingTest {
    static Path eagleDirPath, resultDirPath;
    static Properties buildProp;
    static int numOfCPUCores;
    static String packageName;

    @BeforeAll
    public static void runEprof(TestInfo testInfo) throws InterruptedException, ParserConfigurationException, SAXException, ExecutionException, IOException {
        Path tracesPath = FileSystems.getDefault().getPath(PostProcessingTest.class.getClass().getResource("/traces").getPath());
        File traceZipFile = tracesPath.resolve("eagle-test-trace.zip").toFile();

        String traceName = traceZipFile.getName().replace(".zip", "");
        Path tmpDir = Files.createTempDirectory(traceName + "-trace");
        tmpDir.toFile().deleteOnExit();

        unzipFile(traceZipFile, tmpDir);

        File eagleDir = tmpDir.toFile().listFiles()[0];
        eagleDirPath = eagleDir.toPath();

        Main.main(new String[] {eagleDirPath.toAbsolutePath().toString()});

        resultDirPath = eagleDirPath.resolve("eprof.files");
        assertTrue(resultDirPath.toFile().exists());

        buildProp = new Properties();
        buildProp.load(new FileInputStream(eagleDirPath.resolve("build.prop").toFile()));
        numOfCPUCores = Integer.parseInt(buildProp.getProperty("cpu.cores"));
        packageName = buildProp.getProperty("packageName");
    }

    @Test
    public void testGlobalProp() throws IOException {
        Properties globalProp = new Properties();
        globalProp.load(new FileInputStream(resultDirPath.resolve("global.prop").toFile()));
        assertEquals(globalProp.getProperty("cpu.cores"), buildProp.getProperty("cpu.cores"));
        assertEquals(globalProp.getProperty("packageName"), buildProp.getProperty("packageName"));
        assertEquals(globalProp.getProperty("startTime"), buildProp.getProperty("startTime") + "000");
        assertEquals(globalProp.getProperty("stopTime"), buildProp.getProperty("endTime") + "000");
        Long startTime = Long.parseLong(globalProp.getProperty("startTime"));
        Long stopTime = Long.parseLong(globalProp.getProperty("stopTime"));
        assertTrue(stopTime > startTime);
    }

    @Test
    public void testCsvs() {
        assertTrue(resultDirPath.resolve("GPU.csv").toFile().exists());
        assertTrue(resultDirPath.resolve("TOTAL.csv").toFile().exists());
        for (int i = 0; i < numOfCPUCores; i++) {
            assertTrue(resultDirPath.resolve("CPU-" + i + ".csv").toFile().exists());
        }
    }

    @Test
    public void testAppJsonHasPkgName() throws FileNotFoundException {
        FileReader appJsonReader = new FileReader(resultDirPath.resolve("app.json").toFile());
        JsonElement jelement = new JsonParser().parse(appJsonReader);
        JsonArray jarray = jelement.getAsJsonArray();
        assertTrue(jarray.size() > 0);
        boolean foundPackageName = false;
        for (int i = 0; i < jarray.size(); i++) {
            foundPackageName |= jarray.get(i).getAsJsonObject().getAsJsonPrimitive("taskName")
                    .getAsString().equals(packageName);
        }
        assertTrue(foundPackageName);
    }

    @Test
    // Total CPU energy of all tasks should be equal to total CPU energy of phone
    public void testTotalCPUEnergyOfTasksvsPhone() throws FileNotFoundException {
        FileReader appJsonReader = new FileReader(resultDirPath.resolve("app.json").toFile());
        JsonElement jelement = new JsonParser().parse(appJsonReader);
        JsonArray jarray = jelement.getAsJsonArray();
        assertTrue(jarray.size() > 0);
        double phoneCPU = jarray.get(0).getAsJsonObject().getAsJsonObject("perComponentEnergy").getAsJsonPrimitive("CPU").getAsDouble();

        double totalTaskCPU = 0;
        for (int i = 1; i < jarray.size(); i++) {
            JsonObject perComponentEnergy = jarray.get(i).getAsJsonObject().getAsJsonObject("perComponentEnergy");
            if(perComponentEnergy.has("CPU"))
                totalTaskCPU += perComponentEnergy.getAsJsonPrimitive("CPU").getAsDouble();
        }

        // Total task CPU energy shouldn't be greater than phone CPU energy
        assertTrue(totalTaskCPU <= 1.01 * phoneCPU);   // 1.01 to account for double precision problems

        // We didn't account more than 10% of CPU energy
        assertTrue(totalTaskCPU >= 0.9 * phoneCPU);
    }

    @Test
    public void testScreenMp4() {
        assertTrue(!eagleDirPath.resolve("screen0.mp4").toFile().exists()
                || resultDirPath.resolve("screen.mp4").toFile().exists());
    }

    @Test
    public void testNetwork() {
        assertTrue(resultDirPath.resolve("network_conversations.json").toFile().exists());
        assertTrue(resultDirPath.resolve("network_power.csv").toFile().exists());
        assertTrue(resultDirPath.resolve("network_traffic.csv").toFile().exists());
    }

    private static void unzipFile(File file, Path dest) throws IOException {
        ZipFile zipFile = new ZipFile(file);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();

        while(entries.hasMoreElements()){
            ZipEntry entry = entries.nextElement();
            Path newFilePath = dest.resolve(entry.getName());
            File newFile = newFilePath.toFile();
            newFile.getParentFile().mkdirs();
            Files.createFile(newFilePath);
            try (OutputStream outputStream = new FileOutputStream(newFile);
                 InputStream inputStream = zipFile.getInputStream(entry)) {
                IOUtils.copy(inputStream, outputStream);
            }
        }

        zipFile.close();
    }
}
