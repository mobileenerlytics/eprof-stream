package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.mocks.MockDbgSpliterator;
import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.context.ApplicationContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.spy;

public class AtraceHelperTest {
    @Mock
    Globals globals;

    @Spy
    ApplicationContext ctx;

    @InjectMocks
    AtraceHelper atraceHelper;

    @Spy
    TimeHelper timeHelper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRandomStringInBounds() {
        atraceHelper.timeOffsetUs = 0;

        assertTrue(atraceHelper.inBounds("     atrace-12518 (12518) [002] ...1  2.0: tracing_mark_write: trace_event_clock_sync: parent_ts=1000.000000",
                Pair.of(0L, 3_000_000L)));
    }

    @Test
    public void testFindTimeOffset() {
        String[] line = {
           "     atrace-12518 (12518) [002] ...1  1979.920166: tracing_mark_write: trace_event_clock_sync: realtime_ts=1543616848066"
        };

        DbgSpliterator[] dbgSpliterators = {
                spy(DbgSpliterator.class), spy(DbgSpliterator.class)
        };

        MockDbgSpliterator.mockDbgSpliterator(line, ctx, dbgSpliterators);
        atraceHelper.findTimeOffset();
        assertEquals(1543614868145834L, atraceHelper.timeOffsetUs);
    }
}
