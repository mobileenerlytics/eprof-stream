package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.mocks.MockDoubleFactory;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.MyDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static com.mobileenerlytics.eprof.stream.BaseIntegrationTest.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class MovingAvgSamplingCollectorTest {

    final int PID = 0;
    final Pair<Long, Long> globalTime = Pair.of(10L, 50L);
    final ArrayList<PowerEvent> pes = new ArrayList<PowerEvent>() {{
        add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15));
        add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 25));
        add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 35));
        add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 45));
    }};
    final MyDouble TOTAL_ENERGY = EnergyCollector.mAusTouAh(new ConcreteUnmodifiableDouble(500));

    @InjectMocks
    MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Spy
    ApplicationContext context;

    @Mock
    DoubleFactory doubleFactory;

    @InjectMocks
    EnergyCollector energyCollector = new EnergyCollector(globalTime);

    @InjectMocks
    EnergyCollector energyCollector2 = new EnergyCollector(globalTime);

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MockDoubleFactory.mock(doubleFactory);
        when(context.getBean(any(Class.class), any())).thenReturn(energyCollector, energyCollector2);
    }

    /**
     * Power events are every 10 ms, window size is 10 ms
     */
    @Test
    public void testCollector() {
        final long WINDOW_SIZE = 10;

        List<PowerEvent> avgPes = pes.stream().collect(movingAvgSamplingCollector.new PowerEventCollector(PID, globalTime, WINDOW_SIZE));
//        avgPes.stream().forEach(System.out::println);

        assertThat(avgPes, Matchers.contains(
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(5), 10),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(15), 20),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(0), 50)
        ));
    }

    /**
     * Power events are every 10 ms, window size is 20 ms, tests proper averaging
     */
    @Test
    public void testAvging() {
        final long WINDOW_SIZE = 20;

        List<PowerEvent> avgPes = pes.stream().collect(movingAvgSamplingCollector.new PowerEventCollector(PID, globalTime, WINDOW_SIZE));
//        avgPes.stream().forEach(System.out::println);

        assertThat(avgPes, Matchers.contains(
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 10),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(15), 30),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(0), 50)
        ));
    }

    /**
     * Global time size 40 is not divisible by window size 25, tests corner cases
     */
    @Test
    public void testAvgingWithNonDivisibleWindowSize() {
        final long WINDOW_SIZE = 25;

        List<PowerEvent> avgPes = pes.stream().collect(movingAvgSamplingCollector.new PowerEventCollector(PID, globalTime, WINDOW_SIZE));

//        System.out.println("----");
//        pes.stream().forEach(System.out::println);
//        System.out.println("----");
//        avgPes.stream().forEach(System.out::println);
//        System.out.println("----");

        assertThat(avgPes, Matchers.contains(
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(12), 10),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(13.3333), 35),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(0), 50)
        ));
    }

    /**
     * Small window size -- 5
     */
    @Test
    public void testAvgingWithSmallWindowSize() {
        final long WINDOW_SIZE = 5;

        List<PowerEvent> avgPes = pes.stream().collect(movingAvgSamplingCollector.new PowerEventCollector(PID, globalTime, WINDOW_SIZE));

//        System.out.println("----");
//        pes.stream().forEach(System.out::println);
//        System.out.println("----");
//        avgPes.stream().forEach(System.out::println);
//        System.out.println("----");

        assertThat(avgPes, Matchers.contains(
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 25),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 35),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 45),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(0), 50)
        ));
    }

    /**
     * Small window size, non-divisible -- 10 % 4 != 0
     */
    @Test
    public void testAvgingWithSmallNonDivWindowSize() {
        final long WINDOW_SIZE = 4;

        List<PowerEvent> avgPes = pes.stream().collect(movingAvgSamplingCollector.new PowerEventCollector(PID, globalTime, WINDOW_SIZE));

//        System.out.println("----");
//        pes.stream().forEach(System.out::println);
//        System.out.println("----");
//        avgPes.stream().map(pe -> new PowerEvent(pe.pid, pe.current, pe.time)).forEach(System.out::println);
//        System.out.println("----");

        assertThat(avgPes, Matchers.contains(
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(7.5), 14),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 18),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(12.5), 22),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 26),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(12.5), 34),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 38),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(12.5), 42),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 46),
            new PowerEvent(PID, new ConcreteUnmodifiableDouble(0), 50)
        ));

        assertEquals(TOTAL_ENERGY, EnergyCollector.calcEnergy(context, globalTime, pes), 0.01);
        assertEquals(TOTAL_ENERGY, EnergyCollector.calcEnergy(context, globalTime, avgPes), 0.01);
    }
}
