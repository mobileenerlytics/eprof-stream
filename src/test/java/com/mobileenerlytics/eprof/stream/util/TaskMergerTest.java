package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

public class TaskMergerTest {
    final int PID = 0;
    final int THREADID = 1;
    final int DUP_THREADID = 2;

    @Mock
    TaskData tdPid, tdThreadid, tdDupThreadid;

    @Spy
    PowerSumCalculator powerSumCalculator;

    @InjectMocks
    TaskMerger taskMerger;


    Map<Integer, TaskData> taskDataMap = new HashMap<>();

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        tdPid.taskId = PID;
        tdThreadid.taskName = "PID";
        doReturn(true).when(tdPid).isProc();

        tdThreadid.taskId = THREADID;
        tdThreadid.parentTaskId = PID;
        tdThreadid.taskName = "samename";

        tdDupThreadid.taskId = DUP_THREADID;
        tdDupThreadid.parentTaskId = PID;
        tdDupThreadid.taskName = "samename";

        taskDataMap.put(PID, tdPid);
        taskDataMap.put(THREADID, tdThreadid);
        taskDataMap.put(DUP_THREADID, tdDupThreadid);
    }

    @Test
    public void testMergerNoOp() {
        Map<Integer, List<PowerEvent>> procCurrent = new HashMap<Integer, List<PowerEvent>>();
        procCurrent.put(PID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15));
        }});
        procCurrent.put(THREADID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(20), 20));
        }});

        taskMerger.init(taskDataMap);
        taskMerger.mergeTasks(procCurrent);

        Map<Integer, List<PowerEvent>> expectedProcCurrent = new HashMap<Integer, List<PowerEvent>>();
        expectedProcCurrent.put(PID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15));
        }});
        expectedProcCurrent.put(THREADID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(20), 20));
        }});

        assertEquals(expectedProcCurrent, procCurrent);
    }

    @Test
    public void testMergerOne() {
        Map<Integer, List<PowerEvent>> procCurrent = new HashMap<Integer, List<PowerEvent>>();
//        procCurrent.put(PID, new ArrayList<PowerEvent>() {{
//            add(new PowerEvent(PID, 10, 15));
//        }});
        procCurrent.put(THREADID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(20), 20));
        }});
        procCurrent.put(DUP_THREADID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(DUP_THREADID, new ConcreteUnmodifiableDouble(25), 10));
        }});

        taskMerger.init(taskDataMap);
        taskMerger.mergeTasks(procCurrent);

        Map<Integer, List<PowerEvent>> expectedProcCurrent = new HashMap<Integer, List<PowerEvent>>();
        expectedProcCurrent.put(THREADID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(25), 10));
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(45), 20));
        }});

        assertEquals(expectedProcCurrent, procCurrent);
    }

    @Test
    public void testMergerMultiple() {
        Map<Integer, List<PowerEvent>> procCurrent = new HashMap<Integer, List<PowerEvent>>();
//        procCurrent.put(PID, new ArrayList<PowerEvent>() {{
//            add(new PowerEvent(PID, 10, 15));
//        }});
        procCurrent.put(THREADID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(10), 10));
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(20), 20));
        }});
        procCurrent.put(DUP_THREADID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(DUP_THREADID, new ConcreteUnmodifiableDouble(25), 5));
            add(new PowerEvent(DUP_THREADID, new ConcreteUnmodifiableDouble(5), 15));
        }});

        taskMerger.init(taskDataMap);
        taskMerger.mergeTasks(procCurrent);

        Map<Integer, List<PowerEvent>> expectedProcCurrent = new HashMap<Integer, List<PowerEvent>>();
        expectedProcCurrent.put(THREADID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(25), 5));
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(35), 10));
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(15), 15));
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(25), 20));
        }});

        assertEquals(expectedProcCurrent, procCurrent);
    }

    @Test
    public void testMergerSameTime() {
        Map<Integer, List<PowerEvent>> procCurrent = new HashMap<Integer, List<PowerEvent>>();
        procCurrent.put(THREADID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(10), 10));
        }});
        procCurrent.put(DUP_THREADID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(DUP_THREADID, new ConcreteUnmodifiableDouble(25), 10));
        }});

        taskMerger.init(taskDataMap);
        taskMerger.mergeTasks(procCurrent);

        Map<Integer, List<PowerEvent>> expectedProcCurrent = new HashMap<Integer, List<PowerEvent>>();
        expectedProcCurrent.put(THREADID, new ArrayList<PowerEvent>() {{
            add(new PowerEvent(THREADID, new ConcreteUnmodifiableDouble(35), 10));
        }});

        assertEquals(expectedProcCurrent, procCurrent);
    }

    @Test
    public void testTaskMergerRemove() {
        Map<Integer, TaskData> mergedTasks = new HashMap<Integer, TaskData>() {{
            put(PID, taskDataMap.get(PID));
            put(THREADID, taskDataMap.get(THREADID));
        }};
        taskMerger.init(taskDataMap);
        assertEquals(mergedTasks, taskDataMap);
    }
}
