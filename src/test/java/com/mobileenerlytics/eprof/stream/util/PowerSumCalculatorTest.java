package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PowerSumCalculatorTest {
    final int PID = 0;
    final int PID1 = 1;
    final int PID2 = 2;

    @InjectMocks
    PowerSumCalculator powerSumCalculator;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSumming() {
        final ArrayList<PowerEvent> pes1 = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 25));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 35));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 45));
        }};

        List<PowerEvent> copyPes1 = pes1.stream().map(pe -> new PowerEvent(pe.taskId, pe.current, pe.time))
                .collect(Collectors.toList());

        final ArrayList<PowerEvent> pes2 = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 10));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 20));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 30));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 40));
        }};

        List<List<PowerEvent>> input = new LinkedList<List<PowerEvent>>() {{
            add(pes1);
            add(pes2);
        }};

        final ArrayList<PowerEvent> expectedpes = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 10));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 15));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(30), 20));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(40), 25));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(30), 30));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 35));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(30), 40));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(40), 45));
        }};

        assertEquals(expectedpes, powerSumCalculator.sumPower(input, OptionalInt.empty()));

        // Assert that there was no side effect on input list
        assertEquals(copyPes1, pes1);
    }

    @Test
    public void testSumTasks() {
        Map<Integer, List<PowerEvent>> pmap1 = new HashMap<>();

        final ArrayList<PowerEvent> pes1 = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 25));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 35));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 45));
        }};
        pmap1.put(PID, pes1);

        final ArrayList<PowerEvent> pes2 = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID1, new ConcreteUnmodifiableDouble(10), 10));
            add(new PowerEvent(PID1, new ConcreteUnmodifiableDouble(0), 15));
            add(new PowerEvent(PID1, new ConcreteUnmodifiableDouble(20), 20));
            add(new PowerEvent(PID1, new ConcreteUnmodifiableDouble(30), 25));
            add(new PowerEvent(PID1, new ConcreteUnmodifiableDouble(10), 30));
        }};
        pmap1.put(PID1, pes2);

        Map<Integer, List<PowerEvent>> pmap2 = new HashMap<>();

        final ArrayList<PowerEvent> pes3 = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 10));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 20));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 30));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 40));
        }};
        pmap2.put(PID, pes3);

        final ArrayList<PowerEvent> pes4 = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID2, new ConcreteUnmodifiableDouble(10), 15));
            add(new PowerEvent(PID2, new ConcreteUnmodifiableDouble(0), 20));
            add(new PowerEvent(PID2, new ConcreteUnmodifiableDouble(20), 30));
            add(new PowerEvent(PID2, new ConcreteUnmodifiableDouble(10), 35));
        }};
        pmap2.put(PID2, pes4);


        final ArrayList<PowerEvent> expectedpes = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 10));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 15));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(30), 20));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(40), 25));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(30), 30));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 35));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(30), 40));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(40), 45));
        }};


        Set<Map<Integer, List<PowerEvent>>> pmCollection = new HashSet<Map<Integer, List<PowerEvent>>>() {{
            add(pmap1);
            add(pmap2);
        }};

        Map<Integer, List<PowerEvent>> sumTasks = powerSumCalculator.sumPowerByTasks(pmCollection);

        assertEquals(expectedpes, sumTasks.get(PID));
        assertEquals(pes2, sumTasks.get(PID1));
        assertEquals(pes4, sumTasks.get(PID2));

    }
}
