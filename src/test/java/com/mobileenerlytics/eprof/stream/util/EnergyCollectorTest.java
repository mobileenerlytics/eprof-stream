package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.mocks.MockDoubleFactory;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import com.mobileenerlytics.symbolic.DoubleFactory;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;

import static com.mobileenerlytics.eprof.stream.BaseIntegrationTest.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class EnergyCollectorTest {
    final int PID = 0;
    final Pair<Long, Long> globalTime = Pair.of(10L, 50L);

    @Mock
    ApplicationContext context;

    @Mock
    DoubleFactory doubleFactory;

    @InjectMocks
    EnergyCollector energyCollector = new EnergyCollector(globalTime);

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MockDoubleFactory.mock(doubleFactory);
        when(context.getBean(any(Class.class), any())).thenReturn(energyCollector);
    }

    @Test
    public void testBasic() {
        final ArrayList<PowerEvent> pes = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 25));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 35));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 45));
        }};
        assertEquals(EnergyCollector.mAusTouAh(new ConcreteUnmodifiableDouble(500)),
                EnergyCollector.calcEnergy(context, globalTime, pes), 0.01);
    }

    @Test
    public void testNonUniformInterval() {
        final ArrayList<PowerEvent> pes = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 10));   // milliAmp, microseconds
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 25));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 30));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 41));
        }};
        // 540 = 10*15 + 20*5 + 10*11 + 20*9
        assertEquals(EnergyCollector.mAusTouAh(new ConcreteUnmodifiableDouble(540)),
                EnergyCollector.calcEnergy(context, globalTime, pes), 0.01);
    }

    @Test
    public void testRepetition() {
        // Same power event is repeated twice in a row
       final ArrayList<PowerEvent> pes = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(0), 10));   // milliAmp, microseconds
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 20));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 30));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(0), 40));
        }};
       // 400 =  10 * (40 -20)
        assertEquals(EnergyCollector.mAusTouAh(new ConcreteUnmodifiableDouble(200)),
                EnergyCollector.calcEnergy(context, globalTime, pes), 0.01);
    }
}
