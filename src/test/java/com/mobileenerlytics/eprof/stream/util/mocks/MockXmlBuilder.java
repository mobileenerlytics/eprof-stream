package com.mobileenerlytics.eprof.stream.util.mocks;

import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;


public class MockXmlBuilder {
    public static Document fromString(String xml) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        return dBuilder.parse(new ByteArrayInputStream(xml.getBytes("UTF8")));
    }

    @Test
    public void testMockXml() throws IOException, SAXException, ParserConfigurationException {
        String xml = "<a></a>";
        Document document = fromString(xml);
        NodeList a = document.getElementsByTagName("a");
        assertNotNull(a);
    }
}
