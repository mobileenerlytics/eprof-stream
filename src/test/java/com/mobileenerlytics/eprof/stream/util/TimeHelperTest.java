package com.mobileenerlytics.eprof.stream.util;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

public class TimeHelperTest {
    @InjectMocks
    TimeHelper timeHelper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void isWithin() {
        assertTrue(timeHelper.within(1L, Pair.of(0L, 2L)));
    }

    @Test
    public void isNotWithin() {
        assertFalse(timeHelper.within(3L, Pair.of(0L, 2L)));
    }

    @Test
    public void overlapTest() {
        assertEquals(timeHelper.overlap(Pair.of(0L, 2L), Pair.of(1L, 3L)), 1L);
    }

    @Test
    public void noOverlapTest() {
        assertEquals(timeHelper.overlap(Pair.of(0L, 2L), Pair.of(2L, 4L)), 0L);
    }
}
