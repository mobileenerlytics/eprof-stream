package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddingDifferencingCollectorTest {
    final int PID = 0;
    final ArrayList<PowerEvent> pes = new ArrayList<PowerEvent>() {{
        add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15));
        add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 25));
        add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 35));
        add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 45));
    }};

    @Test
    public void testCollector() {
        List<PowerEvent> subPes = pes.stream().collect(new DifferencingCollector());
//        avgPes.stream().forEach(System.out::println);

        ArrayList<PowerEvent> expectedPes = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 25));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(-10), 35));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 45));
        }};

        assertEquals(expectedPes, subPes);


        List<PowerEvent> addPes = subPes.stream().collect(new AddingCollector());
        assertEquals(pes, addPes);
    }

    @Test
    public void testAddCollectorAtSameTime() {
        // This is a real scenario-- while merging tasks, two tasks can have power events
        // at the same time
        final ArrayList<PowerEvent> sameTimePes = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 15));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15));
        }};

        ArrayList<PowerEvent> expectedPes = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(40), 15));
        }};

        List<PowerEvent> addPes = sameTimePes.stream().collect(new AddingCollector());
        assertEquals(expectedPes, addPes);
    }

    @Test
    public void testCollectorMergeOne() {
        final ArrayList<PowerEvent> pes2 = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 12));
        }};

        List<PowerEvent> subPes = pes.stream().collect(new DifferencingCollector());
        List<PowerEvent> subPes2 = pes2.stream().collect(new DifferencingCollector());
//        subPes2.stream().forEach(System.out::println);

        subPes.addAll(subPes2);
//        subPes.stream().sorted(Comparator.comparingLong(pe -> pe.time)).forEach(System.out::println);


        ArrayList<PowerEvent> expectedPes = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 12));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 15));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(30), 25));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 35));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(30), 45));
        }};

//        Assert.assertEquals(expectedPes, subPes);

        List<PowerEvent> addPes = subPes.stream().sorted(Comparator.comparingLong(pe -> pe.time)).
                collect(new AddingCollector());
//        addPes.stream().forEach(System.out::println);
        assertEquals(expectedPes, addPes);
    }

    @Test
    public void testCollectorMergeMultiple() {
        final ArrayList<PowerEvent> pes2 = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 10));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(5), 20));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(100), 50));
        }};

        List<PowerEvent> subPes = pes.stream().collect(new DifferencingCollector());
        List<PowerEvent> subPes2 = pes2.stream().collect(new DifferencingCollector());
//        subPes2.stream().forEach(System.out::println);

        subPes.addAll(subPes2);
//        subPes.stream().sorted(Comparator.comparingLong(pe -> pe.time)).forEach(System.out::println);


        ArrayList<PowerEvent> expectedPes = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 10));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 15));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(15), 20));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(25), 25));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(15), 35));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(25), 45));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(120), 50));
        }};

        List<PowerEvent> addPes = subPes.stream().sorted(Comparator.comparingLong(pe -> pe.time)).
                collect(new AddingCollector());
//        addPes.stream().forEach(System.out::println);
        assertEquals(expectedPes, addPes);
    }
}
