package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChopperUtilTest {
    final int PID = 0;
    @InjectMocks
    ChopperUtil chopperUtil;

    @Spy
    TimeHelper timeHelper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testChopping() {
        final Pair<Long, Long> globalTime = Pair.of(0L, 50L);
        final ArrayList<PowerEvent> input = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 25));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 35));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 45));
        }};
        final ArrayList<Pair<Long, Long>> brackets = new ArrayList<Pair<Long, Long>>() {{
            add(Pair.of(15L, 20L));
            add(Pair.of(28L, 35L));
        }};

        final ArrayList<PowerEvent> expected = new ArrayList<PowerEvent>() {{
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(0), 0));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(10), 15));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(0), 20));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(20), 28));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(0), 35));
            add(new PowerEvent(PID, new ConcreteUnmodifiableDouble(0), 50));
        }};

        assertEquals(expected, chopperUtil.chop(PID, input, brackets, globalTime));
    }
}
