package com.mobileenerlytics.eprof.stream.util;

import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import com.mobileenerlytics.symbolic.MyDouble;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.SortedMap;
import java.util.TreeMap;

import static com.mobileenerlytics.eprof.stream.BaseIntegrationTest.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExtrapolatorTest {
    SortedMap<Long, MyDouble> freq2Current;

    @BeforeEach
    public void setup() {
        freq2Current = new TreeMap<Long, MyDouble>() {{
            put(10L, new ConcreteUnmodifiableDouble(10.0));
            put(50L, new ConcreteUnmodifiableDouble(110.0));
        }};
    }
    @Test
    public void extrapolates() {
        long freq = 90L;
        Extrapolator.extrapolateCurrent(freq2Current, freq);
        assertTrue(freq2Current.containsKey(freq));
        assertEquals(210, freq2Current.get(freq), 0.1);
    }

    @Test()
    public void throwsException() {
        assertThrows(IllegalArgumentException.class, () -> Extrapolator.extrapolateCurrent(freq2Current, 10L));
    }
}
