package com.mobileenerlytics.eprof.stream.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NetworkUtilsTest {
    private void assertIpEquals(String expected, String ip) {
        assertEquals(expected.replaceAll(" ", ""), NetworkUtils.toBytes(ip));
    }

    @Test
    public void testIPv4() {
        assertIpEquals("00000000 00000000 00000000 00000000", "0.0.0.0");
        assertIpEquals("11111111 11111111 11111111 11111111", "255.255.255.255");
        assertIpEquals("11111111 00000000 11111111 00000000", "255.0.255.0");
        assertIpEquals("11000000 10101000 00000001 00000010", "192.168.1.2");
    }

    @Test
    public void testIPv6() {
        assertIpEquals("00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                "0:0:0:0:0:0:0:0");
        assertIpEquals("11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",
                "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff");
        assertIpEquals("0100111111111110 0010100100000000 0101010101000101 0011001000010000 0010000000000000 1111100011111111 1111111000100001 0110011111001111",
                "4ffe:2900:5545:3210:2000:f8ff:fe21:67cf");

        // some sections are less than 4 in length, should properly append zeros
        assertIpEquals("0100111111111110 0000000000000000 0101010101000101 0011001000010000 0010000000000000 1111100011111111 1111111000100001 0000000011001111",
                "4ffe:0:5545:3210:2000:f8ff:fe21:cf");
    }
}
