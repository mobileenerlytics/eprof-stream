package com.mobileenerlytics.eprof.stream.hardware;

import com.mobileenerlytics.eprof.stream.BaseIntegrationTest;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.batterystats.EventData;
import com.mobileenerlytics.eprof.stream.mocks.MockGlobalsBuilder;
import com.mobileenerlytics.eprof.stream.readers.PropsReader;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.util.PowerEventsToFgTasks;
import com.mobileenerlytics.eprof.stream.util.TimeHelper;
import com.mobileenerlytics.eprof.stream.util.VideoUtil;
import com.mobileenerlytics.eprof.stream.util.mocks.MockXmlBuilder;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameRecorder;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.*;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ScreenTest {
    @InjectMocks
    Screen screen;

    Document document;

    @Mock
    Globals globals;

    @Mock
    MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Spy
    VideoUtil videoUtil;

    @Spy
    TimeHelper timeHelper;

    @Mock
    PowerEventsToFgTasks powerEventsToFgTasks;

    @Mock
    PropsReader propsReader;

    /* Finding screen duration
     * ffmpeg -i screen1.mp4 2>&1 | grep "Duration"| cut -d ' ' -f 4 | sed s/,//
     */
    // Screen on-off related time stamps
    private final long SCREEN_0_DURATION_US = 11_530_000;  // Extracted from screen0.mp4
    private final long SCREEN_1_DURATION_US = 94_220_000;  // Extracted from screen1.mp4

    private final long SCREEN_0_START_TIME_US = 0;
    private final long SCREEN_0_END_TIME_US = SCREEN_0_DURATION_US;

    private final long SCREEN_GAP_US = 78_000_000;
    private final long SCREEN_1_START_TIME_US = SCREEN_0_END_TIME_US + SCREEN_GAP_US;
    private final long SCREEN_1_END_TIME_US = SCREEN_0_DURATION_US + SCREEN_1_DURATION_US;


    @BeforeEach
    public void setup() throws IOException, SAXException, ParserConfigurationException {
        MockitoAnnotations.initMocks(this);
        String xml = "<screenmodel size='256'>" +
                "<grid R='0' G='0' B='0' current='100' slopeR='0.01' slopeG='0.02' slopeB='0.03'/>" +
        "</screenmodel>";

        document = MockXmlBuilder.fromString(xml);
        when(movingAvgSamplingCollector.movingAverageByTasks(any())).then(returnsFirstArg());
        when(powerEventsToFgTasks.accountTaskEnergy(any())).then(returnsFirstArg());
        when(propsReader.getProperty(matches("brightness"), anyString())).thenReturn("255");

        assertTrue(screen.build(document.getElementsByTagName("screenmodel")));
    }

    @Test
    public void testBuildFailsOnDuplicate() throws IOException, SAXException, ParserConfigurationException {
        String xml = "<screenmodel size='256'>" +
                "<grid R='0' G='0' B='0' current='100' slopeR='10' slopeG='20' slopeB='30'/>" +
                "<grid R='0' G='0' B='0' current='110' slopeR='10' slopeG='20' slopeB='30'/>" + // Duplicate for same rgb
                "</screenmodel>";

        document = MockXmlBuilder.fromString(xml);
        when(movingAvgSamplingCollector.movingAverageByTasks(any())).then(returnsFirstArg());
        assertFalse(screen.build(document.getElementsByTagName("screenmodel")));
    }

    @Test
    public void testBuildFailsOnExtra() throws IOException, SAXException, ParserConfigurationException {
        String xml = "<screenmodel size='256'>" +
                "<grid R='0' G='0' B='0' current='100' slopeR='10' slopeG='20' slopeB='30'/>" +
                "<grid R='32' G='0' B='0' current='100' slopeR='10' slopeG='20' slopeB='30'/>" +    // Extra rgb value
                "</screenmodel>";

        document = MockXmlBuilder.fromString(xml);
        when(movingAvgSamplingCollector.movingAverageByTasks(any())).then(returnsFirstArg());
        assertFalse(screen.build(document.getElementsByTagName("screenmodel")));
    }

    @Test
    public void testBuildFailsOnIncomplete() throws IOException, SAXException, ParserConfigurationException {
        String xml = "<screenmodel size='128'>" +
                "<grid R='0' G='0' B='0' current='100' slopeR='10' slopeG='20' slopeB='30'/>" +
                "<grid R='128' G='0' B='0' current='100' slopeR='10' slopeG='20' slopeB='30'/>" +
                "<grid R='128' G='128' B='0' current='100' slopeR='10' slopeG='20' slopeB='30'/>" +
                "<grid R='128' G='128' B='128' current='100' slopeR='10' slopeG='20' slopeB='30'/>" +
                "</screenmodel>";   // There needs to be 8 values instead of just 4

        document = MockXmlBuilder.fromString(xml);
        when(movingAvgSamplingCollector.movingAverageByTasks(any())).then(returnsFirstArg());
        assertFalse(screen.build(document.getElementsByTagName("screenmodel")));
    }


    @Test
    public void testParseLogsWithoutVideo() throws IOException {
        EventData mockEd = mock(EventData.class);
        mockEd.name = "screen";
        mockEd.eventName = "screen";
        doReturn(0L).when(mockEd).getStartTimeMs();
        doReturn(10L).when(mockEd).getEndTimeMs();

        new MockGlobalsBuilder(globals)
                .setGlobalTimeUs(Pair.of(0L, 10L))
                .addTaskDataMap(new HashMap<>()).addEventDataToTask(PHONE_PID, new LinkedList<EventData>(){{
                    add(mockEd);
                }});

        Map<Integer, List<PowerEvent>> taskCurrent = screen.parseLogs();

        assertTrue(taskCurrent.containsKey(PHONE_PID));

        assertThat(taskCurrent.get(PHONE_PID), Matchers.contains(
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(100.0), 0),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0.0), 10_000)
        ));
    }

    @Test
    @Tag("SkipCI")  // Bitbucket's docker container can't write video output due to some unmet ffmpeg dependencies
    public void testAppendVideos() throws IOException {
        Path outFile = Files.createTempFile("screen", ".mp4");
        Path videosPath = FileSystems.getDefault().getPath(getClass().getResource("/videos").getPath());
        List<Path> inFiles = new LinkedList<>();
        inFiles.add(videosPath.resolve("screen0.mp4"));
        inFiles.add(videosPath.resolve("screen1.mp4"));

        Pair<Long, Long> screen0 = Pair.of(SCREEN_0_START_TIME_US, SCREEN_0_END_TIME_US);
        Pair<Long, Long> screen1 = Pair.of(SCREEN_1_START_TIME_US, SCREEN_1_END_TIME_US);
        List<Pair<Long, Long>> screenOnTimesUs = Arrays.asList(screen0, screen1);

        new MockGlobalsBuilder(globals)
                .setGlobalTimeUs(Pair.of(0L, SCREEN_1_DURATION_US));
        List<PowerEvent> powerEvents = screen.appendVideos(screenOnTimesUs, inFiles, outFile);
        System.out.println(outFile.toString());

        assertTrue(!powerEvents.isEmpty());
        assertTrue(outFile.toFile().length() > 1024 * 1024);    // File size is greater than 1MB
    }

    private Path generateSingleColorVideo(int r, int g, int b, int width, int height, int timeUs) throws IOException {
        Path videoPath = Files.createTempFile("rgb-", ".mp4");
        System.out.println(videoPath.toAbsolutePath().toString());

        FrameRecorder recorder = FrameRecorder.create(FFmpegFrameRecorder.class, String.class,
                videoPath.toAbsolutePath().toString(), width, height);
        recorder.start();
        opencv_core.IplImage image = opencv_core.IplImage.create(width, height, IPL_DEPTH_8U, 3);;
        cvSet(image, cvScalar(r, g, b, 1.0));
        Frame frame = new OpenCVFrameConverter.ToIplImage().convert(image);
        recorder.start();
        recorder.record(frame);
        recorder.setTimestamp(timeUs);
        recorder.record(frame);
        recorder.stop();
        return videoPath;
    }

    @Test
    @Tag("SkipCI")  // Bitbucket's docker container can't write video output due to some unmet ffmpeg dependencies
    public void testParseLogsWithSyntheticVideo() throws IOException {
        /** rgb-100-100-100 was generated using the following code */
        // 1 second video
        // Path inputPath = generateSingleColorVideo(100, 100, 100, 100, 100, 1_000_000);

        Path videosPath = FileSystems.getDefault().getPath(getClass().getResource("/videos").getPath());
        Path inputPath = videosPath.resolve("rgb-100-100-100.mp4");
        List<Path> inputPaths = new LinkedList<Path>() {{
            add(inputPath);
        }};
        Path outputPath = Files.createTempFile("synthetic", ".mp4");
        Pair<Long, Long> onTime = Pair.of(0L, 1_000_000L);
        List<Pair<Long, Long>> screenOn = new LinkedList<Pair<Long, Long>>() {{
           add(onTime);
        }};
        new MockGlobalsBuilder(globals)
                .setGlobalTimeUs(onTime);
        List<PowerEvent> powerEvents = screen.appendVideos(screenOn, inputPaths, outputPath);

        // c + rr*slopeR + rg*slopeG + rb*slopeB
        double expectedCurrent = (100 + (100*0.01) + (100*0.02) + (100*0.03));

        assertEquals(2, powerEvents.size());
        PowerEvent powerEvent = powerEvents.get(0);

        // The current will not be exactly equal to expectedCurrent because of double precision
        // problems and because we scale each frame to just 1% of its size to speed up processing.
        // Scaling frames to 1% moves rgb colors a little bit.
        BaseIntegrationTest.assertEquals(expectedCurrent, powerEvent.current, 0.01 * expectedCurrent);
        assertEquals(0, powerEvent.time);
    }
}
