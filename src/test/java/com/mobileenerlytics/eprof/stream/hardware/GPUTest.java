package com.mobileenerlytics.eprof.stream.hardware;

import com.mobileenerlytics.eprof.stream.BaseIntegrationTest;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.mocks.MockDbgSpliterator;
import com.mobileenerlytics.eprof.stream.mocks.MockDoubleFactory;
import com.mobileenerlytics.eprof.stream.mocks.MockGlobalsBuilder;
import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import com.mobileenerlytics.eprof.stream.util.AtraceHelper;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.util.PowerEventsToFgTasks;
import com.mobileenerlytics.eprof.stream.util.TimeHelper;
import com.mobileenerlytics.eprof.stream.util.mocks.MockXmlBuilder;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import com.mobileenerlytics.symbolic.DoubleFactory;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.context.ApplicationContext;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class GPUTest {
    @InjectMocks
    GPU gpu;

    @Spy
    ApplicationContext context;

    @Spy
    DbgSpliterator dbgSpliterator;

    @Spy
    AtraceHelper atraceHelper;

    @Mock
    PowerEventsToFgTasks powerEventsToFgTasks;

    @Mock
    private Globals globals;

    @Mock
    MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Spy
    TimeHelper timeHelper;

    @Mock
    DoubleFactory doubleFactory;

    Document document;

    @BeforeEach
    public void setup() throws IOException, SAXException, ParserConfigurationException {
        MockitoAnnotations.initMocks(this);
        MockDoubleFactory.mock(doubleFactory);
        document = MockXmlBuilder.fromString(
                "<gpumodel>" +
                    "<gpu id='3d0' default_lvl='1'>" +
                        "<gpufreq freq='5' lvl='1'>" +
                            "<gpustate bus='0' active='10' nap='1' slumber='0' init='0'/>" +
                            "<gpustate bus='1' active='10' nap='1' slumber='0' init='0'/>" +
                        "</gpufreq>" +
                        "<gpufreq freq='6' lvl='0'>" +
                            "<gpustate bus='0' active='20' nap='2' slumber='0' init='0'/>" +
                            "<gpustate bus='1' active='20' nap='2' slumber='0' init='0'/>" +
                        "</gpufreq>" +
                    "</gpu>" +
                "</gpumodel>");

        atraceHelper.timeOffsetUs = 0;
        doReturn(true).when(atraceHelper).inBounds(any(), any());
        when(powerEventsToFgTasks.accountTaskEnergy(any())).then(returnsFirstArg());
        when(movingAvgSamplingCollector.movingAverageByTasks(any())).then(returnsFirstArg());
    }

    @Test
    public void testBuild() {
        gpu.build(document.getElementsByTagName("gpumodel"));
        BaseIntegrationTest.assertEquals(10.0, gpu.activeFreq2Current.get(5L, 0), 0.1);
        BaseIntegrationTest.assertEquals(20.0, gpu.activeFreq2Current.get(6L, 0), 0.1);

        BaseIntegrationTest.assertEquals(1.0, gpu.napFreq2Current.get(5L, 0), 0.1);
        BaseIntegrationTest.assertEquals(2.0, gpu.napFreq2Current.get(6L, 0), 0.1);
    }

    @Test
    public void testFailsBuildWithoutGPUTag() throws IOException, SAXException, ParserConfigurationException {
        Document emptyDocument = MockXmlBuilder.fromString(
                "<gpumodel>" +
                "</gpumodel>");

        assertFalse(gpu.build(emptyDocument.getElementsByTagName("gpumodel")));
    }

    @Test
    public void testParseLogs() throws IOException {
        gpu.build(document.getElementsByTagName("gpumodel"));
        String[] lines = {
                "    kworker/u8:4-20766 (20766) [003] ...1  1.0: kgsl_pwrlevel: d_name=kgsl-3d0 pwrlevel=0 freq=6",
                "    kworker/u8:9-5030  ( 5030) [000] ...1  2.0: kgsl_pwr_set_state: d_name=kgsl-3d0 state=SLUMBER",
                "    RenderThread-12358 (12235) [000] ...1  3.0: kgsl_pwr_set_state: d_name=kgsl-3d0 state=ACTIVE",
                "    kworker/u8:3-12176 (12176) [002] ...1  4.0: kgsl_pwr_set_state: d_name=kgsl-3d0 state=NAP",
                "    RenderThread-12358 (12235) [001] ...1  5.0: kgsl_pwr_set_state: d_name=kgsl-3d0 state=ACTIVE",
                "    kworker/u8:4-20766 (20766) [003] ...1  6.0: kgsl_pwrlevel: d_name=kgsl-3d0 pwrlevel=1 freq=5",
                "    kworker/u8:9-5030  ( 5030) [000] ...1  7.0: kgsl_pwr_set_state: d_name=kgsl-3d0 state=NAP"
        };

        new MockGlobalsBuilder(globals).addTaskDataMap(new HashMap<>());
        MockDbgSpliterator.mockDbgSpliterator(lines, context, dbgSpliterator);
        Map<Integer, List<PowerEvent>> powerEventMap = gpu.parseLogs();

        assertFalse(powerEventMap.isEmpty());
        assertTrue(powerEventMap.containsKey(PHONE_PID));

        assertThat(powerEventMap.get(PHONE_PID), Matchers.contains(
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 1_000_000),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 2_000_000),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(20), 3_000_000),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(2), 4_000_000),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(20), 5_000_000),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(10), 6_000_000),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(1), 7_000_000)
        ));
    }
}
