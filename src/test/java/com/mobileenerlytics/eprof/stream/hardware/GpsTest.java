package com.mobileenerlytics.eprof.stream.hardware;

import com.mobileenerlytics.eprof.stream.BaseIntegrationTest;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.batterystats.EventData;
import com.mobileenerlytics.eprof.stream.mocks.MockDoubleFactory;
import com.mobileenerlytics.eprof.stream.mocks.MockGlobalsBuilder;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.util.TimeHelper;
import com.mobileenerlytics.eprof.stream.util.mocks.MockXmlBuilder;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import com.mobileenerlytics.symbolic.DoubleFactory;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class GpsTest {
    @InjectMocks
    Gps gps;

    @Mock
    Globals globals;

    @Spy
    TimeHelper timeHelper;

    @Mock
    EventData gpsEvent;

    @Mock
    DoubleFactory doubleFactory;

    Document document;
    MockGlobalsBuilder mockGlobalsBuilder;

    private final static int GPS_TASKID_1 = 100;
    private final static int GPS_TASKID_2 = 101;
    private final static int NOGPS_TASKID = 20;

    @BeforeEach
    public void setup() throws IOException, SAXException, ParserConfigurationException {
        MockitoAnnotations.initMocks(this);
        MockDoubleFactory.mock(doubleFactory);

        String xml = " <gpsmodel>" +
                "        <gps active='15'/>" +
                "    </gpsmodel>";

        document = MockXmlBuilder.fromString(xml);

        doReturn(true).when(timeHelper).within(anyLong(), any());

        TaskData gpsTask1 = mock(TaskData.class);
        gpsTask1.taskId = GPS_TASKID_1;
        gpsTask1.taskName = "in.sweatco.app";

        TaskData gpsTask2 = mock(TaskData.class);
        gpsTask2.taskId = GPS_TASKID_2;
        gpsTask2.taskName = "com.google.android.gms";

        TaskData nogpsTask = mock(TaskData.class);
        nogpsTask.taskId = NOGPS_TASKID;
        nogpsTask.taskName = "nogps";

        gpsEvent.name = "gps";
        gpsEvent.eventName = "gps";
        doReturn(PHONE_PID).when(gpsEvent).getPid();
        doReturn(1L).when(gpsEvent).getStartTimeMs();
        doReturn(100L).when(gpsEvent).getEndTimeMs();

        mockGlobalsBuilder = new MockGlobalsBuilder(globals).addTaskDataMap(new HashMap<Integer, TaskData>() {{
                put(GPS_TASKID_1, gpsTask1);
                put(GPS_TASKID_2, gpsTask2);
                put(NOGPS_TASKID, nogpsTask);
            }})
            .addFile("location", "in.sweatco.app: fused: Interval 1 seconds: Duration requested 450 out of the last 1736 minutes\n" +
                    "    in.sweatco.app: gps: Interval 0 seconds: Duration requested 0 out of the last 1736 minutes\n" +
                    "    com.google.android.gms: gps: Interval 0 seconds: Duration requested 1736 out of the last 1736 minutes: Currently active")
            .addEventDataToTask(PHONE_PID, new LinkedList<EventData>() {{
                add(gpsEvent);
            }});

    }

    @Test
    public void testBuild() {
        assertTrue(gps.build(document.getElementsByTagName("gpsmodel")));
        BaseIntegrationTest.assertEquals(15.0, gps.activeCurrent, 0.1);
    }

    @Test
    public void testParseLogsNoGps() throws IOException {
        gps.build(document.getElementsByTagName("gpsmodel"));

        Map<Integer, List<PowerEvent>> taskCurrents = gps.parseLogs();
        assertFalse(taskCurrents.isEmpty());

        assertTrue(taskCurrents.containsKey(PHONE_PID));
        assertThat(taskCurrents.get(PHONE_PID), Matchers.contains(
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(15), 1_000),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 100_000)
        ));

        assertFalse(taskCurrents.containsKey(NOGPS_TASKID));
        assertTrue(taskCurrents.get(GPS_TASKID_1).isEmpty());
        assertTrue(taskCurrents.get(GPS_TASKID_2).isEmpty());
    }

    @Test
    public void testParseLogsWakelockSplit() throws IOException {
        gps.build(document.getElementsByTagName("gpsmodel"));

        mockGlobalsBuilder.addEventDataToTask(GPS_TASKID_1, new LinkedList<EventData>() {{
            add(mockWLEvent(GPS_TASKID_1, 2, 7));
            add(mockWLEvent(GPS_TASKID_1, 11,16));
        }});

        mockGlobalsBuilder.addEventDataToTask(GPS_TASKID_2, new LinkedList<EventData>() {{
            add(mockWLEvent(GPS_TASKID_2, 15,20));
        }});

        mockGlobalsBuilder.addEventDataToTask(NOGPS_TASKID, new LinkedList<EventData>() {{
            add(mockWLEvent(NOGPS_TASKID, 0, 20));
        }});

        Map<Integer, List<PowerEvent>> taskCurrents = gps.parseLogs();

        assertFalse(taskCurrents.isEmpty());
        assertFalse(taskCurrents.containsKey(NOGPS_TASKID));

        assertThat(taskCurrents.get(GPS_TASKID_1), Matchers.contains(
                new PowerEvent(GPS_TASKID_1, new ConcreteUnmodifiableDouble(10), 1_000),
                new PowerEvent(GPS_TASKID_1, new ConcreteUnmodifiableDouble(0), 100_000)
        ));

        assertThat(taskCurrents.get(GPS_TASKID_2), Matchers.contains(
                new PowerEvent(GPS_TASKID_2, new ConcreteUnmodifiableDouble(5), 1_000),
                new PowerEvent(GPS_TASKID_2, new ConcreteUnmodifiableDouble(0), 100_000)
        ));
    }

    private EventData mockWLEvent(int pid, long startTimeMs, long endTimeMs) {
        EventData mockWakelock = mock(EventData.class);
        mockWakelock.name = "LocationManagerService";
        mockWakelock.eventName = "wake_lock_in";
        doReturn(pid).when(mockWakelock).getPid();
        doReturn(startTimeMs).when(mockWakelock).getStartTimeMs();
        doReturn(endTimeMs).when(mockWakelock).getEndTimeMs();
        return mockWakelock;
    }
}
