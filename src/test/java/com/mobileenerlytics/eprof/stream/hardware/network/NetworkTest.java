package com.mobileenerlytics.eprof.stream.hardware.network;

import com.att.aro.core.configuration.IProfileFactory;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.batterystats.EventData;
import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.mocks.MockGlobalsBuilder;
import com.mobileenerlytics.eprof.stream.util.GsonHelper;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.writers.CsvWriter;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class NetworkTest {
    @Spy
    @InjectMocks
    Network network;

    @Mock
    MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Mock
    EventData wifiEvent;

    @Mock
    IProfileFactory profileFactory;

    @Mock
    Globals globals;

    @Mock
    CsvWriter csvWriter;

    @Mock
    NetworkTablesParser networkTablesParser;

    @Mock
    Conversation c1 = new Conversation();

    @Mock
    Conversation c2 = new Conversation();

    @Mock
    Conversation c1Total = new Conversation();

    @Mock
    Conversation c2Total = new Conversation();

    @Mock
    GsonHelper gsonHelper;

    MockGlobalsBuilder mockGlobalsBuilder;

    @BeforeEach
    public void setup() throws IOException {
        MockitoAnnotations.initMocks(this);
        when(movingAvgSamplingCollector.movingAverageByTasks(any())).then(returnsFirstArg());
        mockGlobalsBuilder = new MockGlobalsBuilder(globals).addTaskDataMap(new HashMap<>());

        doReturn(1).when(c1).getId();
        doReturn(2).when(c2).getId();
        doReturn(3).when(c1Total).getId();
        doReturn(4).when(c2Total).getId();

        doReturn(new byte[0]).when(gsonHelper).toJsonBytes(any());
    }

    @Test
    public void testGetNetworkProfileLTE() throws IOException {
        network.getNetworkProfile();
        verify(profileFactory, times(1)).createLTEFromDefaultResourceFile();
    }

    @Test
    public void testGetNetworkProfileWiFi() throws IOException {
        wifiEvent.eventName = "wifi";
        doReturn(100L).when(wifiEvent).getDurationMs();
        mockGlobalsBuilder.addEventDataToTask(PHONE_PID, new LinkedList<EventData>() {{
            add(wifiEvent);
        }});

        network.getNetworkProfile();
        verify(profileFactory, times(1)).createWiFiFromFilePath(anyString());
    }

    private void setupConversations() throws IOException {
        doReturn(new LinkedList<Conversation>() {{
            add(c1);
            add(c2);
        }}).when(network).getConversations();

        doReturn(c1Total, c2Total).when(network).getTotalConversation(anyList());

        Map<Integer, List<Conversation>> conversationMap = new HashMap<Integer, List<Conversation>> () {{
            put(1, new LinkedList<Conversation>() {{
                add(c1);
                add(c2);
            }});
        }};

        when(networkTablesParser.matchProcesses(any())).thenReturn(conversationMap);
    }

    @Test
    public void testParseLogsNetworkEvent() throws IOException {
        setupConversations();
        List<NetworkEvent> l1 = new LinkedList<NetworkEvent>() {{
            add(new NetworkEvent(0,100));
            add(new NetworkEvent(2,200));
            add(new NetworkEvent(4,400));
        }};
        doReturn(l1).when(c1).getNetworkEvents();

        List<NetworkEvent> l2 = new LinkedList<NetworkEvent>() {{
            add(new NetworkEvent(0,100));
            add(new NetworkEvent(3,300));
            add(new NetworkEvent(6,600));
        }};

        doReturn(l2).when(c2).getNetworkEvents();


        network.parseLogs();

        // network_traffic.csv
        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);
        verify(csvWriter).writeCsv(any(), captor.capture(), anyLong(), any());

        // Since, there is only 1 pid, getTotalConversation should only be computed once.
        verify(network, times(1)).getTotalConversation(any());
        // conversation c1
        assertThat((List<NetworkEvent>)captor.getValue().get(1), contains(l1.toArray(new NetworkEvent[0])));
        // conversation c2
        assertThat((List<NetworkEvent>)captor.getValue().get(2), contains(l2.toArray(new NetworkEvent[0])));
    }

    @Test
    public void testParseLogsPowerEvents() throws IOException {
        setupConversations();

        List<PowerEvent> l1 = new LinkedList<PowerEvent>() {{
            add(new PowerEvent(1, new ConcreteUnmodifiableDouble(10),100));
            add(new PowerEvent(1, new ConcreteUnmodifiableDouble(20),200));
            add(new PowerEvent(1, new ConcreteUnmodifiableDouble(40),400));
        }};
        doReturn(l1).when(c1).getPowerEvents();

        List<PowerEvent> l2 = new LinkedList<PowerEvent>() {{
            add(new PowerEvent(1, new ConcreteUnmodifiableDouble(30),300));
            add(new PowerEvent(1, new ConcreteUnmodifiableDouble(60),600));
            add(new PowerEvent(1, new ConcreteUnmodifiableDouble(80),800));
        }};
        doReturn(l2).when(c2).getPowerEvents();

        network.parseLogs();

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);
        verify(csvWriter).writePowerCsv(any(), captor.capture(), any());

        assertThat((List<PowerEvent>)captor.getValue().get(1), contains(l1.toArray(new PowerEvent[0])));
        assertThat((List<PowerEvent>)captor.getValue().get(2), contains(l2.toArray(new PowerEvent[0])));
    }
}
