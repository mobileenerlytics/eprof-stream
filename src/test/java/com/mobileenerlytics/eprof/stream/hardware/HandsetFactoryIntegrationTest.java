package com.mobileenerlytics.eprof.stream.hardware;

import com.mobileenerlytics.eprof.stream.BaseIntegrationTest;
import com.mobileenerlytics.eprof.stream.hardware.network.Network;
import com.mobileenerlytics.eprof.stream.readers.PropsReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

import static junit.framework.TestCase.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.matches;
import static org.mockito.Mockito.when;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class HandsetFactoryIntegrationTest extends BaseIntegrationTest {
    @Autowired @InjectMocks
    HandsetFactory handsetFactory;

    @Mock
    PropsReader propsReader;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testShamu() throws IOException, SAXException, ParserConfigurationException {
        when(propsReader.getProperty(matches("cpu.cores"), anyString())).thenReturn("4");
        when(propsReader.getProperty(matches("ro.product.device"), anyString())).thenReturn("shamu");
        Handset handset = handsetFactory.buildHandset();

        assertFalse(handset.hardwares.isEmpty());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof CPU).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof GPU).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof Gps).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof Network).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof Screen).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof HwDecoder).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof DRM).findAny().isPresent());

        assertFalse(handset.hardwares.stream().filter(s -> s instanceof MaliGPU).findAny().isPresent());
    }

    @Test
    public void testAngler() throws IOException, SAXException, ParserConfigurationException {
        when(propsReader.getProperty(matches("cpu.cores"), anyString())).thenReturn("8");
        when(propsReader.getProperty(matches("ro.product.device"), anyString())).thenReturn("angler");
        Handset handset = handsetFactory.buildHandset();

        assertFalse(handset.hardwares.isEmpty());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof CPU).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof GPU).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof Gps).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof Network).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof Screen).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof HwDecoder).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof DRM).findAny().isPresent());

        assertFalse(handset.hardwares.stream().filter(s -> s instanceof MaliGPU).findAny().isPresent());
    }

    @Test
    public void testHuaweiP10Lite() throws IOException, SAXException, ParserConfigurationException {
        when(propsReader.getProperty(matches("cpu.cores"), anyString())).thenReturn("8");
        when(propsReader.getProperty(matches("ro.product.device"), anyString())).thenReturn("HWWAS-H");
        Handset handset = handsetFactory.buildHandset();

        assertFalse(handset.hardwares.isEmpty());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof CPU).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof Gps).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof Network).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof Screen).findAny().isPresent());
        assertTrue(handset.hardwares.stream().filter(s -> s instanceof MaliGPU).findAny().isPresent());

        assertFalse(handset.hardwares.stream().filter(s -> s instanceof GPU).findAny().isPresent());
        assertFalse(handset.hardwares.stream().filter(s -> s instanceof HwDecoder).findAny().isPresent());
        assertFalse(handset.hardwares.stream().filter(s -> s instanceof DRM).findAny().isPresent());
    }
}
