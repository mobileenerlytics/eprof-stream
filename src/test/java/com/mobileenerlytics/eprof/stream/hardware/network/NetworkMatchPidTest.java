package com.mobileenerlytics.eprof.stream.hardware.network;

import com.mobileenerlytics.eprof.stream.mocks.MockConversationBuilder;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.UNKNOWN_PID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

public class NetworkMatchPidTest {
    @Mock
    Conversation unknown, c1, c2, cMatch;

    final int PID_MATCH = 0;

    @Spy
    @InjectMocks
    NetworkTablesParser networkTablesParser;

    Map<Integer, List<Conversation>> pidConversationMap;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MockConversationBuilder.buildConversation(unknown, "255.255.0.0","46747", "255.255.255.255", "443", "remote-name-unknown");
        pidConversationMap = new HashMap<>();
        pidConversationMap.put(UNKNOWN_PID, new LinkedList<>());

        pidConversationMap.put(1, new LinkedList<Conversation>(){{
            add(c1);    // This should be matched with unknown
        }});
        pidConversationMap.put(2, new LinkedList<Conversation>(){{
            add(c2);
        }});
        pidConversationMap.put(PID_MATCH, new LinkedList<Conversation>(){{
            add(cMatch);
        }});
    }

    @Test
    public void testUnknown() {
        pidConversationMap.remove(1);
        pidConversationMap.remove(2);
        pidConversationMap.remove(PID_MATCH);
        networkTablesParser.bestEffortMapToPid(unknown, pidConversationMap);
        assertThat(pidConversationMap.get(UNKNOWN_PID), Matchers.contains(unknown));
    }

    @Test
    public void testMapPidByRemoteHostName() {
        // Remote name is 6 Levenshtein distance from unknown conversation's remoteName
        MockConversationBuilder.buildConversation(c1, "255.255.0.0","46747", "0.0.0.0", "443", "remote-name-abcd");

        // Remote name is 2 Levenshtein distance from unknown conversation's remoteName
        MockConversationBuilder.buildConversation(cMatch, "255.255.0.0","46747", "0.0.0.0", "443", "remote-name-known");

        // Has a different local IP, thus shouldn't be matched
        MockConversationBuilder.buildConversation(c2, "255.255.0.1","46747", "0.0.0.0", "443", "remote-name-unknown");

        doReturn(true).when(unknown).hasRemoteName();
        doReturn(true).when(c1).hasRemoteName();
        doReturn(true).when(c2).hasRemoteName();
        doReturn(true).when(cMatch).hasRemoteName();

        assertEquals(PID_MATCH, networkTablesParser.mapPidByRemoteHostName(unknown, pidConversationMap));
    }

    @Test
    public void testMapPidByRemoteIPPrefix() {
        // Longest IP prefix match if seen in decimal, but not if seen in bitstring
        MockConversationBuilder.buildConversation(c1, "255.255.0.0","46747", "255.255.255.25", "443", "");

        // Local IP doesn't match
        MockConversationBuilder.buildConversation(c2, "255.255.0.1","46747", "255.255.255.255", "443", "");

        // Longest IP prefix in bitstring. Must get matched
        MockConversationBuilder.buildConversation(cMatch, "255.255.0.0","46747", "255.255.255.240", "443", "");

        assertEquals(PID_MATCH, networkTablesParser.mapPidByRemoteIPPrefix(unknown, pidConversationMap));
    }
}
