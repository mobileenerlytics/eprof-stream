package com.mobileenerlytics.eprof.stream.hardware.network;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.mocks.MockConversationBuilder;
import com.mobileenerlytics.eprof.stream.mocks.MockGlobalsBuilder;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.io.IOException;
import java.util.*;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.UNKNOWN_PID;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class NetworkTablesParserTest {
    final String tcpData1 = "   0: 0000FFFF:B69B 0000FFFF:01BB 01 00000000:00000000 00:00000000 00000000 11120        0 949911 1 00000000 22 3 27 10 -1";
    final String tcpData2 = "   0: 0E01A8C0:B69C A2C23AD8:01BD 01 00000000:00000000 00:00000000 00000000 10024        0 949911 1 00000000 22 3 27 10 -1";

    @Mock
    Globals globals;

    @Mock
    Conversation conversation;

    @Spy
    @InjectMocks
    NetworkTablesParser networkTablesParser;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MockConversationBuilder.buildConversation(conversation, "255.255.0.0","46747", "255.255.0.0", "443", "255.255.0.0");
    }

    @Test
    public void testTCPMatchProcesses() throws IOException {
        final int taskId = 5;
        final String tcpHeader = " sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  timeout inode";
        doReturn(taskId).when(networkTablesParser).getPID(any());
        doReturn(Collections.singleton(conversation)).when(networkTablesParser).getMatchingConversations(eq(tcpData1));
        Map<Integer, List<Conversation>> processMatch = runTCPMatchProcesses(String.format("%s\n%s", tcpHeader, tcpData1));

        assertTrue(processMatch.containsKey(taskId));
        Conversation c = processMatch.get(taskId).get(0);
        assertEquals("255.255.0.0:46747", c.localIP + ":" + c.localPort);
        assertEquals("255.255.0.0:443", c.remoteIP + ":" + c.remotePort);
    }

    @Test
    public void testTCPNonMatchProcesses() throws IOException {
        final int taskId = 6;
        final String tcpHeader = " sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  timeout inode";
        doReturn(UNKNOWN_PID).when(networkTablesParser).mapPidByRemoteHostName(any(), any());
        doReturn(UNKNOWN_PID).when(networkTablesParser).mapPidByRemoteIPPrefix(any(), any());
        doReturn(UNKNOWN_PID).when(networkTablesParser).getPID(eq(tcpData2));
        doReturn(taskId).when(networkTablesParser).getPID(eq(tcpData2));
        doReturn(Collections.singleton(conversation)).when(networkTablesParser).getMatchingConversations(eq(tcpData1));
        doReturn(new LinkedList<>()).when(networkTablesParser).getMatchingConversations(eq(tcpData2));
        Map<Integer, List<Conversation>> processMatch = runTCPMatchProcesses(String.format("%s\n%s\n%s", tcpHeader, tcpData1, tcpData2));

        // taskId should not be in results because tcpData1 should return -1 for PID and tcpData2 should never be looked at because its local/remote
        // is are not in the conversation.
        assertFalse(processMatch.containsKey(taskId));
    }

    private Map<Integer, List<Conversation>> runTCPMatchProcesses(String tcpData) throws IOException {
        new MockGlobalsBuilder(globals).addFile("tcp", tcpData);
        final List<Conversation> conversationsList = new ArrayList<Conversation>() {{
            add(conversation);
        }};
        return networkTablesParser.matchProcesses(conversationsList);
    }

    @Test
    public void testGetConversations() {
        networkTablesParser.conversationTable.put("255.255.0.0:46747", "255.255.0.0:443", Collections.singletonList(conversation));
        assertThat(networkTablesParser.getMatchingConversations(tcpData1), Matchers.contains(conversation));
        assertTrue(networkTablesParser.getMatchingConversations(tcpData2).isEmpty());
    }

    @Test
    public void testGetUID() {
        final String tcpData3 = "   0: 0E01A8C0:B69C A2C23AD8:01BD 01 00000000:00000000 00:00000000 00000000 00000        0 949911 1 00000000 22 3 27 10 -1";
        final String expectedUID1 = "app_1120";
        final String expectedUID2 = "app_24";

        assertEquals(expectedUID1, networkTablesParser.getUID(tcpData1));
        assertEquals(expectedUID2, networkTablesParser.getUID(tcpData2));
        assertEquals(String.valueOf(UNKNOWN_PID), networkTablesParser.getUID(tcpData3));
    }

    @Test
    public void testGetPID() throws IOException {
        final TaskData taskData = mock(TaskData.class);
        taskData.taskId = 5;
        taskData.userName = "app_1120";
        doReturn(true).when(taskData).isProc();
        final Map<Integer, TaskData> taskDataMap = new HashMap<Integer, TaskData>() {{
          put(taskData.taskId, taskData);
        }};
        new MockGlobalsBuilder(globals).addTaskDataMap(taskDataMap);

        assertEquals(taskData.taskId, networkTablesParser.getPID(tcpData1));
        assertEquals(UNKNOWN_PID, networkTablesParser.getPID(tcpData2));
    }

    @Test
    public void testHexToIPv4() {
        final String hex1 = "ECF65434:0050";
        final String hex2 = "748B1968:01BB";
        final String tcpHex = "0000000000000000FFFF0000CEC23AD8:01BB";
        final String expectedIP1 = "52.84.246.236:80";
        final String expectedIP2 = "104.25.139.116:443";
        final String expectedTcpIP = "216.58.194.206:443";

        assertEquals(expectedIP1, networkTablesParser.hexToIP4(hex1));
        assertEquals(expectedIP2, networkTablesParser.hexToIP4(hex2));
        assertEquals(expectedTcpIP, networkTablesParser.hexToIP4(tcpHex));
    }

    @Test
    public void testHexToIPv6() {
        final String hex = "2430032600E2030EE4244210A1080000:BD81";
        assertEquals("2603:3024:e03:e200:1042:24e4:0:8a1:48513", networkTablesParser.hexToIP6(hex));
    }
}
