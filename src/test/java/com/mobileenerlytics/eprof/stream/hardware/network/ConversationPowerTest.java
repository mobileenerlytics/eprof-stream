package com.mobileenerlytics.eprof.stream.hardware.network;

import com.att.aro.core.packetanalysis.pojo.PacketInfo;
import com.att.aro.core.packetanalysis.pojo.RRCState;
import com.att.aro.core.packetanalysis.pojo.RrcStateRange;
import com.att.aro.core.packetanalysis.pojo.Session;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.util.TimeHelper;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConversationPowerTest {
    @InjectMocks
    Conversation conversation = new Conversation();

    @Spy
    TimeHelper timeHelper;

    @Mock
    Globals globals;

    @BeforeEach
    public void setup() throws UnknownHostException {
        MockitoAnnotations.initMocks(this);
        when(globals.getGlobalTimeUs()).thenReturn(Pair.of(0L, 10_000L));

        Session session = mock(Session.class);
        when(session.getLocalIP()).thenReturn(InetAddress.getByName("0.0.0.0"));
        when(session.getLocalPort()).thenReturn(12345);
        when(session.getRemoteIP()).thenReturn(InetAddress.getByName("0.0.0.0"));
        when(session.getRemotePort()).thenReturn(12345);

        conversation.setSession(session);
    }

    @Test
    public void testNetworkPower0() {
        PacketInfo packetInfo1 = mock(PacketInfo.class);
        when(packetInfo1.getStateRangeList()).thenReturn(
                createTestRrcStateRangeList(
                        tuple(0, 1000, 5),  // 5 current
                        tuple(1000, 1500, 10)));    // 20 current


        List<PacketInfo> packetInfos = new ArrayList<PacketInfo>() {{
            add(packetInfo1);
        }};

        List<PowerEvent> result = conversation.calculatePower(packetInfos);

        List<PowerEvent> answer = new ArrayList<>();
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(5.0), 0));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(20.0), 1000));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(0.0), 1500));

        assertEquals(answer, result);
    }

    @Test
    public void testNetworkPower1() {
        PacketInfo packetInfo1 = mock(PacketInfo.class);
        when(packetInfo1.getStateRangeList()).thenReturn(
                createTestRrcStateRangeList(
                        tuple(0, 1000, 5),  // 5 current
                        tuple(1000, 1500, 10)));    // 20 current

        PacketInfo packetInfo2 = mock(PacketInfo.class);
        when(packetInfo2.getStateRangeList()).thenReturn(
                createTestRrcStateRangeList(
                        tuple(1500, 2000, 10),  // 20 current
                        tuple(2000, 5000, 2.4)));   // 0.8 current

        List<PacketInfo> packetInfos = new ArrayList<PacketInfo>() {{
            add(packetInfo1);
            add(packetInfo2);
        }};

        List<PowerEvent> result = conversation.calculatePower(packetInfos);

        List<PowerEvent> answer = new ArrayList<>();
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(5.0), 0));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(20.0), 1000));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(20.0), 1500));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(0.8), 2000));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(0), 5000));

        assertEquals(answer, result);
    }

    @Test
    public void testNetworkPower2() {
        PacketInfo packetInfo1 = mock(PacketInfo.class);
        when(packetInfo1.getStateRangeList()).thenReturn(
                createTestRrcStateRangeList(
                        tuple(0, 1000, 2.5),    // 2.5 current
                        tuple(1000, 2000, 5),   // 5 current
                        tuple(2000, 5000, 1.2)));   // 0.4 current

        PacketInfo packetInfo2 = mock(PacketInfo.class);
        when(packetInfo2.getStateRangeList()).thenReturn(
                createTestRrcStateRangeList(
                        tuple(0, 1000, 2.5),    // 2.5 current
                        tuple(1000, 2000, 5),   // 5 current
                        tuple( 2000, 5000, 1.2)));  // 0.4 current

        List<PacketInfo> packetInfos = new ArrayList<PacketInfo>() {{
            add(packetInfo1);
            add(packetInfo2);
        }};

        List<PowerEvent> result = conversation.calculatePower(packetInfos);

        List<PowerEvent> answer = new ArrayList<>();
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(5.0), 0));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(10.0), 1000));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(0.8), 2000));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(0), 5000));

        assertEquals(answer, result);
    }

    @Test
    public void testNetworkPower3() {
        PacketInfo packetInfo1 = mock(PacketInfo.class);
        when(packetInfo1.getStateRangeList()).thenReturn(
                createTestRrcStateRangeList(
                        tuple(0, 1000, 2.5),    // 2.5 current
                        tuple(1000, 1500, 10),  // 20 current
                        tuple(2000, 5000, 1.2)));   // 0.4 current

        PacketInfo packetInfo2 = mock(PacketInfo.class);
        when(packetInfo2.getStateRangeList()).thenReturn(
                createTestRrcStateRangeList(
                        tuple(0, 1000, 2.5),    // 2.5 current
                        tuple(1500, 2000, 10),  // 20 current
                        tuple( 2000, 5000, 1.2)));  // 0.4 current

        List<PacketInfo> packetInfos = new ArrayList<PacketInfo>() {{
            add(packetInfo1);
            add(packetInfo2);
        }};

        List<PowerEvent> result = conversation.calculatePower(packetInfos);

        List<PowerEvent> answer = new ArrayList<>();
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(5.0), 0));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(20.0), 1000));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(20.0), 1500));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(0.8), 2000));
        answer.add(new PowerEvent(conversation.id, new ConcreteUnmodifiableDouble(0), 5000));

        assertEquals(answer, result);
    }

    private double[] tuple(double start, double end, double energy){
        // Time is in us, convert to seconds
        // This energy is in uAh, convert to Joules
        return new double[]{start/1e6, end/1e6, energy*3.7/1e6};
    }

    private RrcStateRange createTestRrcStateRange(double start, double end, double energy){
        RrcStateRange stateRange = new RrcStateRange(start, end, RRCState.PROMO_IDLE_DCH);
        stateRange.setEnergy(energy);
        return stateRange;
    }

    private List<RrcStateRange> createTestRrcStateRangeList(double[]... rawStateRanges) {
        List<RrcStateRange> stateRangeList = new ArrayList<>();
        for(int i = 0; i < rawStateRanges.length; i++) {
            RrcStateRange stateRange = createTestRrcStateRange(rawStateRanges[i][0], rawStateRanges[i][1],
                    rawStateRanges[i][2]);
            stateRangeList.add(stateRange);
        }
        return stateRangeList;
    }
}
