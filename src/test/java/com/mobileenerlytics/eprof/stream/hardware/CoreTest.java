package com.mobileenerlytics.eprof.stream.hardware;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.mocks.MockDoubleFactory;
import com.mobileenerlytics.eprof.stream.mocks.MockGlobalsBuilder;
import com.mobileenerlytics.eprof.stream.readers.CoreReader;
import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.util.mocks.MockXmlBuilder;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import com.mobileenerlytics.symbolic.DoubleFactory;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.context.ApplicationContext;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;

import static com.mobileenerlytics.eprof.stream.BaseIntegrationTest.assertEquals;
import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static java.util.stream.Collectors.toMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.*;

public class CoreTest {
    @Spy
    ApplicationContext context;

    DbgSpliterator[] spliterators = new DbgSpliterator[5];

    @Mock
    Globals globals;

    @Mock
    DoubleFactory doubleFactory;

    @Mock
    MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Mock
    CoreReader coreReader;

    @InjectMocks
    Core core = new Core(0, 1);

    @BeforeEach
    public void setup() throws IOException, SAXException, ParserConfigurationException {
        MockitoAnnotations.initMocks(this);
        MockDoubleFactory.mock(doubleFactory);
        for(int i = 0; i < spliterators.length; i ++)
            spliterators[i] = spy(DbgSpliterator.class);

        Document xmlEntry = MockXmlBuilder.fromString(
                "<cores idle='0'>" +
                        "<core>" +
                            "<state mode='multicore' freq='1' active='10'/>" +
                            "<state mode='multicore' freq='2' active='20'/>" +
                        "</core> + " +
                    "</cores>"
        );

        core.setIdleCurrent(new ConcreteUnmodifiableDouble(0));
        core.addState(xmlEntry.getElementsByTagName("state").item(0));
        core.addState(xmlEntry.getElementsByTagName("state").item(1));
        new MockGlobalsBuilder(globals).addTaskDataMap(new HashMap<>());
        when(movingAvgSamplingCollector.movingAverageByTasks(any())).then(returnsFirstArg());
    }

    @Test
    public void testBuild() {
        core.verifyFreqs(new long[]{1, 2});

        assertEquals(10, core.activeFreq2Current.get(1L), 0.1);
        assertEquals(20, core.activeFreq2Current.get(2L), 0.1);
    }

    @Test
    public void testVerifyFrequency() {
        assertThrows(RuntimeException.class, () -> core.verifyFreqs(new long[]{1, 2, 3}));
    }

    @Test
    public void testGetActiveCurrent() {
        String s = "<...>-10074 (-----) [000] ...1  1.0: cpu_frequency: state=1 cpu_id=0";
        assertEquals(10, core.getActiveCurrent(s), 0.1);
    }

    @Test
    public void testGetIdleInCurrent() {
        String s = "<idle>-0     (-----) [000] .N.2  2.0: cpu_idle: state=1 cpu_id=0";
        assertEquals(0, core.getIdleCurrent(s, new ConcreteUnmodifiableDouble(200)), 0.1);
    }

    @Test
    public void testGetIdleOutCurrent() {
        String s = "<idle>-0     (-----) [000] .N.2  2.0: cpu_idle: state=4294967295 cpu_id=0";
        assertEquals(200, core.getIdleCurrent(s, new ConcreteUnmodifiableDouble(200)), 0.1);
    }

    @Test
    public void testParseLogs() {
        PowerEvent[] phoneEvents = new PowerEvent[] {
            new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 0),
            new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(10), 1),
            new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(20), 2),
            new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 10)
        };

        Mockito.doReturn(Arrays.stream(phoneEvents)
                .collect(toMap(PowerEvent::getTimeUs, Function.identity(), (s1, s2) -> s2, TreeMap::new)))
                .when(coreReader)
                .getPhonePowerEvents(eq(core));

        Mockito.doReturn(new HashMap<>())
                .when(coreReader)
                .getTaskToPowerEventsListMap(eq(core), any());

        Map<Integer, List<PowerEvent>> taskEvents = core.parseLogs();
        assertThat(taskEvents.get(PHONE_PID), Matchers.contains(phoneEvents));
    }
}
