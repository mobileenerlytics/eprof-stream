package com.mobileenerlytics.eprof.stream.hardware;

import com.mobileenerlytics.eprof.stream.BaseIntegrationTest;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.mocks.MockDbgSpliterator;
import com.mobileenerlytics.eprof.stream.mocks.MockDoubleFactory;
import com.mobileenerlytics.eprof.stream.mocks.MockGlobalsBuilder;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import com.mobileenerlytics.eprof.stream.util.AtraceHelper;
import com.mobileenerlytics.eprof.stream.util.MovingAvgSamplingCollector;
import com.mobileenerlytics.eprof.stream.util.PowerEventsToFgTasks;
import com.mobileenerlytics.eprof.stream.util.mocks.MockXmlBuilder;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import com.mobileenerlytics.symbolic.DoubleFactory;
import org.apache.commons.lang3.tuple.Pair;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.context.ApplicationContext;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class DRMTest {
    @InjectMocks
    DRM drm;

    @Mock
    Globals globals;

    @Spy
    private ApplicationContext context;

    @Spy
    private DbgSpliterator dbgSpliterator;

    @Mock
    private PowerEventsToFgTasks powerEventsToFgTasks;

    @Mock
    private MovingAvgSamplingCollector movingAvgSamplingCollector;

    @Spy
    private AtraceHelper atraceHelper;

    @Mock
    private DoubleFactory doubleFactory;

    @BeforeEach
    public void setup() throws IOException, SAXException, ParserConfigurationException {
        MockitoAnnotations.initMocks(this);
        MockDoubleFactory.mock(doubleFactory);
        String xml = "<drm tailms='1000' on='50'/>";

        Document document = MockXmlBuilder.fromString(xml);
        drm.build(document.getChildNodes());

        TaskData mediadrmserver = mock(TaskData.class);
        mediadrmserver.taskId = 1;
        mediadrmserver.parentTaskId = 1;
        mediadrmserver.taskName = "mediadrmserver";
        new MockGlobalsBuilder(globals)
                .setGlobalTimeUs(Pair.of(0L, 10_000_000L))
                .addTaskDataMap(new HashMap<Integer, TaskData>() {{
                    put(1, mediadrmserver);
                }});

        atraceHelper.timeOffsetUs = 0;
        doReturn(true).when(atraceHelper).inBounds(any(), any());
        when(powerEventsToFgTasks.accountTaskEnergy(any())).then(returnsFirstArg());
        when(movingAvgSamplingCollector.movingAverageByTasks(any())).then(returnsFirstArg());
    }

    @Test
    public void build() {
        assertEquals(1000_000, drm.tailUs);
        BaseIntegrationTest.assertEquals(50, drm.on, 0.1);
    }

    @Test
    public void testParsing() {
        String[] lines = {
                "    mediadrmserver (1) [000] ...1  1.0: sched_switch: prev_pid=2 next_pid=1",
                "    mediadrmserver (1) [000] ...1  1.9: sched_switch: prev_pid=2 next_pid=3",
                "    mediadrmserver (1) [000] ...1  1.9: sched_switch: prev_pid=2 next_pid=1",
                "    mediadrmserver (1) [000] ...1  4.0: sched_switch: prev_pid=2 next_pid=1",
        };
        MockDbgSpliterator.mockDbgSpliterator(lines, context, dbgSpliterator);
        Map<Integer, List<PowerEvent>> powerEventMap = drm.parseLogs();
        assertEquals(1, powerEventMap.size());
        assertThat(powerEventMap.get(PHONE_PID), Matchers.contains(
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 0),    // off at start time
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(50), 1_000_000),   // on, since seen in trace
                // no on event at 1.2 seconds since the next_pid is not mediadrmserver pid
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(50), 1_900_000),   // on
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 2_900_000),    // off, because of timeout of 1 seconds
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(50), 4_000_000),   // on
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 5_000_000),    // off because of timeout of 1 second
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 10_000_000)    // off at end time
        ));
    }
}
