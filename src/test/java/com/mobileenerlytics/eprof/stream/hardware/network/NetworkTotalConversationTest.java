package com.mobileenerlytics.eprof.stream.hardware.network;

import com.att.aro.core.packetanalysis.pojo.PacketInfo;
import com.att.aro.core.packetanalysis.pojo.Session;
import com.mobileenerlytics.eprof.stream.mocks.MockSessionBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.context.ApplicationContext;

import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

import static com.mobileenerlytics.eprof.stream.mocks.MockSessionBuilder.mockPacketInfo;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class NetworkTotalConversationTest {
    @InjectMocks
    @Spy
    Network network;

    @Mock
    Session a, b, c;

    @Mock
    Conversation aConv = new Conversation();

    @Mock
    Conversation bConv = new Conversation();

    @Mock
    Conversation totalConv = new Conversation();

    @Mock
    ApplicationContext context;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        doReturn(c).when(network).createSession(any(), anyInt(), any(), anyInt());
        aConv.session = a;
        bConv.session = b;
        doReturn(totalConv).when(context).getBean(eq(Conversation.class), any());
    }

    @Test
    public void testGetTotalSessionsPacketTimestamp() throws UnknownHostException {
        new MockSessionBuilder().setPacketInfoList(new LinkedList<PacketInfo>() {{
            add(mockPacketInfo(10));
            add(mockPacketInfo(30));
            add(mockPacketInfo(50));
        }}).build(a);

        new MockSessionBuilder().setPacketInfoList(new LinkedList<PacketInfo>() {{
            add(mockPacketInfo(20));
            add(mockPacketInfo(40));
        }}).build(b);

        network.getTotalConversation(new LinkedList<Conversation>() {{
            add(aConv);
            add(bConv);
        }});
        ArgumentCaptor<List<PacketInfo>> captor = ArgumentCaptor.forClass(List.class);
        verify(c).setPackets(captor.capture());
        assertEquals(5, captor.getValue().size());
        assertArrayEquals(new double[]{10, 20, 30, 40, 50},
                captor.getValue().stream().mapToDouble(PacketInfo::getTimeStamp).toArray(), 0.1);
    }

    @Test
    public void testGetTotalSessionsTotalBytes() throws UnknownHostException {
        new MockSessionBuilder().setBytes(1000L).build(a);
        new MockSessionBuilder().setBytes(234L).build(b);

        network.getTotalConversation(new LinkedList<Conversation>() {{
            add(aConv);
            add(bConv);
        }});
        verify(c).setBytesTransferred(eq(1234L));
    }
}
