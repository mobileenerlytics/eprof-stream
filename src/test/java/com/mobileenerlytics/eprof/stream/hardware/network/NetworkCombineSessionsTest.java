package com.mobileenerlytics.eprof.stream.hardware.network;

import com.att.aro.core.packetanalysis.pojo.PacketInfo;
import com.att.aro.core.packetanalysis.pojo.Session;
import com.mobileenerlytics.eprof.stream.mocks.MockSessionBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.context.ApplicationContext;

import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class NetworkCombineSessionsTest {
    @InjectMocks
    @Spy
    Network network;

    @Mock
    Session a, b, c, d;

    @Mock
    ApplicationContext ctx;

    @BeforeEach
    public void setup() throws UnknownHostException {
        MockitoAnnotations.initMocks(this);
        doReturn(c).when(network).createSession(any(), anyInt(), any(), anyInt());
        new MockSessionBuilder().setBytes(1000L).build(a);
    }

    @Test
    public void testCombineSession() throws UnknownHostException {
        new MockSessionBuilder().setBytes(234L).build(b);
        network.combineSessions(a, b);
        verify(c).setBytesTransferred(eq(1234L));
    }

    @Test
    public void testCombineSessionWithTCP() throws UnknownHostException {
        new MockSessionBuilder().setUDP(false).build(b);
        assertThrows(IllegalArgumentException.class, () -> network.combineSessions(a, b));
    }

    @Test
    public void testCombineSessionWithPacketInfos() throws UnknownHostException {
        new MockSessionBuilder().setPacketInfoList(new LinkedList<PacketInfo>() {{
            add(MockSessionBuilder.mockPacketInfo(10));
            add(MockSessionBuilder.mockPacketInfo(30));
            add(MockSessionBuilder.mockPacketInfo(50));
        }}).build(b);

        new MockSessionBuilder().setPacketInfoList(new LinkedList<PacketInfo>() {{
            add(MockSessionBuilder.mockPacketInfo(20));
            add(MockSessionBuilder.mockPacketInfo(40));
        }}).build(d);

        network.combineSessions(b, d);
        ArgumentCaptor<List<PacketInfo>> captor = ArgumentCaptor.forClass(List.class);
        verify(c).setUdpPackets(captor.capture());
        assertEquals(5, captor.getValue().size());
        assertArrayEquals(new double[]{10, 20, 30, 40, 50},
                captor.getValue().stream().mapToDouble(PacketInfo::getTimeStamp).toArray(), 0.1);
    }

    @Test
    public void testCombineWithDifferentSession() throws UnknownHostException {
        new MockSessionBuilder().setRemotePort(400).build(b);
        assertThrows(IllegalArgumentException.class, () -> network.combineSessions(a, b));
    }
}
