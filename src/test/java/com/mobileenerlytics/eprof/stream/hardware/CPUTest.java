package com.mobileenerlytics.eprof.stream.hardware;

import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.mocks.MockDoubleFactory;
import com.mobileenerlytics.eprof.stream.util.PowerSumCalculator;
import com.mobileenerlytics.eprof.stream.util.TimeHelper;
import com.mobileenerlytics.eprof.stream.util.mocks.MockXmlBuilder;
import com.mobileenerlytics.symbolic.DoubleFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.context.ApplicationContext;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class CPUTest {
    @Mock
    Globals globals;

    @Spy
    ApplicationContext ctx;

    @Spy
    PowerSumCalculator powerSumCalculator;

    @InjectMocks
    CPU cpu;

    @Mock
    DoubleFactory doubleFactory;

    @Captor
    ArgumentCaptor<long[]> captor;

    @Spy
    TimeHelper timeHelper;

    Core[] cores = new Core[2];

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MockDoubleFactory.mock(doubleFactory);
        cores[0] = mock(Core.class);
        cores[1] = mock(Core.class);
        for(Core core: cores) {
            doNothing().when(core).addState(any());
            doNothing().when(core).verifyFreqs(any());
        }
        doReturn(cores[0], cores[1]).when(ctx).getBean(eq(Core.class), any());
    }

    @Test
    public void testParseLogsEmpty() throws IOException, ExecutionException, InterruptedException {
        // Since this test doesn't do build, we need to explicitly set the cores in cpu
        cpu.cores = cores;
        when(cores[0].parseLogs()).thenReturn(new HashMap<>());
        when(cores[1].parseLogs()).thenReturn(new HashMap<>());
        assertTrue(cpu.parseLogs().isEmpty());
        verify(powerSumCalculator, times(1)).
                sumPowerByTasks(argThat(argument -> argument != null && argument.size() == 2));
    }

    @Test
    public void testBuildAddsStatesToCores() throws IOException, SAXException, ParserConfigurationException {
        String xml = "    <cpumodel corenum='2'>" +
                            "<coregroup>" +
                                 "<freqs>" +
                                     "<freq val='300000'/>" +
                                     "<freq val='422400'/>" +
                                 "</freqs>" +
                                 "<cores  idle='2'>" +
                                    "<core id='0'>" +
                                         "<state mode='multicore' freq='300000' active='19'/>" +
                                         "<state mode='multicore' freq='422400' active='25'/>" +
                                     "</core>"+
                                     "<core id='1'>" +
                                         "<state mode='multicore' freq='300000' active='19'/>" +
                                         "<state mode='multicore' freq='422400' active='25'/>" +
                                    "</core>"+
                                 "</cores>"+
                            "</coregroup>" +
                        "</cpumodel>";
        Document document = MockXmlBuilder.fromString(xml);
        cpu.build(document.getElementsByTagName("cpumodel"));
        assertEquals(2, cpu.cores.length);

        verify(cores[0], times(2)).addState(notNull());
        verify(cores[1], times(2)).addState(notNull());

        verify(cores[0]).verifyFreqs(captor.capture());
        assertArrayEquals(new long[]{300_000, 422_400}, captor.getValue());
    }

    @Test
    public void testBuildGroups() throws IOException, SAXException, ParserConfigurationException {
        String xml = "    <cpumodel corenum='2'>" +
                            "<coregroup>" +
                                 "<freqs>" +
                                     "<freq val='300000'/>" +
                                 "</freqs>" +
                                 "<cores idle='2'>" +
                                    "<core id='0'>" +
                                         "<state mode='multicore' freq='300000' active='19'/>" +
                                     "</core>"+
                                "</cores>"+
                            "</coregroup>" +
                            "<coregroup>" +
                                "<freqs>" +
                                    "<freq val='422400'/>" +
                                "</freqs>" +
                                "<cores idle='2'>" +
                                     "<core id='1'>" +
                                         "<state mode='multicore' freq='422400' active='25'/>" +
                                     "</core>"+
                                 "</cores>"+
                            "</coregroup>" +
                        "</cpumodel>";
        Document document = MockXmlBuilder.fromString(xml);
        cpu.build(document.getElementsByTagName("cpumodel"));
        assertEquals(2, cpu.cores.length);

        verify(cores[0], times(1)).addState(notNull());
        verify(cores[1], times(1)).addState(notNull());

        verify(cores[0]).verifyFreqs(captor.capture());
        assertArrayEquals(new long[]{300_000}, captor.getValue());

        verify(cores[1]).verifyFreqs(captor.capture());
        assertArrayEquals(new long[]{422_400}, captor.getValue());
    }

    @Test
    public void testBuildChecksFreqOrder() throws IOException, SAXException, ParserConfigurationException {
        // The order is important since we assume sorted order when extrapolating states
        String xml = "    <cpumodel corenum='2'>" +
                            "<coregroup>" +
                                 "<freqs>" +
                                     "<freq val='422400'/>" +
                                     "<freq val='300000'/>" +
                                 "</freqs>" +
                            "</coregroup>" +
                        "</cpumodel>";
        Document document = MockXmlBuilder.fromString(xml);
        assertThrows(RuntimeException.class, () -> cpu.build(document.getElementsByTagName("cpumodel")));
    }

    @Test
    public void testFailsToBuildOnMissingGroups() throws IOException, SAXException, ParserConfigurationException {
         // The order is important since we assume sorted order when extrapolating states
        String xml = "<cpumodel corenum='2'>" +
                        "</cpumodel>";
        Document document = MockXmlBuilder.fromString(xml);
        assertThrows(RuntimeException.class, () -> cpu.build(document.getElementsByTagName("cpumodel")));
    }

    @Test
    public void testFailsToBuildOnMissingCores() throws IOException, SAXException, ParserConfigurationException {
        String xml = "    <cpumodel corenum='2'>" +
                            "<coregroup>" +
                                 "<freqs>" +
                                    "<freq val='300000'/>" +
                                    "<freq val='422400'/>" +
                                 "</freqs>" +
                            "</coregroup>" +
                        "</cpumodel>";
        Document document = MockXmlBuilder.fromString(xml);
        assertFalse(cpu.build(document.getElementsByTagName("cpumodel")));
    }
}
