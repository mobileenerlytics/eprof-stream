package com.mobileenerlytics.eprof.stream.readers;

import com.mobileenerlytics.eprof.stream.BaseIntegrationTest;
import com.mobileenerlytics.eprof.stream.Globals;
import com.mobileenerlytics.eprof.stream.hardware.Core;
import com.mobileenerlytics.eprof.stream.hardware.PowerEvent;
import com.mobileenerlytics.eprof.stream.mocks.MockDbgSpliterator;
import com.mobileenerlytics.eprof.stream.mocks.MockGlobalsBuilder;
import com.mobileenerlytics.eprof.stream.ps.TaskData;
import com.mobileenerlytics.eprof.stream.util.AtraceHelper;
import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import org.apache.commons.lang3.tuple.Pair;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.util.*;

import static com.mobileenerlytics.eprof.stream.ps.TaskData.PHONE_PID;
import static com.mobileenerlytics.eprof.stream.ps.TaskData.UNKNOWN_PID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class CoreReaderTest {
    @Mock
    Globals globals;

    @InjectMocks
    CoreReader coreReader;

    @Spy
    AtraceHelper atraceHelper;

    @Spy
    ApplicationContext context;

    DbgSpliterator[] spliterators = new DbgSpliterator[5];

    @Mock
    Core core;

    @BeforeEach
    public void setup() throws IOException {
        MockitoAnnotations.initMocks(this);
        atraceHelper.timeOffsetUs = 0;
        doReturn(true).when(atraceHelper).inBounds(anyString(), any());
        doReturn(0).when(core).idx();
        for(int i = 0; i < spliterators.length; i ++)
            spliterators[i] = spy(DbgSpliterator.class);


        Map<Integer, TaskData> taskDataMap = new HashMap<>();
        new MockGlobalsBuilder(globals)
                .setGlobalTimeUs(Pair.of(0L, 10_000_000L))
                .addTaskDataMap(taskDataMap);

    }

    @Test
    public void testFrequency() {
        String[] testFreqString = {
                "           <...>-10074 (-----) [001] ...1  0.0: cpu_frequency: state=1 cpu_id=1",
                "           <...>-10074 (-----) [000] ...1  1.0: cpu_frequency: state=1 cpu_id=0",
                "           <...>-10074 (-----) [000] ...1  2.0: cpu_frequency: state=2 cpu_id=0",
                "           <...>-10074 (-----) [001] ...1  3.0: cpu_frequency: state=1 cpu_id=1",
                "           <...>-10074 (-----) [000] ...1  4.0: cpu_frequency: state=1 cpu_id=0",
                "           <...>-10074 (-----) [001] ...1  8.0: cpu_frequency: state=1 cpu_id=1",
        };

        MockDbgSpliterator.mockDbgSpliterator(testFreqString, context, spliterators);
        BaseIntegrationTest.doReturn(10.0, 20.0, 10.0).when(core).getActiveCurrent(anyString());

        NavigableMap<Long, PowerEvent> phonePowerEvents = coreReader.getPhonePowerEvents(core);
        assertThat(phonePowerEvents.values(), Matchers.contains(
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 0),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(10), 1_000_000),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(20), 2_000_000),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(10), 4_000_000),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 10_000_000)
        ));
    }

    @Test
    public void testIdle() {
        String[] testIdleString = {
                "           <...>-10074 (-----) [000] ...1  1.0: cpu_frequency: state=1 cpu_id=0" ,
                "          <idle>-0     (-----) [000] .N.2  2.0: sched_switch: prev_pid=10074 next_pid=0" ,
                "          <idle>-0     (-----) [000] .N.2  3.0: sched_switch: prev_pid=0 next_pid=10074" ,
                "           <...>-10074 (-----) [001] ...1  5.0: cpu_frequency: state=2 cpu_id=1" ,
                "           <...>-10074 (-----) [000] ...1  6.0: cpu_frequency: state=2 cpu_id=0" ,
                "          <idle>-0     (-----) [000] .N.2  8.0: sched_switch: prev_pid=10074 next_pid=0",
        };

        MockDbgSpliterator.mockDbgSpliterator(testIdleString, context, spliterators);
        BaseIntegrationTest.doReturn(5.0, 15.0, 5.0).when(core).getIdleCurrent(anyString(), any());
        BaseIntegrationTest.doReturn(10.0, 20.0).when(core).getActiveCurrent(anyString());

        NavigableMap<Long, PowerEvent> phonePowerEvents = coreReader.getPhonePowerEvents(core);
        assertThat(phonePowerEvents.values(), Matchers.contains(
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 0),
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(10), 1_000_000),   // active current
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(5), 2_000_000),    // idle current
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(15), 3_000_000),   // idle current
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(20), 6_000_000),   // active current
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(5), 8_000_000),    // idle current
                new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(0), 10_000_000)
        ));
    }

    @Test
    public void testCtxSw() {
        // Although the phone power events are mocked, the cpu_frequency lines are still required for this test to work.
        // CoreReader searches for cpu_frequency lines to insert the additional power events in task power list.
        String[] testCtxSwString = {
            "           <...>-10074 (-----) [000] ...1  1.0: cpu_frequency: state=1 cpu_id=0",
            "             proc10-10 (   10) [000] d..3  2.0: sched_switch: prevTaskId=10 next_pid=11" ,
            "           thread11-11 (   10) [000] d..3  3.0: sched_switch: prevTaskId=11 next_pid=10" ,
            "             proc10-10 (   10) [000] d..3  4.0: sched_switch: prevTaskId=10 next_pid=11" ,
            "           thread11-11 (   10) [000] d..3  5.0: sched_switch: prevTaskId=11 next_pid=10" ,
            "           <...>-10074 (-----) [000] ...1  5.0: cpu_frequency: state=1 cpu_id=0",
            "             proc10-10 (   10) [000] d..3  7.0: sched_switch: prevTaskId=10 next_pid=11" ,
            "           thread11-11 (   10) [000] d..3  8.0: sched_switch: prevTaskId=11 next_pid=10" ,
            "             proc10-10 (   10) [000] d..3  9.0: sched_switch: prevTaskId=10 next_pid=11" ,
            "           <...>-10074 (-----) [000] ...1  9.0: cpu_frequency: state=1 cpu_id=0",
        };

        TreeMap<Long, PowerEvent> phonePowerEvents = new TreeMap<Long, PowerEvent>() {{
            put(1_000_000L, new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(10.0), 1_000_000));
            put(5_000_000L, new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(20.0), 5_000_000));
            put(9_000_000L, new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(10.0), 9_000_000));
        }};

        MockDbgSpliterator.mockDbgSpliterator(testCtxSwString, context, spliterators);

        Map<Integer, List<PowerEvent>> taskPowerMap = coreReader.getTaskToPowerEventsListMap(core, phonePowerEvents);
        assertEquals(3, taskPowerMap.size());

        assertThat(taskPowerMap.get(10), Matchers.contains(
                new PowerEvent(10, new ConcreteUnmodifiableDouble(10), 3_000_000),
                new PowerEvent(10, new ConcreteUnmodifiableDouble(0), 4_000_000),
                new PowerEvent(10, new ConcreteUnmodifiableDouble(20), 5_000_000),
                new PowerEvent(10, new ConcreteUnmodifiableDouble(0), 7_000_000),
                new PowerEvent(10, new ConcreteUnmodifiableDouble(20), 8_000_000),
                new PowerEvent(10, new ConcreteUnmodifiableDouble(0), 9_000_000)
        ));

        assertThat(taskPowerMap.get(11), Matchers.contains(
                new PowerEvent(11, new ConcreteUnmodifiableDouble(10), 2_000_000),
                new PowerEvent(11,new ConcreteUnmodifiableDouble(0), 3_000_000),
                new PowerEvent(11,new ConcreteUnmodifiableDouble(10), 4_000_000),
                new PowerEvent(11,new ConcreteUnmodifiableDouble(0), 5_000_000),
                new PowerEvent(11,new ConcreteUnmodifiableDouble( 20), 7_000_000),
                new PowerEvent(11,new ConcreteUnmodifiableDouble( 0), 8_000_000),
                new PowerEvent(11,new ConcreteUnmodifiableDouble(10), 9_000_000),
                new PowerEvent(11,new ConcreteUnmodifiableDouble(0), 10_000_000)
        ));

        assertThat(taskPowerMap.get(UNKNOWN_PID), Matchers.contains(
                new PowerEvent(UNKNOWN_PID, new ConcreteUnmodifiableDouble(10), 1_000_000),
                new PowerEvent(UNKNOWN_PID, new ConcreteUnmodifiableDouble(0), 2_000_000)
        ));

    }

    @Test
    public void testCtxSwWithPhonePowerChangeInMiddle() {
        // Although the phone power events are mocked, the cpu_frequency lines are still required for this test to work.
        // CoreReader searches for cpu_frequency lines to insert the additional power events in task power list.
        String[] testCtxSw = {
                "           <...>-10074 (-----) [000] ...1  1.0: cpu_frequency: state=1 cpu_id=0",
                "             proc10-10 (   10) [000] d..3  2.0: sched_switch: prevTaskId=10 next_pid=11",
                "           thread11-11 (   10) [000] d..3  3.0: sched_switch: prevTaskId=11 next_pid=10",
                "             proc10-10 (   10) [000] d..3  4.0: sched_switch: prevTaskId=10 next_pid=11",
                "           thread11-11 (   10) [000] d..3  5.0: sched_switch: prevTaskId=11 next_pid=10",
                "           <...>-10074 (-----) [000] ...1  6.0: cpu_frequency: state=2 cpu_id=0",
                "             proc10-10 (   10) [000] d..3  7.0: sched_switch: prevTaskId=10 next_pid=11",
                "           thread11-11 (   10) [000] d..3  8.0: sched_switch: prevTaskId=11 next_pid=10",
                "           <...>-10074 (-----) [000] ...1  9.0: cpu_frequency: state=1 cpu_id=0",
        };

        TreeMap<Long, PowerEvent> phonePowerEvents = new TreeMap<Long, PowerEvent>() {{
            put(1_000_000L, new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(10.0), 1_000_000));
            put(6_000_000L, new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(20.0), 6_000_000));      // Phone power changed in middle of ctx switch
            put(9_000_000L, new PowerEvent(PHONE_PID, new ConcreteUnmodifiableDouble(10.0), 9_000_000));      // Phone power changed in middle of ctx switch
        }};

        MockDbgSpliterator.mockDbgSpliterator(testCtxSw, context, spliterators);

        Map<Integer, List<PowerEvent>> taskPowerMap = coreReader.getTaskToPowerEventsListMap(core, phonePowerEvents);
        assertEquals(3, taskPowerMap.size());

        assertThat(taskPowerMap.get(10), Matchers.contains(
                new PowerEvent(10, new ConcreteUnmodifiableDouble(10), 3_000_000),
                new PowerEvent(10, new ConcreteUnmodifiableDouble(0), 4_000_000),
                new PowerEvent(10, new ConcreteUnmodifiableDouble(10), 5_000_000),
                new PowerEvent(10, new ConcreteUnmodifiableDouble(20), 6_000_000),
                new PowerEvent(10, new ConcreteUnmodifiableDouble(0), 7_000_000),
                new PowerEvent(10, new ConcreteUnmodifiableDouble(20), 8_000_000),
                new PowerEvent(10, new ConcreteUnmodifiableDouble(10), 9_000_000),
                new PowerEvent(10, new ConcreteUnmodifiableDouble(0), 10_000_000)
        ));

        assertThat(taskPowerMap.get(11), Matchers.contains(
                new PowerEvent(11, new ConcreteUnmodifiableDouble(10), 2_000_000),
                new PowerEvent(11, new ConcreteUnmodifiableDouble(0), 3_000_000),
                new PowerEvent(11, new ConcreteUnmodifiableDouble(10), 4_000_000),
                new PowerEvent(11, new ConcreteUnmodifiableDouble(0), 5_000_000),
                new PowerEvent(11, new ConcreteUnmodifiableDouble(20), 7_000_000),
                new PowerEvent(11, new ConcreteUnmodifiableDouble(0), 8_000_000)
        ));

        assertThat(taskPowerMap.get(UNKNOWN_PID), Matchers.contains(
                new PowerEvent(UNKNOWN_PID, new ConcreteUnmodifiableDouble(10), 1_000_000),
                new PowerEvent(UNKNOWN_PID, new ConcreteUnmodifiableDouble(0), 2_000_000)
        ));

//        taskDataMap.values().stream()
//                .filter(td -> taskPowerMap.containsKey(td.taskId))
//                .forEach(td ->
//                        td.perComponentEnergy.put("CPU",
//                                EnergyCollector.calcEnergy(globals.getGlobalTimeUs(), taskPowerMap.get(td.taskId))));
//        appJsonWriter.write(taskDataMap, globals.getOutDir());
    }
}
