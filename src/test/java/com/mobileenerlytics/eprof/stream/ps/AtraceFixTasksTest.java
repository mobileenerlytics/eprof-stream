package com.mobileenerlytics.eprof.stream.ps;

import com.mobileenerlytics.eprof.stream.mocks.MockDbgSpliterator;
import com.mobileenerlytics.eprof.stream.readers.DbgSpliterator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.context.ApplicationContext;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.eq;

public class AtraceFixTasksTest {
    @InjectMocks
    AtraceFixTasks atraceFixTasks;

    @Spy
    DbgSpliterator dbgSpliterator;

    @Mock
    ApplicationContext context;

    @Mock
    TaskData mockTask;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        doReturn(mockTask).when(context).getBean(eq(TaskData.class), anyInt(), anyInt(), anyString(), anyString());
    }

    @Test
    public void testFindTasks() {
        ConcurrentMap<Integer, TaskData> taskDataMap = new ConcurrentHashMap<>();

        String[] lines = {
                "EnsureDelegate-3786  ( 3767) [002] d..3 58796.925744: sched_switch: prev_pid=3786 next_pid=302",
                "sush-3911  ( 3911) [000] d..3 58796.925814: sched_switch: prev_pid=3911 next_pid=343",
                "DispSync-302   (  292) [002] d..3 58796.925829: sched_switch: prev_pid=302 next_pid=3786",
                "EventThread-343   (  292) [000] d..3 58796.925918: sched_switch: prev_pid=343 next_pid=3789",
                "tracereduce-3898  ( 3897) [003] d..3 58796.926239: sched_switch: prev_pid=3898 next_pid=867",
                "AsyncView-3789  ( 3767) [000] d..3 58796.926264: sched_switch: prev_pid=3789 next_pid=3911",
                "<...>-4051  (-----) [000] d..4 58799.910314: sched_switch: prev_pid=4051 next_pid=3823"

        };
        MockDbgSpliterator.mockDbgSpliterator(lines, context, dbgSpliterator);

        atraceFixTasks.findAndFixTasks(taskDataMap);
        assertTrue(taskDataMap.containsKey(3786));
        assertTrue(taskDataMap.containsKey(3911));
        assertTrue(taskDataMap.containsKey(302));
        assertTrue(taskDataMap.containsKey(343));
        assertTrue(taskDataMap.containsKey(3898));
        assertTrue(taskDataMap.containsKey(3789));
        assertTrue(taskDataMap.containsKey(4051));
    }
}
