package com.mobileenerlytics.eprof.stream;

import com.mobileenerlytics.symbolic.ConcreteUnmodifiableDouble;
import com.mobileenerlytics.symbolic.DoubleUtil;
import com.mobileenerlytics.symbolic.MyDouble;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Stubber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("/META-INF/config.xml")
public class BaseIntegrationTest {
    @Autowired
    protected ApplicationContext context;

    //this blank method need to be here to avoid error
    @Test
    public void testSuppression(){ }

    public static void assertEquals(MyDouble expected, MyDouble md, double delta) {
        Assertions.assertEquals(DoubleUtil.value(expected), DoubleUtil.value(md), delta);
    }
    public static void assertEquals(double value, MyDouble md, double delta) {
        Assertions.assertEquals(value, DoubleUtil.value(md), delta);
    }

    public static Stubber doReturn(double toBeReturned, double... toBeReturnedNext) {
        ConcreteUnmodifiableDouble[] next = Arrays.stream(toBeReturnedNext).mapToObj(t -> new ConcreteUnmodifiableDouble(t))
                .toArray(ConcreteUnmodifiableDouble[]::new);
        return Mockito.doReturn(new ConcreteUnmodifiableDouble(toBeReturned), next);
    }
}
